directives.directive 'fbLike', [
  '$window'
  '$rootScope'
  '$timeout'
  'angularLoad'
  ($window, $rootScope, $timeout, angularLoad) ->
    {
      restrict: 'A'
      scope: fbLike: '=?'
      link: (scope, element, attrs) ->
        renderLikeButton = ->
          if attrs.fbLike and not scope.fbLike and not watchAdded
            # wait for data if it hasn't loaded yet
            watchAdded = true
            unbindWatch = scope.$watch 'fbLike', (newValue, oldValue) ->
              if newValue
                renderLikeButton()
                # only need to run once
                unbindWatch()
          else
            element.html '<div class="fb-like" data-href="https://www.facebook.com/tuzoapp" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>'
            # $window.FB.XFBML.parse(element.parent()[0])

        if not $window.FB
          # Load Facebook SDK if not already loaded
          angularLoad.loadScript 'http://connect.facebook.net/es_LA/sdk.js'
          .then ->
            $window.FB.init
              appId: $rootScope.facebookAppId
              xfbml: true
              version: 'v2.4'
            renderLikeButton()
          .catch (e)->
            console.log "Error"
        else
          renderLikeButton()
        watchAdded = false
        return

    }
]
