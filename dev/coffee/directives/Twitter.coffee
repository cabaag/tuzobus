directives.directive "twitterTimeline", [
  '$timeout'
  ($timeout)->
    {
      restrict: 'E'
      template: '<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/cabaag03/lists/tuzoapp" data-widget-id="631879557834903552"></a>'
      link: (scope, element, attrs)->
        run = ->
          !((d, s, id) ->
            js = undefined
            fjs = d.getElementsByTagName(s)[0]
            p = if /^http:/.test(d.location) then 'http' else 'https'
            if !d.getElementById(id)
              js = d.createElement(s)
              js.id = id
              js.src = p + '://platform.twitter.com/widgets.js'
              fjs.parentNode.insertBefore js, fjs
            return
          )(document, 'script', 'twitter-wjs')
        run()
    }
]
