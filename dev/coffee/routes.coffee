angular.module 'tuzobus'
.config [
  '$stateProvider'
  '$urlRouterProvider'
  ($stateProvider, $urlRouterProvider) ->
    $stateProvider
      .state 'app',
        url: '/app'
        abstract: true
        templateUrl: 'templates/menu.html'
        controller: 'AppController'

      .state 'app.card',
        url: '/card'
        resolve:
          enter: [
            '$rootScope'
            ($rootScope)->
              $rootScope.$broadcast 'hideRightNavButton', false
              $rootScope.$broadcast 'iconRightNavButton', "ion-android-sync"
              $rootScope.$broadcast 'funcRightNavButton', "readCard"
          ]
        views:
          'menuContent':
            templateUrl: 'templates/card.html'
            controller: 'CardController'

      .state 'app.map',
        url: '/map'
        resolve:
          currentLocation: [
            '$rootScope'
            '$ionicLoading'
            'LocationFactory'
            ($rootScope, $ionicLoading, $location)->
              $rootScope.$broadcast 'hideRightNavButton', false
              $rootScope.$broadcast 'iconRightNavButton', 'ion-eye'
              $rootScope.$broadcast 'funcRightNavButton', 'showOptionsMap'
              $ionicLoading.show
                template: '<div class="loader">'+
                '<svg class="circular">'+
                '<circle class="path" cx="50" cy="50" r="20" fill="none" '+
                'stroke-width="2" stroke-miterlimit="10"/>'+
                '</svg></div>'
                hideOnStateChange: true
              $location.findLocation().then (success)->
                return success
              , (error)->
                return "Error"
          ]
        views:
          'menuContent':
            templateUrl: 'templates/map.html'
            controller: 'MapController'

      .state 'app.maker',
        url: '/maker/:id'
        resolve:
          currentLocation: [
            '$rootScope'
            '$ionicLoading'
            'LocationFactory'
            ($rootScope, $ionicLoading, $location)->
              $rootScope.$broadcast 'hideRightNavButton', true
              $ionicLoading.show
                template: '<div class="loader">'+
                '<svg class="circular">'+
                '<circle class="path" cx="50" cy="50" r="20" fill="none" '+
                'stroke-width="2" stroke-miterlimit="10"/>'+
                '</svg></div>'
                hideOnStateChange: true
              $location.findLocation().then (success)->
                return success
              , (error)->
                return "Error"
          ]
        views:
          'menuContent':
            templateUrl: 'templates/maker.html'
            controller: 'MakerController'

      .state 'app.stations',
        url: '/stations'
        resolve:
          enter: [
            '$rootScope'
            ($rootScope)->
              $rootScope.$broadcast 'hideRightNavButton', true
              $rootScope.$broadcast 'iconRightNavButton', ''
          ]
        views:
          'menuContent':
            templateUrl: 'templates/stations.html'
            controller: 'StationsController'

      .state 'app.station',
        url: '/station/:id'
        resolve:
          station: [
            '$rootScope'
            '$stateParams'
            'StationsFactory'
            ($rootScope, $stateParams, $stations)->
              $rootScope.$broadcast 'hideRightNavButton', false
              $rootScope.$broadcast 'funcRightNavButton', 'createRouteToStation'
              $rootScope.$broadcast 'iconRightNavButton', 'ion-navigate'
              $stations.getStationById($stateParams.id)
          ]
        views:
          'menuContent':
            templateUrl: 'templates/station.html'
            controller: 'StationController'

      .state 'app.express-routes',
        url: '/express-routes'
        resolve:
          enter: [
            '$rootScope'
            ($rootScope)->
              $rootScope.$broadcast 'hideRightNavButton', true
          ]
        views:
          'menuContent':
            templateUrl: 'templates/express-routes.html'

      .state 'app.express-route',
        url: '/express-routes/:id'
        resolve:
          enter: [
            '$rootScope'
            ($rootScope)->
              $rootScope.$broadcast 'hideRightNavButton', true
          ]
        views:
          'menuContent':
            templateUrl: 'templates/express-route.html'
            controller: 'ExpressRouteController'

      .state 'app.feeder-routes',
        url: '/feeder-routes'
        resolve:
          enter: [
            '$rootScope'
            ($rootScope)->
              $rootScope.$broadcast 'hideRightNavButton', true
              true
          ]
        views:
          'menuContent':
            templateUrl: 'templates/feeder-routes.html'
            controller: 'FeederRoutesController'

      .state 'app.places',
        url: '/places'
        resolve:
          enter: [
            '$rootScope'
            ($rootScope)->
              $rootScope.$broadcast 'hideRightNavButton', false
              $rootScope.$broadcast 'iconRightNavButton', "ion-ios-settings"
              $rootScope.$broadcast 'funcRightNavButton', "showOptionsPlaces"
          ]
        views:
          'menuContent':
            templateUrl: 'templates/places.html'
            controller: 'PlacesController'

      .state 'app.place',
        url: '/place/:id'
        # resolve:
        #   currentLocation: [
        #     '$rootScope'
        #     'LocationFactory'
        #     ($rootScope, $location)->
        #       $rootScope.$broadcast 'hideRightNavButton', true
        #       $rootScope.$broadcast 'iconRightNavButton', 'ion-eye'
        #       $rootScope.$broadcast 'funcRightNavButton', 'showOptionsMap'
        #       $location.findLocation().then (success)->
        #         return success
        #       , (error)->
        #         return "Error"
        #   ]
        views:
          'menuContent':
            templateUrl: 'templates/place.html'
            controller: 'PlaceController'

      .state 'app.events',
        url: '/events',
        views:
          'menuContent':
            templateUrl: 'templates/events.html'
            controller: 'EventsController'

      .state 'app.news',
        url: '/news',
        resolve:
          tweets: [
            '$rootScope'
            '$ionicLoading'
            'API'
            ($rootScope, $ionicLoading, $api)->
              $rootScope.$broadcast 'hideRightNavButton', true
              $ionicLoading.show
                template: '<div class="loader">'+
                '<svg class="circular">'+
                '<circle class="path" cx="50" cy="50" r="20" fill="none" '+
                'stroke-width="2" stroke-miterlimit="10"/>'+
                '</svg></div>'
                hideOnStateChange: true
              $api.getTweets()
          ]
        views:
          'menuContent':
            templateUrl: 'templates/news.html'
            controller: 'NewsController'

      .state 'app.info',
        url: '/info'
        resolve:
          enter: [
            '$rootScope'
            ($rootScope)->
              $rootScope.$broadcast 'hideRightNavButton', true
          ]
        views:
          'menuContent':
            templateUrl: 'templates/information.html'
            controller: 'InfoController'

      .state 'app.config',
        url: '/config'
        resolve:
          enter: [
            '$rootScope'
            ($rootScope)->
              $rootScope.$broadcast 'hideRightNavButton', true
          ]
        views:
          'menuContent':
            templateUrl: 'templates/configuration.html'
            controller: 'ConfigController'

      .state 'app.help',
        url: '/help'
        resolve:
          enter: [
            '$rootScope'
            ($rootScope)->
              $rootScope.$broadcast 'hideRightNavButton', true
          ]
        views:
          'menuContent':
            templateUrl: 'templates/help.html'
            controller: 'InfoController'

      .state 'app.about',
        url: '/about'
        resolve:
          enter: [
            '$rootScope'
            ($rootScope)->
              $rootScope.$broadcast 'hideRightNavButton', true
          ]
        views:
          'menuContent':
            templateUrl: 'templates/about.html'
            controller: 'InfoController'

    if not localStorage.firstTime?
      $urlRouterProvider.otherwise 'app/help'
      localStorage.firstTime = true
    else
      $urlRouterProvider.otherwise 'app/help'
]
