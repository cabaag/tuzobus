filters.filter 'twitterTime', ->
  (date)->
    now = Date.now()
    tweetTime = new Date(date).getTime()
    time = now - tweetTime
    time = time / 1000 / 60 / 60
    if time < 1
      time *= 10
      time = Math.floor(time)
      time += 'm'
    else if time >= 1 and time <= 25
      time = Math.floor(time)
      time += 'h'
    else
      time = now - new Date(date).getTime()
      time = now - time
      # new Date(time)
    return time
