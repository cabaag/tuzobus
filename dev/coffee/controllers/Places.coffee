controllers.controller 'PlacesController', [
  '$rootScope'
  '$scope'
  '$timeout'
  '$ionicPopover'
  'PlacesFactory'
  ($rootScope, $scope, $timeout, $ionicPopover, $places)->
    $rootScope.$broadcast 'hideRightNavButton', false
    $rootScope.$broadcast 'iconRightNavButton', "ion-ios-settings"
    $rootScope.$broadcast 'funcRightNavButton', "showOptionsPlaces"

    $scope.places = $places.getPlaces()
    $scope.lastCategory = ''
    $scope.order = 'id'
    $scope.popover = $ionicPopover.fromTemplateUrl 'templates/optionsPlaces.html',
      scope: $scope
    .then (popover)->
      $scope.popover = popover
    #
    $scope.isNewCategory = (category)->
      if $scope.lastCategory != category
        $scope.lastCategory = category
        return true
      return false

    $scope.$on 'orderBy', (event, data)->
      if data is "category"
        $places.getPlaces().sort((a,b)->
          categoryA = a.category.toLowerCase()
          categoryB = b.category.toLowerCase()
          if categoryA < categoryB
            return -1
          if categoryA > categoryB
            return 1
          return 0
        )
        $scope.places = $places.getPlaces()
        console.log $scope.places
      $scope.order = data

    $scope.$on 'showOptionsPlaces', (event, data)->
      $scope.popover.show(data)

]
.controller 'OptionsPlacesController', [
  '$scope'
  ($scope)->
    $scope.orderBy = ->
      $scope.$emit 'orderBy', $scope.order
    return
]
