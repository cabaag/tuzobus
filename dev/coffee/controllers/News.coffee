controllers.controller 'NewsController', [
  '$scope'
  '$localStorage'
  '$ionicLoading'
  'tweets'
  'API'
  ($scope, $localStorage, $ionicLoading, tweets, $api)->
    $scope.tweets = tweets
    $scope.counterTweets = 15
    $ionicLoading.hide
    $scope.loadMore = ->
      $ionicLoading.show
        template: '<div class="loader">'+
        '<svg class="circular">'+
        '<circle class="path" cx="50" cy="50" r="20" fill="none" '+
        'stroke-width="2" stroke-miterlimit="10"/>'+
        '</svg></div>'
      $scope.counterTweets += 15
      $api.getTweets($scope.counterTweets)
      .then (tweets)->
        $scope.tweets = tweets
        $ionicLoading.hide()
      , (error)->
        console.log error
]
