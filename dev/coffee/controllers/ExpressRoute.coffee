controllers.controller 'ExpressRouteController', [
  '$scope'
  '$stateParams'
  'ExpressFactory'
  ($scope, $stateParams, $express)->
    if $stateParams.id is "0"
      $scope.title = 'MAIN_ROUTE'
    else
      $scope.title = "ROUTE_#{$stateParams.id}"
    $scope.express = $express.getExpress($stateParams.id)
    $scope.stations = $express.getExpress(0)
    console.log $scope.express
    $scope.routeId = $stateParams.id
]
