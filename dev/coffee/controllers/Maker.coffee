controllers.controller 'MakerController', [
  '$rootScope'
  '$scope'
  '$timeout'
  '$ionicPopup'
  '$ionicLoading'
  '$stateParams'
  '$localStorage'
  '$ionicPlatform'
  'currentLocation'
  'StationsFactory'

  ($rootScope, $scope, $timeout, $ionicPopup, $ionicLoading, $stateParams,
  $localStorage, $ionicPlatform, currentLocation, $stations)->


    #######################################

    $rootScope.$broadcast 'hideRightNavButton', true
    path = [
      {
        latitude: 0
        longitude: 0
      }
      {
        latitude: 0
        longitude: 0
      }
    ]
    strokeOrigin =
      color: "#1B5E20"
      weight: 5
    strokeStations =
      color: "#1B5E20"
      weight: 5
    strokeDestine =
      color: "#1B5E20"
      weight: 5
    $scope.routeOrigin =
      path: path
    $scope.routeStations =
      path: path
    $scope.routeDestine =
      path: path
    currentLocation =
      latitude: 20.090345
      longitude: -98.767556

    $scope.placesOrigin = $localStorage.placesOrigin || []
    $scope.placesDestine = $localStorage.placesDestine || []

    if currentLocation isnt "Error"
      $localStorage.currentLocation = currentLocation
    else
      currentLocation = $localStorage.currentLocation?
      $ionicPopup.alert
        title: 'Error',
        template: 'No se pudo obtener localización'

    $ionicLoading.hide()
    $scope.map =
      coords: currentLocation
      zoom: 14
      options:
        disableDefaultUI: true
        maxZoom: 18
        minZoom: 12
        styles: [{
          featureType: "poi",
          stylers: [{
            visibility: "off"
          }]
        }]
        streetViewControl: false
      pan: true
      control: {}
    $scope.origin = ''
    $scope.destine = ''
    $scope.markerOrigin =
      id: "origin"
      coords:
        longitude: currentLocation.longitude
        latitude: currentLocation.latitude
      icon:
        url: 'https://maps.gstatic.com/mapfiles/ms2/micons/red-dot.png'
      options:
        draggable: true
      events:
        dragend: ->
          $scope.getAddressOrigin()

    $scope.markerDestine =
      id: "destine"
      coords:
        longitude: currentLocation.longitude + 0.005
        latitude: currentLocation.latitude + 0.005
      icon:
        url: 'https://maps.gstatic.com/mapfiles/ms2/micons/blue-dot.png'
      options:
        draggable: true
      events:
        dragend: ->
          $scope.getAddressDestine()

    $scope.activeInputOrigin = false
    $scope.activeInputDestine = false

    $scope.setOrigin = (origin)->
      $scope.origin = origin
      if $scope.origin isnt 'Mi ubicación'
        geocoder = new google.maps.Geocoder()
        geocoder.geocode
          address: $scope.origin
        , (results, status)->
          if status is google.maps.GeocoderStatus.OK
            $scope.markerOrigin.coords =
              latitude: results[0].geometry.location.G
              longitude: results[0].geometry.location.K
        $scope.map.control.refresh()

    $scope.toggleInputOrigin = ->
      $timeout ->
        $scope.activeInputOrigin = not $scope.activeInputOrigin
      , 250

    $scope.getAddressOrigin = ->
      location = new google.maps.LatLng($scope.markerOrigin.coords.latitude,
      $scope.markerOrigin.coords.longitude)
      geocoder = new google.maps.Geocoder()
      geocoder.geocode
        location: location
      , (results, status)->
        if status is google.maps.GeocoderStatus.OK
          $scope.origin = results[0].formatted_address
          $scope.$apply()
        else
          $ionicPopup.alert
            title: 'Error',
            template: 'Intente en otro momento'
          alert("Geocode wasn't successful for the following reason: " + status)

    $scope.setDestine = (destine)->
      $scope.destine = destine
      if $scope.destine isnt 'Mi ubicación'
        geocoder = new google.maps.Geocoder()
        geocoder.geocode
          address: $scope.destine
        , (results, status)->
          if status is google.maps.GeocoderStatus.OK
            $scope.markerDestine.coords =
              latitude: results[0].geometry.location.G
              longitude: results[0].geometry.location.K
        $scope.map.control.refresh()

    $scope.toggleInputDestine = ->
      $timeout ->
        $scope.activeInputDestine = not $scope.activeInputDestine
      , 250

    $scope.getAddressDestine = ->
      location = new google.maps.LatLng($scope.markerDestine.coords.latitude,
      $scope.markerDestine.coords.longitude)
      geocoder = new google.maps.Geocoder()
      geocoder.geocode
        location: location
      , (results, status)->
        if status is google.maps.GeocoderStatus.OK
          $scope.destine = results[0].formatted_address
          $scope.$apply()
        else
          $ionicPopup.alert
            title: 'Error',
            template: 'Intente en otro momento'
          alert("Geocode wasn't successful for the following reason: " + status)

    $scope.createRoute = ->
      $ionicLoading.show
        template: '<div class="loader">'+
        '<svg class="circular">'+
        '<circle class="path" cx="50" cy="50" r="20" fill="none" '+
        'stroke-width="2" stroke-miterlimit="10"/>'+
        '</svg></div>'
      if $scope.placesOrigin.indexOf($scope.origin) is -1
        $scope.placesOrigin.push($scope.origin)
      if $scope.placesDestine.indexOf($scope.destine) is -1
        $scope.placesDestine.push($scope.destine)
      $localStorage.placesOrigin = $scope.placesOrigin
      $localStorage.placesDestine = $scope.placesDestine
      $scope.routes =
        path: [
          {
            latitude: 0
            longitude: 0
          }
          {
            latitude: 0
            longitude: 0
          }
        ]
        stroke:
          weight: 1
      $scope.routeOrigin.path = []
      $scope.routeStations.path = []
      $scope.routeDestine.path = []

      $stations.searchNearestStation($scope.markerOrigin.coords)
      .then (success)->
        $scope.nearestStationOrigin = success
      , (error)->
        console.log error

      $stations.searchNearestStation($scope.markerDestine.coords)
      .then (success)->
        $scope.nearestStationDestine = success
        createRoute(
          $scope.markerOrigin.coords,
          $scope.nearestStationOrigin.position,
          'origin')
        createRoute(
          $scope.nearestStationOrigin.position,
          $scope.nearestStationDestine.position,
          'stations')
        createRoute(
          $scope.markerDestine.coords,
          $scope.nearestStationDestine.position,
          'destine')
        $scope.map.coords =
          latitude: currentLocation.latitude
          longitude: currentLocation.longitude

        $scope.refreshMap = true
        $scope.map.coords =
          longitude: currentLocation.longitude+0.00000000000001
          latitude: currentLocation.latitude+0.00000000000001
        $ionicLoading.hide()
      , (error)->
        console.log error

    createRoute = (from, to, route)->
      origin = new google.maps.LatLng(from.latitude, from.longitude)
      destination = new google.maps.LatLng(to.latitude, to.longitude)

      directionsService = new google.maps.DirectionsService()
      if route is 'stations'
        travelMode = google.maps.TravelMode.DRIVING
      else
        travelMode = google.maps.TravelMode.WALKING
      directionsService.route
        origin: origin
        destination: destination
        travelMode: travelMode
      , (response, status)->
        path = []
        angular.forEach response.routes[0].overview_path, (value, key)->
          path.push
            latitude: value.lat()
            longitude: value.lng()

        if route is 'origin'
          $scope.routeOrigin.path = path
          $scope.routeOrigin.stroke = strokeOrigin
        else if route is 'stations'
          $scope.routeStations.path = path
          $scope.routeStations.stroke = strokeStations
        else
          $scope.routeDestine.path = path
          $scope.routeDestine.stroke = strokeDestine

    if $stateParams.id isnt ''
      station = $stations.getStationById($stateParams.id)
      $scope.markerDestine.coords =
        longitude: station.position.longitude
        latitude: station.position.latitude
      $scope.origin = 'Mi ubicación'
      $scope.destine = station.name
      uiGmapIsReady.promise(1)
      .then (instances)->
        $scope.createRoute()
    $ionicLoading.hide()

]
