controllers.controller 'MainController', [
  '$scope'
  'ConfigurationFactory'
  ($scope, $config)->
    $scope.theme = $config.getTheme()

    $scope.$on 'changeTheme', (event, data)->
      $scope.theme = data
]
