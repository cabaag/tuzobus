controllers.controller 'PlaceController', [
  '$scope'
  '$stateParams'
  'PlacesFactory'
  'LocationFactory'
  ($scope, $stateParams, $places, $location)->
    $location.findLocation().then (success)->
      currentLocation = success
      service = new google.maps.DistanceMatrixService()
      origin = new google.maps.LatLng(currentLocation.latitude,
        currentLocation.longitude)
      destination = new google.maps.LatLng($scope.place.coords.latitude,
        $scope.place.coords.longitude)
      service.getDistanceMatrix
        origins: [origin]
        destinations: [destination]
        travelMode: google.maps.TravelMode.DRIVING
      , (response, status)->
        if status is "OK"
          place = response.rows[0].elements[0]
          $scope.distance =  place.distance.text
          $scope.eta = place.duration.text
          $scope.$apply()
    , (error)->
      $scope.distance = ''
      $scope.eta = ''
    $scope.place = $places.getPlaceById($stateParams.id)
]
