controllers.controller 'AppController', [
  '$scope'
  '$ionicPlatform'
  '$ionicSideMenuDelegate'
  '$timeout'
  ($scope, $ionicPlatform, $ionicSideMenuDelegate, $timeout)->
    $scope.hideRightNavButton = false
    $scope.iconRightNavButton = ""
    $scope.funcRightNavButton = ""

    $ionicPlatform.ready ->
      # Añade un listener para cuando presione el contenido del menu el
      # menu se cierre automaticamente
      menuContent = document.getElementsByTagName("ion-side-menu-content")[0]
      menuContent.addEventListener "click", ->
        if $ionicSideMenuDelegate.isOpen()
          document.getElementsByTagName("ion-side-menu")[0].style.width = "0px"
      $timeout ->
        if window.innerWidth < 768
          document.getElementsByTagName("ion-side-menu")[0].style.width = "0px"
          document.getElementsByTagName("ion-side-menu")[0].style.opacity = "1"
      , 50
      # Al cambiar el tamaño de la pantalla hace que el menu aparezca

      window.addEventListener "resize", ->
        if window.innerWidth > 767
          console.log "resizing"
          document.getElementsByTagName("ion-side-menu")[0].style.width= "275px"
        else
          document.getElementsByTagName("ion-side-menu")[0].style.width = "0px"

    $scope.toggleLeftSideMenu = ->
      document.getElementsByTagName("ion-side-menu")[0].style.width = "275px"

    $scope.clickRightNavButton = ($event)->
      $scope.$broadcast $scope.funcRightNavButton, $event

    $scope.$on 'hideRightNavButton', (event, data)->
      $scope.hideRightNavButton = data
    $scope.$on 'iconRightNavButton', (event, data)->
      $scope.iconRightNavButton = data
    $scope.$on 'funcRightNavButton', (event, data)->
      $scope.funcRightNavButton = data
]
