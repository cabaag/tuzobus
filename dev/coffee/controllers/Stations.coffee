controllers.controller 'StationsController', [
  '$rootScope'
  '$scope'
  'StationsFactory'
  ($rootScope, $scope, $stations)->
    $rootScope.$broadcast 'hideRightNavButton', false
    $scope.stations = $stations.getStations()
    $scope.station = ""
]
