controllers.controller 'RouteController', [
  "$scope"
  "RoutesFactory"
  "StationsFactory"
  ($scope, $routes, $stations)->
    $scope.routes = $routes.getRoutes()
    angular.forEach $scope.routes, (value, key)->
      stations = []
      angular.forEach value.stations, (value, key)->
        stations.push $stations.getStation(value-1)
      value.stations = stations
]
