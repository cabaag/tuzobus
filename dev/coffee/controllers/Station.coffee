controllers.controller 'StationController', [
  '$scope'
  '$state'
  'station'
  'StationsFactory'
  'PlacesFactory'
  ($scope, $state, station, $stations, $places)->
    $scope.station = station
    $scope.nearestPlaces = $places.getPlacesByStation($scope.station.id)

    $scope.$on 'createRouteToStation', (event, data)->
      $state.go 'app.maker',
        id: $scope.station.id
]
