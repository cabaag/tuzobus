controllers.controller 'InfoController', [
  '$rootScope'
  '$scope'
  ($rootScope, $scope)->
    $rootScope.$broadcast 'hideRightNavButton', false
    $scope.sendComment = (email)->
      window.plugin.email.open
        to: [email]
        cc: ""
        bcc: ""
        subject: 'TuzoApp'
        body: ''
        isHtml: true
      , ->
        console.log "Error"
      , @
    $scope.myModel =
      Url: 'https://www.facebook.com/tuzoapp'
    $scope.connect = ->
      facebookConnectPlugin.login()
      .then (success)->
        $scope.success = success
      , (error)->
        $scope.error = error

]
