controllers.controller 'ConfigController', [
  '$scope'
  '$translate'
  '$localStorage'
  'ConfigurationFactory'
  ($scope, $translate, $localStorage, $config)->
    $scope.selectedLang = $config.getLanguageCode()
    $scope.selectedTheme = $config.getTheme()
    $scope.langs =
    [
      {
        name: 'Español'
        code: 'es'
      }
      {
        name: 'English'
        code: 'en'
      }
      {
        name: 'Italiano'
        code: 'it'
      }
      {
        name: 'Français'
        code: 'fr'
      }
    ]
    $scope.changeLang = (code)->
      $localStorage.languageCode = code
      $config.getLanguageCode(code)
      $translate.use(code)

    $scope.changeTheme = (theme)->
      $config.setTheme(theme)
      $scope.$emit 'changeTheme', theme

]
