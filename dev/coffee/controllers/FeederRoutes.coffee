# View: feeder-routes
controllers.controller 'FeederRoutesController', [
  '$scope'
  '$timeout'
  'FeedersRoutesFactory'
  'StationsFactory'
  ($scope, $timeout, $feedersRoutes, $stations)->
    $scope.routes = $feedersRoutes.getRoutes()
]
