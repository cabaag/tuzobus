controllers.controller 'MapController', [
  '$rootScope'
  '$scope'
  '$ionicSideMenuDelegate'
  '$window'
  '$state'
  '$ionicPlatform'
  '$ionicPopover'
  '$ionicPopup'
  '$ionicLoading'
  '$interval'
  '$localStorage'
  'currentLocation'
  'StationsFactory'
  'RoutesFactory'
  'FeedersRoutesFactory'
  'BusFactory'
  ($rootScope, $scope, $ionicSideMenuDelegate, $window, $state, $ionicPlatform,
  $ionicPopover, $ionicPopup, $ionicLoading, $interval, $localStorage, currentLocation,
  $stations, $routes, $feedersRoutes, $busFactory)->
    map = undefined
    $scope.feedersRoutes = $feedersRoutes.getRoutes()
    $scope.stations = $stations.getStations()
    $scope.stations.visible = true
    $scope.feedersRoutes.visible = true
    $scope.main_route = $routes.getRoutes()

    $scope.bus =
      id: "bus"
      coords:
        longitude: currentLocation.longitude
        latitude: currentLocation.latitude
      icon:
        url: 'img/bus.png'
        # url: 'https://maps.gstatic.com/mapfiles/ms2/micons/blue-dot.png'


    # onMapInit = ->
    #   map.addMarker
    #     position: new plugin.google.maps.LatLng($scope.currentLocation.latitude,
    #       $scope.currentLocation.longitude)
    #
    #   angular.forEach $stations.getStations(), (station, key)->
    #     map.addMarker
    #       position: new plugin.google.maps.LatLng(station.position.latitude,
    #         station.position.longitude)
    #       title: station.name
    #       icon: station.iconx32.url
    #     , (marker)->
    #       marker.showInfoWindow()
    #
    #   angular.forEach $scope.feedersRoutes, (feederRoute, key)->
    #     map.addMarker
    #       position: new plugin.google.maps.LatLng(feederRoute.path[0].latitude,
    #         feederRoute.path[0].longitude)
    #       title: feederRoute.name
    #       icon: feederRoute.stroke.color
    #     , (marker)->
    #       marker.showInfoWindow()
    #
    #     points = []
    #     angular.forEach feederRoute.path, (path, k)->
    #       points.push new plugin.google.maps.LatLng(path.latitude,
    #         path.longitude)
    #     map.addPolyline
    #       points: points
    #       color: feederRoute.stroke.color
    #       width: feederRoute.stroke.weight
    #     , (polyline)->
    #       # console.log polyline
    #       # $scope.$watch '$scope.feedersRoutes[key].visible', (newValue, oldValue)->
    #       #   polyline.setVisibe(newValue)
    #       #   return
    #       # return
    #
    #   points = []
    #   angular.forEach $scope.main_route.path, (path, k)->
    #     points.push new plugin.google.maps.LatLng(path.latitude,
    #       path.longitude)
    #   map.addPolyline
    #     points: points
    #     color: $scope.main_route.stroke.color
    #     width: $scope.main_route.stroke.weight
    #
    #   return
    #
    # $scope.centerMap = ->
    #   console.log "centering"
    #   map.setCenter(new plugin.google.maps.LatLng(
    #     $scope.currentLocation.latitude,
    #     $scope.currentLocation.longitude
    #   ))
    #   return
    #
    # $scope.fullScreen = ->
    #   console.log "full"
    #   # map.remove()
    #   map.showDialog()
    #   return

    $ionicPlatform.ready ->
      if currentLocation isnt "Error"
        $localStorage.currentLocation = currentLocation
        $scope.currentLocation =
          id: "currentLocation"
          coords: currentLocation

        $stations.searchNearestStation($scope.currentLocation.coords)
        .then (success)->
          $ionicLoading.hide()
          $scope.nearestStation = success
        , (error)->
          $scope.nearestStation = $scope.stations[0]
        $ionicLoading.hide()
      else
        currentLocation = $localStorage.currentLocation
        $scope.currentLocation =
          id: "currentLocation"
          coords: currentLocation
        $ionicLoading.hide()
        $ionicPopup.alert
          title: 'Error',
          template: 'No se pudo obtener localización'

      buses = $busFactory.getBuses()
      index = 0

      $interval ->
        if not buses[index]?
          index = 0
        $scope.bus =
          coords:
            latitude: buses[index].lat
            longitude: buses[index].lng
          icon:
            url: 'img/bus.png'

        index++
      , 1000

    #
    #   if $window.plugin
    #     document.getElementsByTagName("ion-side-menu")[0].style.width = "0px"
    #     mapDiv = document.getElementById('map_canvas')
    #     # Initialize the map plugin
    #     map = $window.plugin.google.maps.Map.getMap(mapDiv,
    #       camera:
    #         latLng: new plugin.google.maps.LatLng(
    #           $scope.currentLocation.latitude,
    #           $scope.currentLocation.longitude
    #         )
    #         zoom: 13
    #     )
    #     map.on plugin.google.maps.event.MAP_CLICK, (latLng)->
    #       if $ionicSideMenuDelegate.isOpen() and  window.innerWidth < 768
    #         document.getElementsByTagName("ion-side-menu")[0].style.width= "0px"
    #         $ionicSideMenuDelegate.toggleLeft()
    #     if $localStorage.theme is "theme-light"
    #       map.setBackgroundColor("#fff")
    #     else
    #       map.setBackgroundColor("#444")
    #     # You have to wait the MAP_READY event.
    #     mapDiv.addEventListener "click", ->
    #       if $ionicSideMenuDelegate.isOpen()
    #         document.getElementsByTagName("ion-side-menu")[0].style.width = "0px"
    #     map.on $window.plugin.google.maps.event.MAP_READY, onMapInit
    #   return

    $scope.areVisibleStations = true
    $rootScope.$broadcast 'hideRightNavButton', false
    $rootScope.$broadcast 'iconRightNavButton', 'ion-eye'
    $rootScope.$broadcast 'funcRightNavButton', 'showOptionsMap'

    #
    $scope.map =
      coords: angular.copy currentLocation
      zoom: 14
      options:
        disableDefaultUI: true
        maxZoom: 20
        minZoom: 12
        styles: [{
          featureType: "poi",
          stylers: [{
            visibility: "off"
          }]
        }]
        streetViewControl: false
      pan: true
      control: {}
    $scope.routeTo = []

    $scope.popover = $ionicPopover.fromTemplateUrl 'templates/optionsMap.html',
      scope: $scope
    .then (popover)->
      $scope.popover = popover

    $scope.centerMap = ->
      $scope.map.coords =
        latitude: currentLocation.latitude
        longitude: currentLocation.longitude
      return
    #
    $scope.fullScreen = ->
      map.showDialog()
      return

    $scope.$on 'showOptionsMap', (event, data)->
      $scope.popover.show(data)

    $scope.$on 'viewRoute', (event, data)->
      allVisibles = true
      angular.forEach $scope.feedersRoutes, (value, key)->
        if value.visible is false
          allVisibles = false
      $scope.feedersRoutes.visible = allVisibles
      angular.forEach $scope.feedersRoutes[data].substations, (v, k)->
        v.options.visible = $scope.feedersRoutes[data].visible

    $scope.$on 'toggleRoutes', (event, data)->
      angular.forEach $scope.feedersRoutes, (value, key)->
        value.visible = data
        angular.forEach value.substations, (v, k)->
          v.options.visible = data

    $scope.$on 'toggleStations', (event, data)->
      $scope.main_route.visible = not $scope.main_route.visible
      angular.forEach $scope.stations, (value, key)->
        value.options.visible = data

]

.controller 'OptionsMapController', [
  '$scope'
  ($scope)->
    $scope.viewRoute = (id)->
      $scope.$emit 'viewRoute', id

    $scope.viewStation = ->
      $scope.$emit 'viewStation', true

    $scope.toggleRoutes = ->
      $scope.$emit 'toggleRoutes', $scope.feedersRoutes.visible

    $scope.toggleStations = ->
      $scope.areVisibleStations =  not $scope.areVisibleStations
      $scope.$emit 'toggleStations', $scope.areVisibleStations
]
