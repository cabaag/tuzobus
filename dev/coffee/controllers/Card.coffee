controllers.controller 'CardController', [
  '$rootScope'
  '$scope'
  ($rootScope, $scope)->
    $rootScope.$broadcast 'hideRightNavButton', false
    $rootScope.$broadcast 'iconRightNavButton', "ion-android-sync"
    $rootScope.$broadcast 'funcRightNavButton', "readCard"
    $scope.readCard = ->
      nfc.addNdefListener( (nfcEvent)->
        tag = nfcEvent.tag
        ndefMessage = tag.ndefMessage
        $scope.tag = tag
        $scope.ndefMessage = ndefMessage
      )

    $scope.$on 'readCard', (event, data)->
      $scope.readCard()
]
