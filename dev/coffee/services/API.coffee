services.factory 'API', [
  '$http'
  '$q'
  '$localStorage'
  ($http, $q, $localStorage)->
    base = "http://tuzoapp.bpro.com.mx/api"
    # base = "http://localhost/tuzobus/api"

    _getTweets = (limit = 15)->
      deferred = $q.defer()
      $http.get "#{base}/news/#{limit}"
        .success (data)->
          deferred.resolve(data.data)
        .error (err)->
          deferred.reject(err)
      deferred.promise

    _getPlaces = ->
      deferred = $q.defer()
      $http.get "#{base}/places"
        .success (data)->
          deferred.resolve(data.data)
        .error (err)->
          deferred.reject(err)
      deferred.promise
    {
      getTweets: _getTweets
      getPlaces: _getPlaces
    }


]
