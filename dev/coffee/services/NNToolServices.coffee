services.service 'NNToolService', ->

  @getNearestStation = (coords)->
    x = []
    y = []
    for i in @p
      x.push Math.abs i[0] - coords.latitude
      y.push Math.abs i[1] - coords.longitude

    minX = x.indexOf(Math.min.apply(null, x))
    minY = y.indexOf(Math.min.apply(null, y))
    first = x[minX] + y[minX]
    second = x[minY] + y[minY]
    min = if first < second then minX else minY
  @p =
  [
    [20.0149, -98.8065]
    [20.0256, -98.8076]
    [20.0305, -98.8031]
    [20.0336, -98.8003]
    [20.0428, -98.7935]
    [20.0457, -98.7918]
    [20.0523, -98.7884]
    [20.0551, -98.7870]
    [20.0611, -98.7838]
    [20.0653, -98.7817]
    [20.0687, -98.7798]
    [20.0755, -98.7762]
    [20.0795, -98.7743]
    [20.0825, -98.7729]
    [20.0891, -98.7709]
    [20.0910, -98.7708]
    [20.0949, -98.7692]
    [20.0963, -98.7678]
    [20.1026, -98.7611]
    [20.1077, -98.7559]
    [20.1127, -98.7507]
    [20.1148, -98.7480]
    [20.1157, -98.7449]
    [20.1184, -98.7394]
    [20.1218, -98.7347]
    [20.1250, -98.7328]
    [20.1280, -98.7316]
    [20.1228, -98.7370]
    [20.1204, -98.7405]
    [20.1165, -98.7457]
  ]
  return
