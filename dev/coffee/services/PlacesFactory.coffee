services.factory 'PlacesFactory', [
  '$q'
  ($q)->
    _places = []
    _setPlaces = (places)->
      _places.push.apply _places, places

    _getPlaces = ->
      _places

    _getPlaceById = (id)->
      _places[id-1]

    _getPlacesByStation = (idStation)->
      places = []
      angular.forEach _places, (place, id)->
        if place.stations.indexOf(idStation) isnt -1
          places.push place
      places
    {
      setPlaces: _setPlaces
      getPlaces: _getPlaces
      getPlaceById: _getPlaceById
      getPlacesByStation: _getPlacesByStation
    }
]
