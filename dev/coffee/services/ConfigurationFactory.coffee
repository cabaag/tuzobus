services.factory 'ConfigurationFactory', [
  '$localStorage'
  ($localStorage)->
    _languageCode = $localStorage.languageCode || "es"
    _theme = $localStorage.theme || "theme-light"
    _setLanguageCode = (code)->
      _languageCode = code
    _getLanguageCode = ->
      _languageCode
    _setTheme = (theme)->
      $localStorage.theme = theme
      _theme = theme
    _getTheme = ->
      _theme
    {
      setLanguageCode: _setLanguageCode
      getLanguageCode: _getLanguageCode
      setTheme: _setTheme
      getTheme: _getTheme
    }
]
