services.factory 'RoutesFactory', [
  ->
    _main_route =
      visible: true
      stroke:
        weight: 5
        color: '#1B5E20'
      path: [
        {
        	longitude: -98.8048352
        	latitude: 20.0120225
        }
        {
        	longitude:  -98.8096665
        	latitude: 20.0204082
        }
        {
        	longitude:  -98.8104273
        	latitude: 20.0216813
        }
        {
        	longitude:  -98.8107091
        	latitude: 20.0221969
        }
        {
        	longitude:  -98.8109949
        	latitude: 20.022584
        }
        {
        	longitude:  -98.8072827
        	latitude: 20.0258084
        }
        {
        	longitude:  -98.8070843
        	latitude: 20.0260087
        }
        {
        	longitude:  -98.8060751
        	latitude: 20.026972
        }
        {
        	longitude:  -98.8059282
        	latitude: 20.0271125
        }
        {
        	longitude:  -98.8040581
        	latitude: 20.0292859
        }
        {
        	longitude:  -98.8040527
        	latitude: 20.0292916
        }
        {
        	longitude:  -98.8025661
        	latitude: 20.0309573
        }
        {
        	longitude:  -98.801685
        	latitude: 20.031977900000005
        }
        {
        	longitude:  -98.801237
        	latitude: 20.0324876
        }
        {
        	longitude:  -98.800944
        	latitude: 20.0328221
        }
        {
        	longitude:  -98.8005015
        	latitude: 20.0333191
        }
        {
        	longitude:  -98.7987258
        	latitude: 20.035336300000004
        }
        {
        	longitude:  -98.7985012
        	latitude: 20.0356135
        }
        {
        	longitude:  -98.7980707
        	latitude: 20.0361704
        }
        {
        	longitude:  -98.7959088
        	latitude: 20.039257900000003
        }
        {
        	longitude:  -98.7931636
        	latitude: 20.043264900000004
        }
        {
        	longitude:  -98.7922691
        	latitude: 20.0448899
        }
        {
        	longitude:  -98.7911372
        	latitude: 20.0470616
        }
        {
        	longitude:  -98.7888828
        	latitude: 20.0513544
        }
        {
        	longitude:  -98.788121
        	latitude: 20.0528731
        }
        {
        	longitude:  -98.7877757
        	latitude: 20.0535358
        }
        {
        	longitude:  -98.7873975
        	latitude: 20.0542539
        }
        {
        	longitude:  -98.7866908
        	latitude: 20.0556012
        }
        {
        	longitude:  -98.7863488
        	latitude: 20.0563306
        }
        {
        	longitude:  -98.7861671
        	latitude: 20.0566846
        }
        {
        	longitude:  -98.7859337
        	latitude: 20.056987
        }
        {
        	longitude:  -98.7855448
        	latitude: 20.057712
        }
        {
        	longitude:  -98.785355
        	latitude: 20.0581604
        }
        {
        	longitude:  -98.7846476
        	latitude: 20.0594618
        }
        {
        	longitude:  -98.782416
        	latitude: 20.063909900000002
        }
        {
        	longitude:  -98.7806746
        	latitude: 20.0671989
        }
        {
        	longitude:  -98.7803527
        	latitude: 20.0677249
        }
        {
        	longitude:  -98.7802796
        	latitude: 20.0678439
        }
        {
        	longitude:  -98.7793412
        	latitude: 20.0696635
        }
        {
        	longitude:  -98.77842420000002
        	latitude: 20.0714881
        }
        {
        	longitude:  -98.7765983
        	latitude: 20.0747311
        }
        {
        	longitude:  -98.7765024
        	latitude: 20.0749043
        }
        {
        	longitude:  -98.7738536
        	latitude: 20.0803829
        }
        {
        	longitude:  -98.7727538
        	latitude: 20.0827363
        }
        {
        	longitude:  -98.7725329
        	latitude: 20.0832622
        }
        {
        	longitude:  -98.7710635
        	latitude: 20.086329900000003
        }
        {
        	longitude:  -98.7708423
        	latitude: 20.0868381
        }
        {
        	longitude:  -98.7707645
        	latitude: 20.0871127
        }
        {
        	longitude:  -98.7707189
        	latitude: 20.0874603
        }
        {
        	longitude:  -98.7707779
        	latitude: 20.0887935
        }
        {
        	longitude:  -98.7708919
        	latitude: 20.0898748
        }
        {
        	longitude:  -98.7708731
        	latitude: 20.090337100000003
        }
        {
        	longitude:  -98.7707792
        	latitude: 20.0907993
        }
        {
        	longitude:  -98.77073230000002
        	latitude: 20.0911708
        }
        {
        	longitude:  -98.7707236
        	latitude: 20.0913447
        }
        {
        	longitude:  -98.7706625
        	latitude: 20.0917376
        }
        {
        	longitude:  -98.770515
        	latitude: 20.0924303
        }
        {
        	longitude:  -98.7703548
        	latitude: 20.0930557
        }
        {
        	longitude:  -98.7702803
        	latitude: 20.0933252
        }
        {
        	longitude:  -98.77019180000002
        	latitude: 20.0935084
        }
        {
        	longitude:  -98.7698083
        	latitude: 20.0940941
        }
        {
        	longitude:  -98.7688393
        	latitude: 20.0951571
        }
        {
        	longitude:  -98.7679475
        	latitude: 20.0960633
        }
        {
        	longitude:  -98.7650674
        	latitude: 20.098678500000002
        }
        {
        	longitude:  -98.7648328
        	latitude: 20.0988636
        }
        {
        	longitude:  -98.7641233
        	latitude: 20.0995513
        }
        {
        	longitude:  -98.7637478
        	latitude: 20.1000324
        }
        {
        	longitude:  -98.7630873
        	latitude: 20.1006338
        }
        {
        	longitude:  -98.7627587
        	latitude: 20.1009801
        }
        {
        	longitude:  -98.7613714
        	latitude: 20.1022553
        }
        {
        	longitude:  -98.7605318
        	latitude: 20.1031217
        }
        {
        	longitude:  -98.7578858
        	latitude: 20.1057035
        }
        {
        	longitude:  -98.75771820000001
        	latitude: 20.1058892
        }
        {
        	longitude:  -98.7547108
        	latitude: 20.1087864
        }
        {
        	longitude:  -98.7538223
        	latitude: 20.1096302
        }
        {
        	longitude:  -98.7519561
        	latitude: 20.1113788
        }
        {
        	longitude:  -98.7518857
        	latitude: 20.1114713
        }
        {
        	longitude:  -98.7513801
        	latitude: 20.1119486
        }
        {
        	longitude:  -98.7510489
        	latitude: 20.112323900000003
        }
        {
        	longitude:  -98.7504454
        	latitude: 20.112864800000004
        }
        {
        	longitude:  -98.7498841
        	latitude: 20.1133301
        }
        {
        	longitude:  -98.7489635
        	latitude: 20.1140164
        }
        {
        	longitude:  -98.7479154
        	latitude: 20.114816700000002
        }
        {
        	longitude:  -98.747784
        	latitude: 20.1149036
        }
        {
        	longitude:  -98.746695
        	latitude: 20.1157643
        }
        {
        	longitude:  -98.7465374
        	latitude: 20.1158216
        }
        {
        	longitude:  -98.7463088
        	latitude: 20.11585
        }
        {
        	longitude:  -98.7461109
        	latitude: 20.1158178
        }
        {
        	longitude:  -98.7460593
        	latitude: 20.1157857
        }
        {
        	longitude:  -98.7458843
        	latitude: 20.1156182
        }
        {
        	longitude:  -98.7457093
        	latitude: 20.1155792
        }
        {
        	longitude:  -98.7435675
        	latitude: 20.1157209
        }
        {
        	longitude:  -98.7433134
        	latitude: 20.115741
        }
        {
        	longitude:  -98.7431236
        	latitude: 20.115811500000003
        }
        {
        	longitude:  -98.7425838
        	latitude: 20.1161402
        }
        {
        	longitude:  -98.7408505
        	latitude: 20.1173825
        }
        {
        	longitude:  -98.739331
        	latitude: 20.1184113
        }
        {
        	longitude:  -98.7387409
        	latitude: 20.1188326
        }
        {
        	longitude:  -98.7380295
        	latitude: 20.1194572
        }
        {
        	longitude:  -98.7375513
        	latitude: 20.1197921
        }
        {
        	longitude:  -98.7371269
        	latitude: 20.1200906
        }
        {
        	longitude:  -98.736687
        	latitude: 20.1203796
        }
        {
        	longitude:  -98.7361915
        	latitude: 20.1207064
        }
        {
        	longitude:  -98.7360969
        	latitude: 20.1206787
        }
        {
        	longitude:  -98.7360017
        	latitude: 20.1206963
        }
        {
        	longitude:  -98.7359118
        	latitude: 20.1207561
        }
        {
        	longitude:  -98.735875
        	latitude: 20.1208342
        }
        {
        	longitude:  -98.7358783
        	latitude: 20.1209355
        }
        {
        	longitude:  -98.7355471
        	latitude: 20.1211685
        }
        {
        	longitude:  -98.735307
        	latitude: 20.1213549
        }
        {
        	longitude:  -98.7345848
        	latitude: 20.1218397
        }
        {
        	longitude:  -98.73432
        	latitude: 20.122033
        }
        {
        	longitude:  -98.73414900000002
        	latitude: 20.1221299
        }
        {
        	longitude:  -98.7339746
        	latitude: 20.1221879
        }
        {
        	longitude:  -98.7331478
        	latitude: 20.122339
        }
        {
        	longitude:  -98.7330285
        	latitude: 20.1224095
        }
        {
        	longitude:  -98.7329634
        	latitude: 20.1225209
        }
        {
        	longitude:  -98.7329641
        	latitude: 20.1225826
        }
        {
        	longitude:  -98.7329453
        	latitude: 20.1227086
        }
        {
        	longitude:  -98.7330157
        	latitude: 20.1236133
        }
        {
        	longitude:  -98.7330848
        	latitude: 20.1244507
        }
        {
        	longitude:  -98.7330258
        	latitude: 20.1246673
        }
        {
        	longitude:  -98.73244310000001
        	latitude: 20.1252793
        }
        {
        	longitude:  -98.7320253
        	latitude: 20.1258333
        }
        {
        	longitude:  -98.7319516
        	latitude: 20.1259857
        }
        {
        	longitude:  -98.7318671
        	latitude: 20.1262772
        }
        {
        	longitude:  -98.7317538
        	latitude: 20.1265901
        }
        {
        	longitude:  -98.73166590000001
        	latitude: 20.1267563
        }
        {
        	longitude:  -98.7315888
        	latitude: 20.1270378
        }
        {
        	longitude:  -98.7314191
        	latitude: 20.1276189
        }
        {
        	longitude:  -98.7313601
        	latitude: 20.1276919
        }
        {
        	longitude:  -98.7313052
        	latitude: 20.12786
        }
        {
        	longitude:  -98.73155260000001
        	latitude: 20.1279841
        }
        {
        	longitude:  -98.73193480000002
        	latitude: 20.1281043
        }
        {
        	longitude:  -98.7311321
        	latitude: 20.1295354
        }
        {
        	longitude:  -98.7320736
        	latitude: 20.1276705
        }
        {
        	longitude:  -98.7321487
        	latitude: 20.1275163
        }
        {
        	longitude:  -98.7322392
        	latitude: 20.1273135
        }
        {
        	longitude:  -98.7325075
        	latitude: 20.126923800000004
        }
        {
        	longitude:  -98.7330325
        	latitude: 20.126056800000004
        }
        {
        	longitude:  -98.7330493
        	latitude: 20.1259826
        }
        {
        	longitude:  -98.73304790000002
        	latitude: 20.1258976
        }
        {
        	longitude:  -98.7330184
        	latitude: 20.1256847
        }
        {
        	longitude:  -98.7329453
        	latitude: 20.1256105
        }
        {
        	longitude:  -98.733003
        	latitude: 20.1253542
        }
        {
        	longitude:  -98.7330365
        	latitude: 20.1246805
        }
        {
        	longitude:  -98.7330989
        	latitude: 20.1244463
        }
        {
        	longitude:  -98.7335039
        	latitude: 20.1242625
        }
        {
        	longitude:  -98.7337781
        	latitude: 20.1241113
        }
        {
        	longitude:  -98.7347142
        	latitude: 20.123318
        }
        {
        	longitude:  -98.7349154
        	latitude: 20.1232236
        }
        {
        	longitude:  -98.7353063
        	latitude: 20.1232368
        }
        {
        	longitude:  -98.7363376
        	latitude: 20.1235359
        }
        {
        	longitude:  -98.7364174
        	latitude: 20.1235497
        }
        {
        	longitude:  -98.7364852
        	latitude: 20.123546
        }
        {
        	longitude:  -98.736579
        	latitude: 20.1232677
        }
        {
        	longitude:  -98.7367038
        	latitude: 20.1230769
        }
        {
        	longitude:  -98.7369244
        	latitude: 20.122871
        }
        {
        	longitude:  -98.73800600000001
        	latitude: 20.1221255
        }
        {
        	longitude:  -98.7395154
        	latitude: 20.1211018
        }
        {
        	longitude:  -98.7434167
        	latitude: 20.118371
        }
        {
        	longitude:  -98.7447779
        	latitude: 20.1171993
        }
        {
        	longitude:  -98.7459232
        	latitude: 20.1163386
        }
        {
        	longitude:  -98.7464147
        	latitude: 20.1162529
        }
        {
        	longitude:  -98.7464684
        	latitude: 20.116181100000002
        }
        {
        	longitude:  -98.7465005
        	latitude: 20.1160584
        }
        {
        	longitude:  -98.7466782
        	latitude: 20.115867
        }
        {
        	longitude:  -98.747332
        	latitude: 20.1153998
        }
        {
        	longitude:  -98.7480891
        	latitude: 20.1148425
        }
        {
        	longitude:  -98.74864230000001
        	latitude: 20.1143967
        }
        {
        	longitude:  -98.7487898
        	latitude: 20.1143023
        }
        {
        	longitude:  -98.7492042
        	latitude: 20.1139818
        }
        {
        	longitude:  -98.7500511
        	latitude: 20.1133471
        }
        {
        	longitude:  -98.7506425
        	latitude: 20.112924
        }
        {
        	longitude:  -98.7507559
        	latitude: 20.1127993
        }
        {
        	longitude:  -98.7509047
        	latitude: 20.1126563
        }
        {
        	longitude:  -98.7511428
        	latitude: 20.1123598
        }
        {
        	longitude:  -98.7514438
        	latitude: 20.1120172
        }
        {
        	longitude:  -98.7521741
        	latitude: 20.1113574
        }
        {
        	longitude:  -98.7527655
        	latitude: 20.1108127
        }
        {
        	longitude:  -98.7536473
        	latitude: 20.1099696
        }
        {
        	longitude:  -98.75421120000001
        	latitude: 20.1094136
        }
        {
        	longitude:  -98.754747
        	latitude: 20.1089608
        }
        {
        	longitude:  -98.7651117
        	latitude: 20.0988901
        }
        {
        	longitude:  -98.7667204
        	latitude: 20.0973926
        }
        {
        	longitude:  -98.76735630000002
        	latitude: 20.0968771
        }
        {
        	longitude:  -98.7679251
        	latitude: 20.0963553
        }
        {
        	longitude:  -98.7689546
        	latitude: 20.0952523
        }
        {
        	longitude:  -98.7699529
        	latitude: 20.0942838
        }
        {
        	longitude:  -98.7701189
        	latitude: 20.094107700000002
        }
        {
        	longitude:  -98.7703889
        	latitude: 20.093633
        }
        {
        	longitude:  -98.7706328
        	latitude: 20.0927369
        }
        {
        	longitude:  -98.7706851
        	latitude: 20.0925162
        }
        {
        	longitude:  -98.7708936
        	latitude: 20.0911948
        }
        {
        	longitude:  -98.7710625
        	latitude: 20.0903873
        }
        {
        	longitude:  -98.7711387
        	latitude: 20.0899678
        }
        {
        	longitude:  -98.7711746
        	latitude: 20.0897044
        }
        {
        	longitude:  -98.7712638
        	latitude: 20.0886216
        }
        {
        	longitude:  -98.7718761
        	latitude: 20.0855825
        }
        {
        	longitude:  -98.7725775
        	latitude: 20.0838024
        }
        {
        	longitude:  -98.7751682
        	latitude: 20.0784102
        }
        {
        	longitude:  -98.7760558
        	latitude: 20.0766119
        }
        {
        	longitude:  -98.7769106
        	latitude: 20.0750119
        }
        {
        	longitude:  -98.7773539
        	latitude: 20.0741838
        }
        {
        	longitude:  -98.7785012
        	latitude: 20.0718995
        }
        {
        	longitude:  -98.7788699
        	latitude: 20.0712906
        }
        {
        	longitude:  -98.7793936
        	latitude: 20.0703078
        }
        {
        	longitude:  -98.7799052
        	latitude: 20.0693136
        }
        {
        	longitude:  -98.7808878
        	latitude: 20.0672228
        }
        {
        	longitude:  -98.7812883
        	latitude: 20.0664697
        }
        {
        	longitude:  -98.7819422
        	latitude: 20.0652151
        }
        {
        	longitude:  -98.7838585
        	latitude: 20.0617689
        }
        {
        	longitude:  -98.7842706
        	latitude: 20.0609922
        }
        {
        	longitude:  -98.7850693
        	latitude: 20.0594565
        }
        {
        	longitude:  -98.7867582
        	latitude: 20.0562366
        }
        {
        	longitude:  -98.7872562
        	latitude: 20.0552921
        }
        {
        	longitude:  -98.7879405
        	latitude: 20.0538362
        }
        {
        	longitude:  -98.7890665
        	latitude: 20.051605700000003
        }
        {
        	longitude:  -98.7905028
        	latitude: 20.0488531
        }
        {
        	longitude:  -98.7910379
        	latitude: 20.047827
        }
        {
        	longitude:  -98.7912954
        	latitude: 20.047296
        }
        {
        	longitude:  -98.7914724
        	latitude: 20.0469584
        }
        {
        	longitude:  -98.792206
        	latitude: 20.0456274
        }
        {
        	longitude:  -98.7922429
        	latitude: 20.0455361
        }
        {
        	longitude:  -98.79281460000001
        	latitude: 20.0444651
        }
        {
        	longitude:  -98.793095
        	latitude: 20.043938400000002
        }
        {
        	longitude:  -98.7934077
        	latitude: 20.0434231
        }
        {
        	longitude:  -98.7946576
        	latitude: 20.041510700000003
        }
        {
        	longitude:  -98.798494
        	latitude: 20.0359981
        }
        {
        	longitude:  -98.8020302
        	latitude: 20.031973
        }
        {
        	longitude:  -98.8028487
        	latitude: 20.0310056
        }
        {
        	longitude:  -98.803309
        	latitude: 20.0305006
        }
        {
        	longitude:  -98.8042633
        	latitude: 20.029405
        }
        {
        	longitude:  -98.806091
        	latitude: 20.0273054
        }
        {
        	longitude:  -98.8064791
        	latitude: 20.0268731
        }
        {
        	longitude:  -98.8073544
        	latitude: 20.0260358
        }
        {
        	longitude:  -98.8085435
        	latitude: 20.0250197
        }
        {
        	longitude:  -98.8097084
        	latitude: 20.0240578
        }
        {
        	longitude:  -98.81077150000002
        	latitude: 20.023186
        }
        {
        	longitude:  -98.8112028
        	latitude: 20.0227905
        }
        {
        	longitude:  -98.8110077
        	latitude: 20.022575
        }
        {
        	longitude:  -98.810736
        	latitude: 20.0221901
        }
        {
        	longitude:  -98.8048581
        	latitude: 20.0120149
        }
      ]
    _getRoutes = ->
      _main_route
    _getRoute = (index)->
      # _routes[index]
    _getStations = ->

    {
      getRoutes: _getRoutes
      getRoute: _getRoute
    }
]
