services.factory 'LomaAbetosBFactory', [
  ->
    _loma_abetos_icon = 'img/routes/loma_abetos_van.png'
    _loma_abetos_color = '#cedc39'
    _route =
      id: 21
      name: "La Loma / Abetos B"
      stations: [18]
      visible: true
      icon:
        url: _loma_abetos_icon
      stroke:
        color: _loma_abetos_color
        weight: 5
      path: [
        {
          longitude: -98.8009836
          latitude: 20.1148394
        }
        {
          longitude: -98.8009848
          latitude: 20.1148183
        }
        {
          longitude: -98.800931
          latitude: 20.114806
        }
        {
          longitude: -98.8004908
          latitude: 20.114641700000004
        }
        {
          longitude: -98.7997625
          latitude: 20.1144005
        }
        {
          longitude: -98.7989484
          latitude: 20.1141178
        }
        {
          longitude: -98.7982812
          latitude: 20.1138464
        }
        {
          longitude: -98.7975892
          latitude: 20.113604
        }
        {
          longitude: -98.7964232
          latitude: 20.1131903
        }
        {
          longitude: -98.7962736
          latitude: 20.1131122
        }
        {
          longitude: -98.7957304
          latitude: 20.1129145
        }
        {
          longitude: -98.7950498
          latitude: 20.1126723
        }
        {
          longitude: -98.79437580000001
          latitude: 20.1124314
        }
        {
          longitude: -98.793644
          latitude: 20.112151500000003
        }
        {
          longitude: -98.7929845
          latitude: 20.1119341
        }
        {
          longitude: -98.7926499
          latitude: 20.111803200000004
        }
        {
          longitude: -98.7922282
          latitude: 20.1116571
        }
        {
          longitude: -98.7919814
          latitude: 20.1115853
        }
        {
          longitude: -98.7918641
          latitude: 20.111521700000004
        }
        {
          longitude: -98.791734
          latitude: 20.1114757
        }
        {
          longitude: -98.79153150000002
          latitude: 20.1114027
        }
        {
          longitude: -98.7907891
          latitude: 20.111249
        }
        {
          longitude: -98.789495
          latitude: 20.1109801
        }
        {
          longitude: -98.7890289
          latitude: 20.1108845
        }
        {
          longitude: -98.7885616
          latitude: 20.1107737
        }
        {
          longitude: -98.7880734
          latitude: 20.1106565
        }
        {
          longitude: -98.7877516
          latitude: 20.110591100000004
        }
        {
          longitude: -98.7875765
          latitude: 20.1105633
        }
        {
          longitude: -98.7872493
          latitude: 20.1104978
        }
        {
          longitude: -98.7869281
          latitude: 20.1104104
        }
        {
          longitude: -98.7867639
          latitude: 20.1103656
        }
        {
          longitude: -98.7865003
          latitude: 20.110309000000004
        }
        {
          longitude: -98.7862535
          latitude: 20.1102529
        }
        {
          longitude: -98.7861013
          latitude: 20.1102208
        }
        {
          longitude: -98.785931
          latitude: 20.1101843
        }
        {
          longitude: -98.7857017
          latitude: 20.1101446
        }
        {
          longitude: -98.7855167
          latitude: 20.1100955
        }
        {
          longitude: -98.7851659
          latitude: 20.1099973
        }
        {
          longitude: -98.7847877
          latitude: 20.109896500000005
        }
        {
          longitude: -98.7844525
          latitude: 20.1098134
        }
        {
          longitude: -98.784256
          latitude: 20.1097637
        }
        {
          longitude: -98.7842419
          latitude: 20.1098518
        }
        {
          longitude: -98.7842111
          latitude: 20.1100596
        }
        {
          longitude: -98.7841903
          latitude: 20.1102284
        }
        {
          longitude: -98.7841634
          latitude: 20.1104204
        }
        {
          longitude: -98.7841299
          latitude: 20.1106723
        }
        {
          longitude: -98.7841098
          latitude: 20.110853
        }
        {
          longitude: -98.7840904
          latitude: 20.1110287
        }
        {
          longitude: -98.7840629
          latitude: 20.1112617
        }
        {
          longitude: -98.7840192
          latitude: 20.1116332
        }
        {
          longitude: -98.7839924
          latitude: 20.1118611
        }
        {
          longitude: -98.7839684
          latitude: 20.1120518
        }
        {
          longitude: -98.7839562
          latitude: 20.1121664
        }
        {
          longitude: -98.7840997
          latitude: 20.1121823
        }
        {
          longitude: -98.7843626
          latitude: 20.112208
        }
        {
          longitude: -98.7844632
          latitude: 20.1122175
        }
        {
          longitude: -98.7845148
          latitude: 20.1122238
        }
        {
          longitude: -98.7845162
          latitude: 20.1123163
        }
        {
          longitude: -98.7845175
          latitude: 20.1124841
        }
        {
          longitude: -98.7845202
          latitude: 20.1126217
        }
        {
          longitude: -98.7845195
          latitude: 20.1128386
        }
        {
          longitude: -98.7845215
          latitude: 20.113061300000002
        }
        {
          longitude: -98.7845235
          latitude: 20.1133515
        }
        {
          longitude: -98.7845255
          latitude: 20.1136386
        }
        {
          longitude: -98.7845249
          latitude: 20.113711000000002
        }
        {
          longitude: -98.7845282
          latitude: 20.1137997
        }
        {
          longitude: -98.7845282
          latitude: 20.1139821
        }
        {
          longitude: -98.7845268
          latitude: 20.1143218
        }
        {
          longitude: -98.7845215
          latitude: 20.1144301
        }
        {
          longitude: -98.7845108
          latitude: 20.1144981
        }
        {
          longitude: -98.7844927
          latitude: 20.1145592
        }
        {
          longitude: -98.7844739
          latitude: 20.114607
        }
        {
          longitude: -98.7844538
          latitude: 20.114665000000002
        }
        {
          longitude: -98.7844457
          latitude: 20.1147727
        }
        {
          longitude: -98.784428
          latitude: 20.1149694
        }
        {
          longitude: -98.7844122
          latitude: 20.115158
        }
        {
          longitude: -98.7843727
          latitude: 20.115531400000002
        }
        {
          longitude: -98.7843679
          latitude: 20.1155648
        }
        {
          longitude: -98.7839878
          latitude: 20.1155439
        }
        {
          longitude: -98.783981
          latitude: 20.1155704
        }
        {
          longitude: -98.7839884
          latitude: 20.1156926
        }
        {
          longitude: -98.7839905
          latitude: 20.1160628
        }
        {
          longitude: -98.7839817
          latitude: 20.1163021
        }
        {
          longitude: -98.7839737
          latitude: 20.1165776
        }
        {
          longitude: -98.7839704
          latitude: 20.1168593
        }
        {
          longitude: -98.7839704
          latitude: 20.1169714
        }
        {
          longitude: -98.78381950000002
          latitude: 20.1169859
        }
        {
          longitude: -98.7835358
          latitude: 20.117006
        }
        {
          longitude: -98.7831402
          latitude: 20.1170412
        }
        {
          longitude: -98.7826661
          latitude: 20.1170733
        }
        {
          longitude: -98.782306
          latitude: 20.117088500000005
        }
        {
          longitude: -98.7818923
          latitude: 20.117111800000004
        }
        {
          longitude: -98.7816684
          latitude: 20.1171275
        }
        {
          longitude: -98.7814691
          latitude: 20.116958100000005
        }
        {
          longitude: -98.7812392
          latitude: 20.1169185
        }
        {
          longitude: -98.7810347
          latitude: 20.1167522
        }
        {
          longitude: -98.7807953
          latitude: 20.1165646
        }
        {
          longitude: -98.7804828
          latitude: 20.1163171
        }
        {
          longitude: -98.7802491
          latitude: 20.1161238
        }
        {
          longitude: -98.78012000000001
          latitude: 20.1160225
        }
        {
          longitude: -98.7798672
          latitude: 20.1158191
        }
        {
          longitude: -98.77960000000002
          latitude: 20.1156135
        }
        {
          longitude: -98.7793543
          latitude: 20.1154067
        }
        {
          longitude: -98.7791954
          latitude: 20.1152656
        }
        {
          longitude: -98.7790572
          latitude: 20.1151404
        }
        {
          longitude: -98.7789318
          latitude: 20.1150244
        }
        {
          longitude: -98.7786072
          latitude: 20.1147364
        }
        {
          longitude: -98.7784262
          latitude: 20.1145678
        }
        {
          longitude: -98.7782532
          latitude: 20.1144049
        }
        {
          longitude: -98.7780111
          latitude: 20.114185500000005
        }
        {
          longitude: -98.7778327
          latitude: 20.1140215
        }
        {
          longitude: -98.7777207
          latitude: 20.113905
        }
        {
          longitude: -98.7775464
          latitude: 20.1137454
        }
        {
          longitude: -98.7773815
          latitude: 20.1135845
        }
        {
          longitude: -98.7769892
          latitude: 20.11323
        }
        {
          longitude: -98.776785
          latitude: 20.1130414
        }
        {
          longitude: -98.7765896
          latitude: 20.1128584
        }
        {
          longitude: -98.77641890000001
          latitude: 20.112691300000005
        }
        {
          longitude: -98.7762462
          latitude: 20.1125292
        }
        {
          longitude: -98.7760504
          latitude: 20.1123534
        }
        {
          longitude: -98.7758252
          latitude: 20.1121375
        }
        {
          longitude: -98.7755951
          latitude: 20.1119209
        }
        {
          longitude: -98.7754496
          latitude: 20.1117748
        }
        {
          longitude: -98.7752331
          latitude: 20.1115513
        }
        {
          longitude: -98.775011
          latitude: 20.1113422
        }
        {
          longitude: -98.7747502
          latitude: 20.1110916
        }
        {
          longitude: -98.7745316
          latitude: 20.1109015
        }
        {
          longitude: -98.7742379
          latitude: 20.1106272
        }
        {
          longitude: -98.77374500000002
          latitude: 20.1101475
        }
        {
          longitude: -98.7729471
          latitude: 20.1093946
        }
        {
          longitude: -98.77229670000001
          latitude: 20.1087779
        }
        {
          longitude: -98.7719983
          latitude: 20.108498
        }
        {
          longitude: -98.7718086
          latitude: 20.108328
        }
        {
          longitude: -98.7712473
          latitude: 20.1077991
        }
        {
          longitude: -98.77043050000002
          latitude: 20.1070353
        }
        {
          longitude: -98.7692637
          latitude: 20.1059302
        }
        {
          longitude: -98.7676165
          latitude: 20.1043831
        }
        {
          longitude: -98.76599480000002
          latitude: 20.102815800000002
        }
        {
          longitude: -98.7658963
          latitude: 20.1027225
        }
        {
          longitude: -98.7650165
          latitude: 20.101883700000002
        }
        {
          longitude: -98.7632804
          latitude: 20.1002238
        }
        {
          longitude: -98.7631062
          latitude: 20.1004329
        }
        {
          longitude: -98.7627367
          latitude: 20.1007855
        }
        {
          longitude: -98.7622599
          latitude: 20.101296900000005
        }
        {
          longitude: -98.7617732
          latitude: 20.1017358
        }
        {
          longitude: -98.7616215
          latitude: 20.1018932
        }
        {
          longitude: -98.76142770000001
          latitude: 20.1020959
        }
        {
          longitude: -98.7613714
          latitude: 20.1022553
        }
        {
          longitude: -98.7605194
          latitude: 20.1031151
        }
        {
          longitude: -98.7606459
          latitude: 20.1031998
        }
        {
          longitude: -98.76137950000002
          latitude: 20.1024669
        }
        {
          longitude: -98.7614908
          latitude: 20.1024196
        }
        {
          longitude: -98.76166640000001
          latitude: 20.1023043
        }
        {
          longitude: -98.7630256
          latitude: 20.1010242
        }
        {
          longitude: -98.7632885
          latitude: 20.1010185
        }
        {
          longitude: -98.7635829
          latitude: 20.1010494
        }
        {
          longitude: -98.7638564
          latitude: 20.1011369
        }
        {
          longitude: -98.7648965
          latitude: 20.1020884
        }
        {
          longitude: -98.7651198
          latitude: 20.1021961
        }
        {
          longitude: -98.7655342
          latitude: 20.1026041
        }
        {
          longitude: -98.7658111
          latitude: 20.1028277
        }
        {
          longitude: -98.7690864
          latitude: 20.1059687
        }
        {
          longitude: -98.7703146
          latitude: 20.1071203
        }
        {
          longitude: -98.77100150000001
          latitude: 20.1077805
        }
        {
          longitude: -98.7716372
          latitude: 20.1083875
        }
        {
          longitude: -98.7718782
          latitude: 20.1086032
        }
        {
          longitude: -98.7721324
          latitude: 20.1088519
        }
        {
          longitude: -98.7728271
          latitude: 20.1094806
        }
        {
          longitude: -98.7736311
          latitude: 20.1102648
        }
        {
          longitude: -98.7742332
          latitude: 20.1108429
        }
        {
          longitude: -98.774423
          latitude: 20.1110048
        }
        {
          longitude: -98.7745169
          latitude: 20.1111068
        }
        {
          longitude: -98.77468250000001
          latitude: 20.1112692
        }
        {
          longitude: -98.7748723
          latitude: 20.1114474
        }
        {
          longitude: -98.7752129
          latitude: 20.1117623
        }
        {
          longitude: -98.7754704
          latitude: 20.1119924
        }
        {
          longitude: -98.7757239
          latitude: 20.112215
        }
        {
          longitude: -98.7759251
          latitude: 20.1124064
        }
        {
          longitude: -98.77614970000002
          latitude: 20.1126239
        }
        {
          longitude: -98.7763193
          latitude: 20.1127955
        }
        {
          longitude: -98.77649
          latitude: 20.1129592
        }
        {
          longitude: -98.7766673
          latitude: 20.1131223
        }
        {
          longitude: -98.7768343
          latitude: 20.1132857
        }
        {
          longitude: -98.7769912
          latitude: 20.113439
        }
        {
          longitude: -98.7776564
          latitude: 20.114056000000005
        }
        {
          longitude: -98.7778743
          latitude: 20.1142695
        }
        {
          longitude: -98.77810710000001
          latitude: 20.1144723
        }
        {
          longitude: -98.7783833
          latitude: 20.1147346
        }
        {
          longitude: -98.7786623
          latitude: 20.1149943
        }
        {
          longitude: -98.779023
          latitude: 20.1153161
        }
        {
          longitude: -98.7792161
          latitude: 20.1154873
        }
        {
          longitude: -98.7793924
          latitude: 20.1156207
        }
        {
          longitude: -98.7795671
          latitude: 20.1157609
        }
        {
          longitude: -98.77982300000001
          latitude: 20.1159664
        }
        {
          longitude: -98.7801063
          latitude: 20.1161903
        }
        {
          longitude: -98.7803902
          latitude: 20.116417200000004
        }
        {
          longitude: -98.7807275
          latitude: 20.1166874
        }
        {
          longitude: -98.7814604
          latitude: 20.1172661
        }
        {
          longitude: -98.7825353
          latitude: 20.1181525
        }
        {
          longitude: -98.7828183
          latitude: 20.1183764
        }
        {
          longitude: -98.7830946
          latitude: 20.118604
        }
        {
          longitude: -98.78321390000002
          latitude: 20.1184932
        }
        {
          longitude: -98.7829128
          latitude: 20.1182584
        }
        {
          longitude: -98.7827237
          latitude: 20.1181164
        }
        {
          longitude: -98.7815392
          latitude: 20.1171693
        }
        {
          longitude: -98.7812774
          latitude: 20.1169468
        }
        {
          longitude: -98.7814524
          latitude: 20.1169839
        }
        {
          longitude: -98.7816576
          latitude: 20.1171433
        }
        {
          longitude: -98.7816864
          latitude: 20.1171515
        }
        {
          longitude: -98.7818266
          latitude: 20.1171445
        }
        {
          longitude: -98.7833789
          latitude: 20.1170501
        }
        {
          longitude: -98.78351970000001
          latitude: 20.1170356
        }
        {
          longitude: -98.7836699
          latitude: 20.1170274
        }
        {
          longitude: -98.7839569
          latitude: 20.1170047
        }
        {
          longitude: -98.7839992
          latitude: 20.116994
        }
        {
          longitude: -98.7840005
          latitude: 20.1169185
        }
        {
          longitude: -98.7840045
          latitude: 20.1168845
        }
        {
          longitude: -98.7840065
          latitude: 20.1166446
        }
        {
          longitude: -98.7840072
          latitude: 20.1165659
        }
        {
          longitude: -98.7840099
          latitude: 20.1164796
        }
        {
          longitude: -98.7840153
          latitude: 20.1162523
        }
        {
          longitude: -98.7840206
          latitude: 20.116178600000005
        }
        {
          longitude: -98.7840273
          latitude: 20.1160225
        }
        {
          longitude: -98.7840233
          latitude: 20.1159255
        }
        {
          longitude: -98.7840246
          latitude: 20.115877700000002
        }
        {
          longitude: -98.78401320000002
          latitude: 20.1156082
        }
        {
          longitude: -98.7840159
          latitude: 20.1155761
        }
        {
          longitude: -98.784258
          latitude: 20.1155899
        }
        {
          longitude: -98.78438
          latitude: 20.1155981
        }
        {
          longitude: -98.7844041
          latitude: 20.1155641
        }
        {
          longitude: -98.7844163
          latitude: 20.1154401
        }
        {
          longitude: -98.7844229
          latitude: 20.1153563
        }
        {
          longitude: -98.7844343
          latitude: 20.1152512
        }
        {
          longitude: -98.78445040000001
          latitude: 20.1151473
        }
        {
          longitude: -98.7844705
          latitude: 20.1148558
        }
        {
          longitude: -98.7844839
          latitude: 20.1146833
        }
        {
          longitude: -98.784494
          latitude: 20.1146404
        }
        {
          longitude: -98.7845128
          latitude: 20.114597
        }
        {
          longitude: -98.7845564
          latitude: 20.1144521
        }
        {
          longitude: -98.7845644
          latitude: 20.1143262
        }
        {
          longitude: -98.7845631
          latitude: 20.1141562
        }
        {
          longitude: -98.7845584
          latitude: 20.1139698
        }
        {
          longitude: -98.7845577
          latitude: 20.1137979
        }
        {
          longitude: -98.7845497
          latitude: 20.1135139
        }
        {
          longitude: -98.784551
          latitude: 20.1132709
        }
        {
          longitude: -98.7845483
          latitude: 20.1127659
        }
        {
          longitude: -98.7845524
          latitude: 20.1121866
        }
        {
          longitude: -98.7840012
          latitude: 20.1121362
        }
        {
          longitude: -98.7840575
          latitude: 20.111557
        }
        {
          longitude: -98.7841205
          latitude: 20.1110205
        }
        {
          longitude: -98.784201
          latitude: 20.110331600000002
        }
        {
          longitude: -98.7842721
          latitude: 20.1098216
        }
        {
          longitude: -98.7842841
          latitude: 20.1097989
        }
        {
          longitude: -98.7845725
          latitude: 20.109875700000003
        }
        {
          longitude: -98.7850231
          latitude: 20.1099878
        }
        {
          longitude: -98.7856521
          latitude: 20.1101616
        }
        {
          longitude: -98.7857956
          latitude: 20.1101868
        }
        {
          longitude: -98.7859498
          latitude: 20.1102158
        }
        {
          longitude: -98.7865506
          latitude: 20.1103518
        }
        {
          longitude: -98.7868725
          latitude: 20.1104273
        }
        {
          longitude: -98.7871286
          latitude: 20.1105042
        }
        {
          longitude: -98.787362
          latitude: 20.1105621
        }
        {
          longitude: -98.7877791
          latitude: 20.1106339
        }
        {
          longitude: -98.7881505
          latitude: 20.1107006
        }
        {
          longitude: -98.7884872
          latitude: 20.1107825
        }
        {
          longitude: -98.7890182
          latitude: 20.1109053
        }
        {
          longitude: -98.7894916
          latitude: 20.1110028
        }
        {
          longitude: -98.7907818
          latitude: 20.1112704
        }
        {
          longitude: -98.7914919
          latitude: 20.1114172
        }
        {
          longitude: -98.7917259
          latitude: 20.111494
        }
        {
          longitude: -98.7918594
          latitude: 20.1115453
        }
        {
          longitude: -98.7919686
          latitude: 20.1116042
        }
        {
          longitude: -98.7922201
          latitude: 20.1116782
        }
        {
          longitude: -98.79263380000002
          latitude: 20.1118209
        }
        {
          longitude: -98.7929738
          latitude: 20.1119536
        }
        {
          longitude: -98.7936373
          latitude: 20.1121771
        }
        {
          longitude: -98.7943664
          latitude: 20.1124514
        }
        {
          longitude: -98.7950432
          latitude: 20.112692900000003
        }
        {
          longitude: -98.7957077
          latitude: 20.112932100000002
        }
        {
          longitude: -98.7962542
          latitude: 20.1131273
        }
        {
          longitude: -98.7964171
          latitude: 20.113211700000004
        }
        {
          longitude: -98.7975504
          latitude: 20.1136172
        }
        {
          longitude: -98.79827660000001
          latitude: 20.1138656
        }
        {
          longitude: -98.7989357
          latitude: 20.1141354
        }
        {
          longitude: -98.7997545
          latitude: 20.1144225
        }
        {
          longitude: -98.8004783
          latitude: 20.1146615
        }
        {
          longitude: -98.8009219
          latitude: 20.1148268
        }
        {
          longitude: -98.8009836
          latitude: 20.1148394
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7962636
            latitude: 20.0890039
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
