services.factory 'MatildeBFactory', [
  ->
    _matilde_icon = 'img/routes/matilde_van.png'
    _matilde_color = '#F4EB37'
    _route =
      id: 2
      name: "Matilde B"
      stations: [3]
      visible: true
      icon:
        url: _matilde_icon
      stroke:
        color: _matilde_color
        weight: 5
      path: [
        {
        	longitude: -98.8124829
        	latitude: 20.0338282
        }
        {
        	longitude: -98.8124735
        	latitude: 20.0338449
        }
        {
        	longitude: -98.8125446
        	latitude: 20.0338805
        }
        {
        	longitude: -98.8130019
        	latitude: 20.0341388
        }
        {
        	longitude: -98.8130186
        	latitude: 20.0336152
        }
        {
        	longitude: -98.8130049
        	latitude: 20.0329348
        }
        {
        	longitude: -98.8130059
        	latitude: 20.0327131
        }
        {
        	longitude: -98.8130338
        	latitude: 20.0322957
        }
        {
        	longitude: -98.8130475
        	latitude: 20.0317914
        }
        {
        	longitude: -98.81303
        	latitude: 20.0315861
        }
        {
        	longitude: -98.8130348
        	latitude: 20.0313552
        }
        {
        	longitude: -98.8130073
        	latitude: 20.0304168
        }
        {
        	longitude: -98.8129771
        	latitude: 20.0295884
        }
        {
        	longitude: -98.8129691
        	latitude: 20.0291663
        }
        {
        	longitude: -98.8129509
        	latitude: 20.0291348
        }
        {
        	longitude: -98.8126431
        	latitude: 20.0291952
        }
        {
        	longitude: -98.8123387
        	latitude: 20.0292633
        }
        {
        	longitude: -98.8113292
        	latitude: 20.0297134
        }
        {
        	longitude: -98.8107468
        	latitude: 20.0301472
        }
        {
        	longitude: -98.81051550000001
        	latitude: 20.0302215
        }
        {
        	longitude: -98.8090724
        	latitude: 20.0305371
        }
        {
        	longitude: -98.8088739
        	latitude: 20.0306039
        }
        {
        	longitude: -98.8074745
        	latitude: 20.031155200000004
        }
        {
        	longitude: -98.8085119
        	latitude: 20.0317303
        }
        {
        	longitude: -98.80881
        	latitude: 20.0318806
        }
        {
        	longitude: -98.8090282
        	latitude: 20.0319887
        }
        {
        	longitude: -98.80884180000001
        	latitude: 20.0323203
        }
        {
        	longitude: -98.80872650000002
        	latitude: 20.0324724
        }
        {
        	longitude: -98.8081853
        	latitude: 20.0335075
        }
        {
        	longitude: -98.8078178
        	latitude: 20.034118
        }
        {
        	longitude: -98.8075215
        	latitude: 20.033881
        }
        {
        	longitude: -98.8067416
        	latitude: 20.0332826
        }
        {
        	longitude: -98.8048211
        	latitude: 20.0317146
        }
        {
        	longitude: -98.803309
        	latitude: 20.0305006
        }
        {
        	longitude: -98.8042693
        	latitude: 20.0294359
        }
        {
        	longitude: -98.8071238
        	latitude: 20.0309586
        }
        {
        	longitude: -98.8075013
        	latitude: 20.0311677
        }
        {
        	longitude: -98.8088766
        	latitude: 20.030626
        }
        {
        	longitude: -98.8090771
        	latitude: 20.0305586
        }
        {
        	longitude: -98.8104625
        	latitude: 20.0302631
        }
        {
        	longitude: -98.81076830000002
        	latitude: 20.030159800000003
        }
        {
        	longitude: -98.8113517
        	latitude: 20.029726300000004
        }
        {
        	longitude: -98.8123454
        	latitude: 20.0292872
        }
        {
        	longitude: -98.8129409
        	latitude: 20.0291574
        }
        {
        	longitude: -98.8129519
        	latitude: 20.029754
        }
        {
        	longitude: -98.8129764
        	latitude: 20.0303103
        }
        {
        	longitude: -98.8130103
        	latitude: 20.0313548
        }
        {
        	longitude: -98.8130052
        	latitude: 20.0315892
        }
        {
        	longitude: -98.8130213
        	latitude: 20.0317914
        }
        {
        	longitude: -98.8130107
        	latitude: 20.0322709
        }
        {
        	longitude: -98.81297770000002
        	latitude: 20.0327433
        }
        {
        	longitude: -98.8129945
        	latitude: 20.0336228
        }
        {
        	longitude: -98.812975
        	latitude: 20.0340997
        }
        {
        	longitude: -98.8124829
        	latitude: 20.0338282
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.812442
            latitude: 20.0338823
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.81303680000002
            latitude: 20.0331925
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.8130274
            latitude: 20.030266900000004
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.8113014
            latitude: 20.0297074
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.8090195
            latitude: 20.0305352
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.8075047
            latitude: 20.0311205
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.808986
            latitude: 20.0319905
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.8067818
            latitude: 20.0332914
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.8055285
            latitude: 20.0322582
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.8033587
            latitude: 20.0304931
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.8053482
            latitude: 20.0300256
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.8074148
            latitude: 20.0311457
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.8088404
            latitude: 20.0306657
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.8113423
            latitude: 20.0297597
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.812957
            latitude: 20.0303645
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.81295160000002
            latitude: 20.0330344
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
