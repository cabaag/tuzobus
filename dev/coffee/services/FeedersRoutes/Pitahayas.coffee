services.factory 'PitahayasFactory', [
  ->
    _pitahayas_icon = 'img/routes/pitahayas_van.png'
    _pitahayas_color = '#a61b40'
    _route =
      id: 13
      name: "Pitahayas"
      stations: [9]
      visible: true
      icon:
        url: _pitahayas_icon
      stroke:
        color: _pitahayas_color
        weight: 5
      path: [
        {
          longitude: -98.7974839
          latitude: 20.0630671
        }
        {
          longitude: -98.7976288
          latitude: 20.0627257
        }
        {
          longitude: -98.79770120000002
          latitude: 20.0627005
        }
        {
          longitude: -98.7977307
          latitude: 20.0626445
        }
        {
          longitude: -98.79773010000001
          latitude: 20.0625903
        }
        {
          longitude: -98.7976684
          latitude: 20.0625349
        }
        {
          longitude: -98.7975852
          latitude: 20.0625305
        }
        {
          longitude: -98.7975302
          latitude: 20.0625739
        }
        {
          longitude: -98.7975168
          latitude: 20.0626552
        }
        {
          longitude: -98.7975558
          latitude: 20.0627062
        }
        {
          longitude: -98.7973928
          latitude: 20.0630394
        }
        {
          longitude: -98.796853
          latitude: 20.0640717
        }
        {
          longitude: -98.79670740000002
          latitude: 20.064221
        }
        {
          longitude: -98.7966193
          latitude: 20.0642884
        }
        {
          longitude: -98.7964802
          latitude: 20.0643445
        }
        {
          longitude: -98.796331
          latitude: 20.064386100000004
        }
        {
          longitude: -98.7961644
          latitude: 20.0644062
        }
        {
          longitude: -98.795196
          latitude: 20.064369
        }
        {
          longitude: -98.793965
          latitude: 20.064301
        }
        {
          longitude: -98.7931428
          latitude: 20.064267
        }
        {
          longitude: -98.7927505
          latitude: 20.0643602
        }
        {
          longitude: -98.7926982
          latitude: 20.064322400000002
        }
        {
          longitude: -98.7926593
          latitude: 20.0643117
        }
        {
          longitude: -98.7926057
          latitude: 20.064319900000005
        }
        {
          longitude: -98.7925671
          latitude: 20.064342900000003
        }
        {
          longitude: -98.792538
          latitude: 20.0643848
        }
        {
          longitude: -98.7924552
          latitude: 20.0644027
        }
        {
          longitude: -98.7918909
          latitude: 20.0646021
        }
        {
          longitude: -98.7910648
          latitude: 20.0649183
        }
        {
          longitude: -98.7894568
          latitude: 20.0655506
        }
        {
          longitude: -98.7893173
          latitude: 20.0652055
        }
        {
          longitude: -98.7891161
          latitude: 20.0648691
        }
        {
          longitude: -98.78893910000001
          latitude: 20.0647847
        }
        {
          longitude: -98.7882772
          latitude: 20.0633971
        }
        {
          longitude: -98.7876476
          latitude: 20.0620007
        }
        {
          longitude: -98.7861637
          latitude: 20.0589516
        }
        {
          longitude: -98.7852699
          latitude: 20.0570852
        }
        {
          longitude: -98.7852424
          latitude: 20.0569498
        }
        {
          longitude: -98.785239
          latitude: 20.0568244
        }
        {
          longitude: -98.7853027
          latitude: 20.0567054
        }
        {
          longitude: -98.7854174
          latitude: 20.0566115
        }
        {
          longitude: -98.7855582
          latitude: 20.0565813
        }
        {
          longitude: -98.7857003
          latitude: 20.0566109
        }
        {
          longitude: -98.7858197
          latitude: 20.0566884
        }
        {
          longitude: -98.7858847
          latitude: 20.0567923
        }
        {
          longitude: -98.7859337
          latitude: 20.056987
        }
        {
          longitude: -98.7855273
          latitude: 20.0576886
        }
        {
          longitude: -98.7853798
          latitude: 20.0580615
        }
        {
          longitude: -98.7852725
          latitude: 20.0582656
        }
        {
          longitude: -98.7846308
          latitude: 20.0594561
        }
        {
          longitude: -98.78458180000001
          latitude: 20.0594904
        }
        {
          longitude: -98.7845041
          latitude: 20.0595713
        }
        {
          longitude: -98.784207
          latitude: 20.0601439
        }
        {
          longitude: -98.7815348
          latitude: 20.0586964
        }
        {
          longitude: -98.778844
          latitude: 20.0572874
        }
        {
          longitude: -98.7787984
          latitude: 20.0570971
        }
        {
          longitude: -98.7789646
          latitude: 20.0569045
        }
        {
          longitude: -98.7804868
          latitude: 20.0553051
        }
        {
          longitude: -98.7806447
          latitude: 20.0552132
        }
        {
          longitude: -98.7808007
          latitude: 20.0551641
        }
        {
          longitude: -98.7810045
          latitude: 20.0551458
        }
        {
          longitude: -98.7812304
          latitude: 20.0551855
        }
        {
          longitude: -98.78252590000001
          latitude: 20.0558891
        }
        {
          longitude: -98.784036
          latitude: 20.05674
        }
        {
          longitude: -98.7852189
          latitude: 20.0573472
        }
        {
          longitude: -98.7853845
          latitude: 20.057533
        }
        {
          longitude: -98.7855139
          latitude: 20.0578083
        }
        {
          longitude: -98.7858539
          latitude: 20.0585194
        }
        {
          longitude: -98.7861174
          latitude: 20.0590933
        }
        {
          longitude: -98.7875846
          latitude: 20.0622848
        }
        {
          longitude: -98.7878381
          latitude: 20.0628366
        }
        {
          longitude: -98.7888747
          latitude: 20.064971800000002
        }
        {
          longitude: -98.7892254
          latitude: 20.0657333
        }
        {
          longitude: -98.7895674
          latitude: 20.0655891
        }
        {
          longitude: -98.79109630000002
          latitude: 20.0649951
        }
        {
          longitude: -98.792434
          latitude: 20.0644944
        }
        {
          longitude: -98.7925514
          latitude: 20.0644723
        }
        {
          longitude: -98.7925883
          latitude: 20.0645076
        }
        {
          longitude: -98.7926356
          latitude: 20.0645268
        }
        {
          longitude: -98.7926855
          latitude: 20.0645208
        }
        {
          longitude: -98.7927324
          latitude: 20.064494300000003
        }
        {
          longitude: -98.7927592
          latitude: 20.0644465
        }
        {
          longitude: -98.7930596
          latitude: 20.064375
        }
        {
          longitude: -98.7939716
          latitude: 20.0644155
        }
        {
          longitude: -98.7949077
          latitude: 20.064466
        }
        {
          longitude: -98.7961234
          latitude: 20.0645044
        }
        {
          longitude: -98.7962944
          latitude: 20.0645013
        }
        {
          longitude: -98.796401
          latitude: 20.0644767
        }
        {
          longitude: -98.7966773
          latitude: 20.0643832
        }
        {
          longitude: -98.7968335
          latitude: 20.0642972
        }
        {
          longitude: -98.7970414
          latitude: 20.0639376
        }
        {
          longitude: -98.7971661
          latitude: 20.063663
        }
        {
          longitude: -98.7974839
          latitude: 20.0630671
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7974813
            latitude: 20.0627862
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.796983
            latitude: 20.0637612
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.7962153
            latitude: 20.0643829
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.7939991
            latitude: 20.0642865
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.7927599
            latitude: 20.0643413
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.79071610000001
            latitude: 20.065031
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7894554
            latitude: 20.0654977
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.7878273
            latitude: 20.0623365
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7842104
            latitude: 20.0600815
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.78219600000001
            latitude: 20.0590297
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.7802307
            latitude: 20.0579809
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7814484
            latitude: 20.0553309
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7877945
            latitude: 20.0627893
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.7895621
            latitude: 20.065625
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.7906571
            latitude: 20.0651815
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.7927847
            latitude: 20.0644604
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.7939133
            latitude: 20.0644289
        }
        {
          id: 18
          options:
            visible: true
          position:
            longitude: -98.7961455
            latitude: 20.0645183
        }
        {
          id: 19
          options:
            visible: true
          position:
            longitude: -98.7970561
            latitude: 20.0639464
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
