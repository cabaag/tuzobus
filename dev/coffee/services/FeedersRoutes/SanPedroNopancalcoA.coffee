services.factory 'SanPedroNopancalcoAFactory', [
  ->
    _san_pedro_nopancalco_icon = 'img/routes/san_pedro_nopancalco_van.png'
    _san_pedro_nopancalco_color = '#F4EB37'
    _route =
      id: 18
      name: "San Pedro Nopalcalco A"
      stations: [17]
      visible: true
      icon:
        url: _san_pedro_nopancalco_icon
      stroke:
        color: _san_pedro_nopancalco_color
        weight: 5
      path: [
        {
          longitude: -98.79631920000001
          latitude: 20.0890039
        }
        {
          longitude: -98.7962052
          latitude: 20.0889154
        }
        {
          longitude: -98.7958975
          latitude: 20.0887179
        }
        {
          longitude: -98.795359
          latitude: 20.0884214
        }
        {
          longitude: -98.794437
          latitude: 20.0880082
        }
        {
          longitude: -98.7937517
          latitude: 20.087682
        }
        {
          longitude: -98.79393140000002
          latitude: 20.0873243
        }
        {
          longitude: -98.7942398
          latitude: 20.0867148
        }
        {
          longitude: -98.7947569
          latitude: 20.0857852
        }
        {
          longitude: -98.7950794
          latitude: 20.0852001
        }
        {
          longitude: -98.79478290000002
          latitude: 20.0849079
        }
        {
          longitude: -98.7947193
          latitude: 20.0848159
        }
        {
          longitude: -98.7947072
          latitude: 20.0847284
        }
        {
          longitude: -98.7946489
          latitude: 20.0847076
        }
        {
          longitude: -98.7946315
          latitude: 20.0848213
        }
        {
          longitude: -98.794433
          latitude: 20.085180600000005
        }
        {
          longitude: -98.7942318
          latitude: 20.0854577
        }
        {
          longitude: -98.7940789
          latitude: 20.0853796
        }
        {
          longitude: -98.7939985
          latitude: 20.085374
        }
        {
          longitude: -98.79347
          latitude: 20.0863413
        }
        {
          longitude: -98.793222
          latitude: 20.086816
        }
        {
          longitude: -98.79315420000002
          latitude: 20.0869081
        }
        {
          longitude: -98.7929571
          latitude: 20.0872935
        }
        {
          longitude: -98.7925277
          latitude: 20.0881091
        }
        {
          longitude: -98.7922656
          latitude: 20.0885524
        }
        {
          longitude: -98.7920371
          latitude: 20.0889793
        }
        {
          longitude: -98.7916059
          latitude: 20.0887583
        }
        {
          longitude: -98.7911017
          latitude: 20.088496900000003
        }
        {
          longitude: -98.7908562
          latitude: 20.0890083
        }
        {
          longitude: -98.7907047
          latitude: 20.0893295
        }
        {
          longitude: -98.7905894
          latitude: 20.0895738
        }
        {
          longitude: -98.7902414
          latitude: 20.0903044
        }
        {
          longitude: -98.78962110000002
          latitude: 20.0900474
        }
        {
          longitude: -98.7894662
          latitude: 20.0899844
        }
        {
          longitude: -98.7886769
          latitude: 20.0896513
        }
        {
          longitude: -98.78745990000002
          latitude: 20.0891588
        }
        {
          longitude: -98.787014
          latitude: 20.088997
        }
        {
          longitude: -98.7865822
          latitude: 20.0888257
        }
        {
          longitude: -98.7864212
          latitude: 20.0887501
        }
        {
          longitude: -98.7862066
          latitude: 20.0885939
        }
        {
          longitude: -98.7861047
          latitude: 20.0884377
        }
        {
          longitude: -98.785925
          latitude: 20.0881367
        }
        {
          longitude: -98.7859196
          latitude: 20.0879723
        }
        {
          longitude: -98.7859357
          latitude: 20.0871675
        }
        {
          longitude: -98.7859458
          latitude: 20.0866139
        }
        {
          longitude: -98.78599070000001
          latitude: 20.0860875
        }
        {
          longitude: -98.7860745
          latitude: 20.0855786
        }
        {
          longitude: -98.7862194
          latitude: 20.0848581
        }
        {
          longitude: -98.7863615
          latitude: 20.084178
        }
        {
          longitude: -98.78646810000001
          latitude: 20.0835872
        }
        {
          longitude: -98.7861778
          latitude: 20.0835489
        }
        {
          longitude: -98.7860833
          latitude: 20.0839783
        }
        {
          longitude: -98.78600280000002
          latitude: 20.0846755
        }
        {
          longitude: -98.78594650000001
          latitude: 20.0848506
        }
        {
          longitude: -98.7858171
          latitude: 20.0853469
        }
        {
          longitude: -98.785634
          latitude: 20.0864363
        }
        {
          longitude: -98.7856719
          latitude: 20.087216600000005
        }
        {
          longitude: -98.7857044
          latitude: 20.0879239
        }
        {
          longitude: -98.7857895
          latitude: 20.0880397
        }
        {
          longitude: -98.7857829
          latitude: 20.0882186
        }
        {
          longitude: -98.7857573
          latitude: 20.0901362
        }
        {
          longitude: -98.7857473
          latitude: 20.0920323
        }
        {
          longitude: -98.7857326
          latitude: 20.0920827
        }
        {
          longitude: -98.7845216
          latitude: 20.0917357
        }
        {
          longitude: -98.7838604
          latitude: 20.0915323
        }
        {
          longitude: -98.78256290000002
          latitude: 20.0911224
        }
        {
          longitude: -98.7812392
          latitude: 20.0907149
        }
        {
          longitude: -98.7803112
          latitude: 20.090422700000005
        }
        {
          longitude: -98.7791363
          latitude: 20.090065
        }
        {
          longitude: -98.7789305
          latitude: 20.0900008
        }
        {
          longitude: -98.7786267
          latitude: 20.0899398
        }
        {
          longitude: -98.77812720000001
          latitude: 20.0899038
        }
        {
          longitude: -98.7776745
          latitude: 20.0898628
        }
        {
          longitude: -98.7774915
          latitude: 20.0898588
        }
        {
          longitude: -98.7770576
          latitude: 20.0898887
        }
        {
          longitude: -98.7768162
          latitude: 20.0898963
        }
        {
          longitude: -98.7764749
          latitude: 20.0898685
        }
        {
          longitude: -98.7756917
          latitude: 20.0898371
        }
        {
          longitude: -98.7755489
          latitude: 20.0898326
        }
        {
          longitude: -98.77546
          latitude: 20.089837
        }
        {
          longitude: -98.7753792
          latitude: 20.0898534
        }
        {
          longitude: -98.7749615
          latitude: 20.0897634
        }
        {
          longitude: -98.7744223
          latitude: 20.0895769
        }
        {
          longitude: -98.77377390000001
          latitude: 20.0893647
        }
        {
          longitude: -98.7735024
          latitude: 20.0892772
        }
        {
          longitude: -98.7734524
          latitude: 20.0892334
        }
        {
          longitude: -98.7725529
          latitude: 20.0889283
        }
        {
          longitude: -98.7717019
          latitude: 20.0886588
        }
        {
          longitude: -98.7714056
          latitude: 20.0886103
        }
        {
          longitude: -98.7714934
          latitude: 20.0881631
        }
        {
          longitude: -98.7717676
          latitude: 20.0867222
        }
        {
          longitude: -98.77186950000001
          latitude: 20.0862285
        }
        {
          longitude: -98.7720425
          latitude: 20.086135900000002
        }
        {
          longitude: -98.7722309
          latitude: 20.085998
        }
        {
          longitude: -98.7724039
          latitude: 20.0860068
        }
        {
          longitude: -98.7724492
          latitude: 20.0859275
        }
        {
          longitude: -98.772524
          latitude: 20.0859111
        }
        {
          longitude: -98.773501
          latitude: 20.0863142
        }
        {
          longitude: -98.7735841
          latitude: 20.086396
        }
        {
          longitude: -98.7736512
          latitude: 20.0864955
        }
        {
          longitude: -98.7739637
          latitude: 20.0869969
        }
        {
          longitude: -98.7739848
          latitude: 20.0870831
        }
        {
          longitude: -98.7739858
          latitude: 20.0871858
        }
        {
          longitude: -98.7739449
          latitude: 20.0873092
        }
        {
          longitude: -98.773511
          latitude: 20.087893
        }
        {
          longitude: -98.7734017
          latitude: 20.088063400000003
        }
        {
          longitude: -98.7733407
          latitude: 20.0881361
        }
        {
          longitude: -98.7732207
          latitude: 20.0882261
        }
        {
          longitude: -98.7731516
          latitude: 20.0882538
        }
        {
          longitude: -98.7728941
          latitude: 20.0882841
        }
        {
          longitude: -98.7727741
          latitude: 20.088313
        }
        {
          longitude: -98.7726655
          latitude: 20.088453400000002
        }
        {
          longitude: -98.7725616
          latitude: 20.0888181
        }
        {
          longitude: -98.7725066
          latitude: 20.0890076
        }
        {
          longitude: -98.7728298
          latitude: 20.0891103
        }
        {
          longitude: -98.7734078
          latitude: 20.0892998
        }
        {
          longitude: -98.7734997
          latitude: 20.089297900000002
        }
        {
          longitude: -98.7744122
          latitude: 20.089599
        }
        {
          longitude: -98.7749528
          latitude: 20.0897848
        }
        {
          longitude: -98.7754047
          latitude: 20.089881800000004
        }
        {
          longitude: -98.7754865
          latitude: 20.089859400000005
        }
        {
          longitude: -98.7755831
          latitude: 20.0898572
        }
        {
          longitude: -98.7764742
          latitude: 20.0898912
        }
        {
          longitude: -98.7768135
          latitude: 20.0899221
        }
        {
          longitude: -98.7771079
          latitude: 20.0899139
        }
        {
          longitude: -98.7774945
          latitude: 20.0898827
        }
        {
          longitude: -98.7776786
          latitude: 20.0898893
        }
        {
          longitude: -98.77812240000002
          latitude: 20.0899302
        }
        {
          longitude: -98.7786166
          latitude: 20.0899605
        }
        {
          longitude: -98.7788997
          latitude: 20.0900153
        }
        {
          longitude: -98.7799865
          latitude: 20.0903528
        }
        {
          longitude: -98.7808247
          latitude: 20.0906116
        }
        {
          longitude: -98.7815939
          latitude: 20.0908515
        }
        {
          longitude: -98.7825501
          latitude: 20.0911557
        }
        {
          longitude: -98.7838537
          latitude: 20.0915638
        }
        {
          longitude: -98.7845134
          latitude: 20.0917603
        }
        {
          longitude: -98.7857292
          latitude: 20.0921079
        }
        {
          longitude: -98.7856862
          latitude: 20.0923063
        }
        {
          longitude: -98.7855422
          latitude: 20.0927086
        }
        {
          longitude: -98.7850485
          latitude: 20.0938882
        }
        {
          longitude: -98.785172
          latitude: 20.0939298
        }
        {
          longitude: -98.7854254
          latitude: 20.093369900000003
        }
        {
          longitude: -98.7855461
          latitude: 20.0931123
        }
        {
          longitude: -98.7857111
          latitude: 20.0927112
        }
        {
          longitude: -98.7858358
          latitude: 20.092317
        }
        {
          longitude: -98.7858941
          latitude: 20.0920575
        }
        {
          longitude: -98.785915
          latitude: 20.0909781
        }
        {
          longitude: -98.7859223
          latitude: 20.0902142
        }
        {
          longitude: -98.7859964
          latitude: 20.0900855
        }
        {
          longitude: -98.7860826
          latitude: 20.0897401
        }
        {
          longitude: -98.7861201
          latitude: 20.0895984
        }
        {
          longitude: -98.7862059
          latitude: 20.0894504
        }
        {
          longitude: -98.7863615
          latitude: 20.0892275
        }
        {
          longitude: -98.786495
          latitude: 20.0890845
        }
        {
          longitude: -98.7865583
          latitude: 20.0889582
        }
        {
          longitude: -98.7865868
          latitude: 20.0888508
        }
        {
          longitude: -98.7870049
          latitude: 20.0890268
        }
        {
          longitude: -98.7874512
          latitude: 20.0891796
        }
        {
          longitude: -98.7886605
          latitude: 20.0896771
        }
        {
          longitude: -98.78945550000002
          latitude: 20.0900089
        }
        {
          longitude: -98.7902501
          latitude: 20.0903301
        }
        {
          longitude: -98.7906135
          latitude: 20.0895852
        }
        {
          longitude: -98.7908603
          latitude: 20.0890687
        }
        {
          longitude: -98.7913897
          latitude: 20.089302
        }
        {
          longitude: -98.7918131
          latitude: 20.0894711
        }
        {
          longitude: -98.7920518
          latitude: 20.088993800000004
        }
        {
          longitude: -98.7922848
          latitude: 20.088563
        }
        {
          longitude: -98.7925487
          latitude: 20.0881209
        }
        {
          longitude: -98.7929678
          latitude: 20.0873205
        }
        {
          longitude: -98.7937309
          latitude: 20.0876939
        }
        {
          longitude: -98.7944209
          latitude: 20.0880271
        }
        {
          longitude: -98.7953395
          latitude: 20.088444
        }
        {
          longitude: -98.7958767
          latitude: 20.0887337
        }
        {
          longitude: -98.7960152
          latitude: 20.0888231
        }
        {
          longitude: -98.7962299
          latitude: 20.0889638
        }
        {
          longitude: -98.7962944
          latitude: 20.0890165
        }
        {
          longitude: -98.79631920000001
          latitude: 20.0890039
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7962636
            latitude: 20.0890039
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.7953898
            latitude: 20.0884289
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.7937865
            latitude: 20.0876782
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.7942351
            latitude: 20.086767
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.7950452
            latitude: 20.0853135
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.7933949
            latitude: 20.0864395
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7925272
            latitude: 20.0880674
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.7908388
            latitude: 20.0890102
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7902407
            latitude: 20.0902665
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.7887239
            latitude: 20.0896531
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.78662440000001
            latitude: 20.0888225
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7859531
            latitude: 20.0870119
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7864406
            latitude: 20.083933
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.7858861
            latitude: 20.0849154
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.7857084
            latitude: 20.0920449
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.7813291
            latitude: 20.0907238
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.7789613
            latitude: 20.0899907
        }
        {
          id: 18
          options:
            visible: true
          position:
            longitude: -98.7764152
            latitude: 20.0898446
        }
        {
          id: 19
          options:
            visible: true
          position:
            longitude: -98.7746584
            latitude: 20.0896304
        }
        {
          id: 20
          options:
            visible: true
          position:
            longitude: -98.7714491
            latitude: 20.0884862
        }
        {
          id: 21
          options:
            visible: true
          position:
            longitude: -98.7745323
            latitude: 20.0896569
        }
        {
          id: 22
          options:
            visible: true
          position:
            longitude: -98.7757943
            latitude: 20.089878
        }
        {
          id: 23
          options:
            visible: true
          position:
            longitude: -98.77895260000001
            latitude: 20.090053
        }
        {
          id: 24
          options:
            visible: true
          position:
            longitude: -98.7811587
            latitude: 20.090726900000003
        }
        {
          id: 25
          options:
            visible: true
          position:
            longitude: -98.7856916
            latitude: 20.0921123
        }
        {
          id: 26
          options:
            visible: true
          position:
            longitude: -98.7858988
            latitude: 20.0921381
        }
        {
          id: 27
          options:
            visible: true
          position:
            longitude: -98.7866224
            latitude: 20.0888899
        }
        {
          id: 28
          options:
            visible: true
          position:
            longitude: -98.78862530000002
            latitude: 20.0896746
        }
        {
          id: 29
          options:
            visible: true
          position:
            longitude: -98.7902058
            latitude: 20.0903232
        }
        {
          id: 30
          options:
            visible: true
          position:
            longitude: -98.7908529
            latitude: 20.0891059
        }
        {
          id: 31
          options:
            visible: true
          position:
            longitude: -98.79253530000001
            latitude: 20.0881688
        }
        {
          id: 32
          options:
            visible: true
          position:
            longitude: -98.7936974
            latitude: 20.0876978
        }
        {
          id: 33
          options:
            visible: true
          position:
            longitude: -98.7953027
            latitude: 20.0884446
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
