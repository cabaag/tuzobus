services.factory 'LomaAbetosAFactory', [
  ->
    _loma_abetos_icon = 'img/routes/loma_abetos_van.png'
    _loma_abetos_color = '#cedc39'
    _route =
      id: 20
      name: "La Loma / Abetos A"
      stations: [18]
      visible: true
      icon:
        url: _loma_abetos_icon
      stroke:
        color: _loma_abetos_color
        weight: 5
      path: [
        {
          longitude: -98.8070098
          latitude: 20.1021438
        }
        {
          longitude: -98.8070045
          latitude: 20.102118
        }
        {
          longitude: -98.806344
          latitude: 20.102225
        }
        {
          longitude: -98.8049948
          latitude: 20.1024644
        }
        {
          longitude: -98.8042941
          latitude: 20.1026042
        }
        {
          longitude: -98.8036068
          latitude: 20.1027288
        }
        {
          longitude: -98.8029369
          latitude: 20.1028529
        }
        {
          longitude: -98.8023924
          latitude: 20.1029422
        }
        {
          longitude: -98.8014707
          latitude: 20.103122
        }
        {
          longitude: -98.8009031
          latitude: 20.1032464
        }
        {
          longitude: -98.7999536
          latitude: 20.103504
        }
        {
          longitude: -98.7988187
          latitude: 20.1038522
        }
        {
          longitude: -98.7969717
          latitude: 20.1043736
        }
        {
          longitude: -98.7976583
          latitude: 20.106497500000003
        }
        {
          longitude: -98.7967303
          latitude: 20.1067198
        }
        {
          longitude: -98.7962803
          latitude: 20.106847
        }
        {
          longitude: -98.7958009
          latitude: 20.1069427
        }
        {
          longitude: -98.7953489
          latitude: 20.107063
        }
        {
          longitude: -98.7941097
          latitude: 20.1073551
        }
        {
          longitude: -98.7939052
          latitude: 20.1074383
        }
        {
          longitude: -98.7938409
          latitude: 20.1074861
        }
        {
          longitude: -98.7934499
          latitude: 20.1073822
        }
        {
          longitude: -98.7924347
          latitude: 20.107124
        }
        {
          longitude: -98.7922369
          latitude: 20.1070895
        }
        {
          longitude: -98.79142020000002
          latitude: 20.1068873
        }
        {
          longitude: -98.7904613
          latitude: 20.1066096
        }
        {
          longitude: -98.7902313
          latitude: 20.106561100000004
        }
        {
          longitude: -98.7900214
          latitude: 20.1066071
        }
        {
          longitude: -98.7893428
          latitude: 20.1066946
        }
        {
          longitude: -98.78905040000001
          latitude: 20.1067456
        }
        {
          longitude: -98.7872674
          latitude: 20.1069786
        }
        {
          longitude: -98.78570580000002
          latitude: 20.1071933
        }
        {
          longitude: -98.7837105
          latitude: 20.1074669
        }
        {
          longitude: -98.7830088
          latitude: 20.1075705
        }
        {
          longitude: -98.7826634
          latitude: 20.1076149
        }
        {
          longitude: -98.7820861
          latitude: 20.1076782
        }
        {
          longitude: -98.78129220000001
          latitude: 20.1077066
        }
        {
          longitude: -98.7791538
          latitude: 20.1078299
        }
        {
          longitude: -98.7789372
          latitude: 20.1077934
        }
        {
          longitude: -98.7786328
          latitude: 20.1077103
        }
        {
          longitude: -98.7784349
          latitude: 20.1076926
        }
        {
          longitude: -98.7783491
          latitude: 20.1076989
        }
        {
          longitude: -98.7782874
          latitude: 20.1077109
        }
        {
          longitude: -98.7777416
          latitude: 20.1078671
        }
        {
          longitude: -98.7766546
          latitude: 20.1079131
        }
        {
          longitude: -98.7753001
          latitude: 20.1079792
        }
        {
          longitude: -98.7752974
          latitude: 20.1076108
        }
        {
          longitude: -98.7753574
          latitude: 20.1060303
        }
        {
          longitude: -98.7753738
          latitude: 20.1037305
        }
        {
          longitude: -98.77539700000001
          latitude: 20.1028173
        }
        {
          longitude: -98.7739014
          latitude: 20.102837100000002
        }
        {
          longitude: -98.77243050000001
          latitude: 20.1028658
        }
        {
          longitude: -98.772434
          latitude: 20.1029289
        }
        {
          longitude: -98.7695883
          latitude: 20.1030166
        }
        {
          longitude: -98.76958500000002
          latitude: 20.1025862
        }
        {
          longitude: -98.7695495
          latitude: 20.1016867
        }
        {
          longitude: -98.7695139
          latitude: 20.1007881
        }
        {
          longitude: -98.7690002
          latitude: 20.1008302
        }
        {
          longitude: -98.76893320000002
          latitude: 20.1008126
        }
        {
          longitude: -98.7666278
          latitude: 20.1022754
        }
        {
          longitude: -98.7663217
          latitude: 20.1024668
        }
        {
          longitude: -98.7658963
          latitude: 20.1027225
        }
        {
          longitude: -98.7640415
          latitude: 20.1009442
        }
        {
          longitude: -98.7632804
          latitude: 20.1002238
        }
        {
          longitude: -98.763088
          latitude: 20.1004443
        }
        {
          longitude: -98.7622236
          latitude: 20.1013327
        }
        {
          longitude: -98.7618104
          latitude: 20.1017047
        }
        {
          longitude: -98.7614387
          latitude: 20.102083
        }
        {
          longitude: -98.7613714
          latitude: 20.1022553
        }
        {
          longitude: -98.7605194
          latitude: 20.1031151
        }
        {
          longitude: -98.7606459
          latitude: 20.1031998
        }
        {
          longitude: -98.76137950000002
          latitude: 20.1024669
        }
        {
          longitude: -98.7614908
          latitude: 20.1024196
        }
        {
          longitude: -98.76166640000001
          latitude: 20.1023043
        }
        {
          longitude: -98.7630256
          latitude: 20.1010242
        }
        {
          longitude: -98.7632885
          latitude: 20.1010185
        }
        {
          longitude: -98.7635829
          latitude: 20.1010494
        }
        {
          longitude: -98.7638564
          latitude: 20.1011369
        }
        {
          longitude: -98.7641391
          latitude: 20.1013753
        }
        {
          longitude: -98.7648965
          latitude: 20.1020884
        }
        {
          longitude: -98.7651198
          latitude: 20.1021961
        }
        {
          longitude: -98.7655342
          latitude: 20.1026041
        }
        {
          longitude: -98.7658111
          latitude: 20.1028277
        }
        {
          longitude: -98.7690864
          latitude: 20.1059687
        }
        {
          longitude: -98.7692637
          latitude: 20.1059302
        }
        {
          longitude: -98.7676165
          latitude: 20.1043831
        }
        {
          longitude: -98.76599480000002
          latitude: 20.102815800000002
        }
        {
          longitude: -98.7663529
          latitude: 20.1025082
        }
        {
          longitude: -98.766652
          latitude: 20.1023126
        }
        {
          longitude: -98.7689892
          latitude: 20.1008532
        }
        {
          longitude: -98.7694892
          latitude: 20.1008128
        }
        {
          longitude: -98.7695284
          latitude: 20.1016888
        }
        {
          longitude: -98.769567
          latitude: 20.1025825
        }
        {
          longitude: -98.7695636
          latitude: 20.10304
        }
        {
          longitude: -98.7724533
          latitude: 20.1029505
        }
        {
          longitude: -98.7724535
          latitude: 20.1028896
        }
        {
          longitude: -98.7739057
          latitude: 20.1028635
        }
        {
          longitude: -98.77537330000001
          latitude: 20.1028419
        }
        {
          longitude: -98.77535320000001
          latitude: 20.1037304
        }
        {
          longitude: -98.7753237
          latitude: 20.1060434
        }
        {
          longitude: -98.7752751
          latitude: 20.1076131
        }
        {
          longitude: -98.7752794
          latitude: 20.1080001
        }
        {
          longitude: -98.77665400000001
          latitude: 20.1079399
        }
        {
          longitude: -98.7777432
          latitude: 20.1078852
        }
        {
          longitude: -98.7782932
          latitude: 20.1077294
        }
        {
          longitude: -98.7783484
          latitude: 20.1077195
        }
        {
          longitude: -98.7784331
          latitude: 20.1077153
        }
        {
          longitude: -98.7786273
          latitude: 20.1077346
        }
        {
          longitude: -98.7789304
          latitude: 20.1078137
        }
        {
          longitude: -98.7791473
          latitude: 20.1078499
        }
        {
          longitude: -98.7812923
          latitude: 20.1077248
        }
        {
          longitude: -98.7820885
          latitude: 20.1076996
        }
        {
          longitude: -98.7826639
          latitude: 20.1076385
        }
        {
          longitude: -98.7830113
          latitude: 20.107595
        }
        {
          longitude: -98.7837164
          latitude: 20.1074868
        }
        {
          longitude: -98.7857125
          latitude: 20.1072191
        }
        {
          longitude: -98.7872671
          latitude: 20.1070041
        }
        {
          longitude: -98.7890563
          latitude: 20.1067725
        }
        {
          longitude: -98.789342
          latitude: 20.1067228
        }
        {
          longitude: -98.7900293
          latitude: 20.1066317
        }
        {
          longitude: -98.7902328
          latitude: 20.106583000000004
        }
        {
          longitude: -98.7904532
          latitude: 20.1066321
        }
        {
          longitude: -98.792227
          latitude: 20.1071131
        }
        {
          longitude: -98.792428
          latitude: 20.1071496
        }
        {
          longitude: -98.793453
          latitude: 20.1074053
        }
        {
          longitude: -98.7938511
          latitude: 20.1075082
        }
        {
          longitude: -98.7939231
          latitude: 20.1074579
        }
        {
          longitude: -98.7941172
          latitude: 20.1073749
        }
        {
          longitude: -98.7953583
          latitude: 20.1070842
        }
        {
          longitude: -98.7958062
          latitude: 20.1069635
        }
        {
          longitude: -98.7962893
          latitude: 20.1068694
        }
        {
          longitude: -98.7967409
          latitude: 20.1067413
        }
        {
          longitude: -98.797691
          latitude: 20.1065135
        }
        {
          longitude: -98.7970001
          latitude: 20.1043862
        }
        {
          longitude: -98.7988265
          latitude: 20.103875500000004
        }
        {
          longitude: -98.7999677
          latitude: 20.1035252
        }
        {
          longitude: -98.8009098
          latitude: 20.1032663
        }
        {
          longitude: -98.8014807
          latitude: 20.1031456
        }
        {
          longitude: -98.8024016
          latitude: 20.1029633
        }
        {
          longitude: -98.8029439
          latitude: 20.1028751
        }
        {
          longitude: -98.8036108
          latitude: 20.1027497
        }
        {
          longitude: -98.8042997
          latitude: 20.1026284
        }
        {
          longitude: -98.8050003
          latitude: 20.1024878
        }
        {
          longitude: -98.8063484
          latitude: 20.1022484
        }
        {
          longitude: -98.8070098
          latitude: 20.1021438
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7962636
            latitude: 20.0890039
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
