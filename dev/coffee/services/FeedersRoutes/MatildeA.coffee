services.factory 'MatildeAFactory', [
  ->
    _matilde_icon = 'img/routes/matilde_van.png'
    _matilde_color = '#F4EB37'
    _route =
      id: 1
      name: "Matilde A"
      stations: [3]
      visible: true
      icon:
        url: _matilde_icon
      stroke:
        color: _matilde_color
        weight: 5
      path: [
        {
        	longitude: -98.8108501
        	latitude: 20.0366832
        }
        {
        	longitude: -98.8125352
        	latitude: 20.0338799
        }
        {
        	longitude: -98.8124735
        	latitude: 20.0338449
        }
        {
        	longitude: -98.8107738
        	latitude: 20.0329681
        }
        {
        	longitude: -98.8102127
        	latitude: 20.0326561
        }
        {
        	longitude: -98.809991
        	latitude: 20.0325504
        }
        {
        	longitude: -98.8096109
        	latitude: 20.0323105
        }
        {
        	longitude: -98.8090282
        	latitude: 20.0319887
        }
        {
        	longitude: -98.8085119
        	latitude: 20.0317303
        }
        {
        	longitude: -98.8075013
        	latitude: 20.0311677
        }
        {
        	longitude: -98.8071238
        	latitude: 20.0309586
        }
        {
        	longitude: -98.8056651
        	latitude: 20.0301708
        }
        {
        	longitude: -98.8042693
        	latitude: 20.0294359
        }
        {
        	longitude: -98.803309
        	latitude: 20.0305006
        }
        {
        	longitude: -98.8048211
        	latitude: 20.0317146
        }
        {
        	longitude: -98.8067416
        	latitude: 20.0332826
        }
        {
        	longitude: -98.8075215
        	latitude: 20.033881
        }
        {
        	longitude: -98.8078178
        	latitude: 20.034118
        }
        {
        	longitude: -98.8083107
        	latitude: 20.0345703
        }
        {
        	longitude: -98.8085457
        	latitude: 20.0347829
        }
        {
        	longitude: -98.8087968
        	latitude: 20.0349829
        }
        {
        	longitude: -98.8093366
        	latitude: 20.0353826
        }
        {
        	longitude: -98.8098305
        	latitude: 20.0358471
        }
        {
        	longitude: -98.8106275
        	latitude: 20.0364929
        }
        {
        	longitude: -98.8108501
        	latitude: 20.0366832
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
          	longitude: -98.812442
          	latitude: 20.0338823
        }
        {
          id: 2
          options:
            visible: true
          position:
          	longitude: -98.8108246
          	latitude: 20.0365912
        }
        {
          id: 3
          options:
            visible: true
          position:
          	longitude: -98.8093092
          	latitude: 20.035337600000002
        }
        {
          id: 4
          options:
            visible: true
          position:
          	longitude: -98.8067818
          	latitude: 20.0332914
        }
        {
          id: 5
          options:
            visible: true
          position:
          	longitude: -98.8055285
          	latitude: 20.0322582
        }
        {
          id: 6
          position:
          	longitude: -98.8033593
          	latitude: 20.0304937
          options:
            visible: true
        }
        {
          id: 7
          position:
          	longitude: -98.8053475
          	latitude: 20.0300262
          options:
            visible: true
        }
        {
          id: 8
          position:
          	longitude: -98.8074148
          	latitude: 20.0311457
          options:
            visible: true
        }
        {
          id: 9
          position:
          	longitude: -98.8089853
          	latitude: 20.0319899
          options:
            visible: true
        }
        {
          id: 10
          options:
            visible: true
          position:
          	longitude: -98.8107401
          	latitude: 20.0329682
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
