services.factory 'RealJoyasBFactory', [
  ->
    _real_joyas_icon = 'img/routes/real_joyas_van.png'
    _real_joyas_color = '#cedc39'
    _route =
      id: 4
      name: "Real de Joyas B"
      stations: [2, 3]
      visible: true
      icon:
        url: _real_joyas_icon
      stroke:
        color: _real_joyas_color
        weight: 5
      path: [
        {
          longitude: -98.7837403
          latitude: 20.0180345
        }
        {
          longitude: -98.7898538
          latitude: 20.018059
        }
        {
          longitude: -98.7917982
          latitude: 20.0180871
        }
        {
          longitude: -98.791854
          latitude: 20.018115800000004
        }
        {
          longitude: -98.7918996
          latitude: 20.018136500000004
        }
        {
          longitude: -98.7935727
          latitude: 20.0181541
        }
        {
          longitude: -98.7949453
          latitude: 20.0181989
        }
        {
          longitude: -98.7956306
          latitude: 20.0181876
        }
        {
          longitude: -98.79576
          latitude: 20.0181554
        }
        {
          longitude: -98.7959638
          latitude: 20.0181202
        }
        {
          longitude: -98.7967718
          latitude: 20.0180968
        }
        {
          longitude: -98.7991597
          latitude: 20.0181019
        }
        {
          longitude: -98.7993783
          latitude: 20.0181246
        }
        {
          longitude: -98.7999167
          latitude: 20.0181435
        }
        {
          longitude: -98.8016126
          latitude: 20.0181734
        }
        {
          longitude: -98.8024612
          latitude: 20.018189
        }
        {
          longitude: -98.8035217
          latitude: 20.0182279
        }
        {
          longitude: -98.8052694
          latitude: 20.0182503
        }
        {
          longitude: -98.8054374
          latitude: 20.0182575
        }
        {
          longitude: -98.8082604
          latitude: 20.0182877
        }
        {
          longitude: -98.80834550000002
          latitude: 20.0182695
        }
        {
          longitude: -98.8084146
          latitude: 20.0182323
        }
        {
          longitude: -98.8096665
          latitude: 20.0204082
        }
        {
          longitude: -98.8104273
          latitude: 20.0216813
        }
        {
          longitude: -98.8107091
          latitude: 20.0221969
        }
        {
          longitude: -98.8109949
          latitude: 20.022584
        }
        {
          longitude: -98.8104672
          latitude: 20.0230552
        }
        {
          longitude: -98.8091522
          latitude: 20.0241987
        }
        {
          longitude: -98.807675
          latitude: 20.0254644
        }
        {
          longitude: -98.8072827
          latitude: 20.0258084
        }
        {
          longitude: -98.8070843
          latitude: 20.0260087
        }
        {
          longitude: -98.8059282
          latitude: 20.0271125
        }
        {
          longitude: -98.8040581
          latitude: 20.0292859
        }
        {
          longitude: -98.8032787
          latitude: 20.0288834
        }
        {
          longitude: -98.8028669
          latitude: 20.0286676
        }
        {
          longitude: -98.8024685
          latitude: 20.0284392
        }
        {
          longitude: -98.8009326
          latitude: 20.0276007
        }
        {
          longitude: -98.8004411
          latitude: 20.0271245
        }
        {
          longitude: -98.80006690000002
          latitude: 20.0268157
        }
        {
          longitude: -98.7989813
          latitude: 20.0257624
        }
        {
          longitude: -98.7985944
          latitude: 20.0261234
        }
        {
          longitude: -98.7982994
          latitude: 20.0258549
        }
        {
          longitude: -98.7975108
          latitude: 20.026563100000004
        }
        {
          longitude: -98.7973773
          latitude: 20.0266318
        }
        {
          longitude: -98.7966203
          latitude: 20.0272731
        }
        {
          longitude: -98.7958955
          latitude: 20.027993200000004
        }
        {
          longitude: -98.7955179
          latitude: 20.0276096
        }
        {
          longitude: -98.7952725
          latitude: 20.0282169
        }
        {
          longitude: -98.7951827
          latitude: 20.0290397
        }
        {
          longitude: -98.7930637
          latitude: 20.0285023
        }
        {
          longitude: -98.792198
          latitude: 20.0282698
        }
        {
          longitude: -98.7921276
          latitude: 20.0282484
        }
        {
          longitude: -98.7890129
          latitude: 20.0274476
        }
        {
          longitude: -98.78756520000002
          latitude: 20.0270759
        }
        {
          longitude: -98.7875336
          latitude: 20.0251582
        }
        {
          longitude: -98.7875249
          latitude: 20.023553500000002
        }
        {
          longitude: -98.7874927
          latitude: 20.0226463
        }
        {
          longitude: -98.7883933
          latitude: 20.0226198
        }
        {
          longitude: -98.7884275
          latitude: 20.0225997
        }
        {
          longitude: -98.7884449
          latitude: 20.0225291
        }
        {
          longitude: -98.78841470000002
          latitude: 20.0220219
        }
        {
          longitude: -98.7906323
          latitude: 20.0219734
        }
        {
          longitude: -98.7918996
          latitude: 20.021935
        }
        {
          longitude: -98.7919231
          latitude: 20.021976
        }
        {
          longitude: -98.7919821
          latitude: 20.0219816
        }
        {
          longitude: -98.7920377
          latitude: 20.0219545
        }
        {
          longitude: -98.7920404
          latitude: 20.021884600000003
        }
        {
          longitude: -98.7919854
          latitude: 20.0218424
        }
        {
          longitude: -98.7919647
          latitude: 20.0205408
        }
        {
          longitude: -98.7919063
          latitude: 20.0181208
        }
        {
          longitude: -98.7919009
          latitude: 20.0179771
        }
        {
          longitude: -98.78371820000001
          latitude: 20.0179621
        }
        {
          longitude: -98.7836941
          latitude: 20.0180342
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7850164
            latitude: 20.0180754
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.7872507
            latitude: 20.0180962
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.7896848
            latitude: 20.0180824
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.7917856
            latitude: 20.018129
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.7944336
            latitude: 20.0182172
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.7976932
            latitude: 20.0181227
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.8002493
            latitude: 20.0181737
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.8042197
            latitude: 20.0182525
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.8074222
            latitude: 20.0256458
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.8040312
            latitude: 20.0292469
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.8009266
            latitude: 20.027568
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7990303
            latitude: 20.0257832
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7973921
            latitude: 20.0266022
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.7951431
            latitude: 20.029005
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.7902635
            latitude: 20.0277513
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.78761340000001
            latitude: 20.0270703
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.7875524
            latitude: 20.0248911
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.7884382
            latitude: 20.0220541
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.7916884
            latitude: 20.021998
        }
        {
          id: 18
          options:
            visible: true
          position:
            longitude: -98.7919989
            latitude: 20.0206844
        }
        {
          id: 19
          options:
            visible: true
          position:
            longitude: -98.791838
            latitude: 20.0179438
        }
        {
          id: 20
          options:
            visible: true
          position:
            longitude: -98.7897753
            latitude: 20.01795
        }
        {
          id: 21
          options:
            visible: true
          position:
            longitude: -98.7874015
            latitude: 20.0179393
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
