services.factory 'PaseoCamelinasFactory', [
  ->
    _paseo_camelinas_icon = 'img/routes/paseo_camelinas_van.png'
    _paseo_camelinas_color = '#db4436'
    _route =
      id: 14
      name: "Paseo de Camelinas"
      stations: [9]
      visible: true
      icon:
        url: _paseo_camelinas_icon
      stroke:
        color: _paseo_camelinas_color
        weight: 5
      path: [
        {
          longitude: -98.7863059
          latitude: 20.0759982
        }
        {
          longitude: -98.7862864
          latitude: 20.0760058
        }
        {
          longitude: -98.7868195
          latitude: 20.076834
        }
        {
          longitude: -98.787472
          latitude: 20.0778196
        }
        {
          longitude: -98.787531
          latitude: 20.077933
        }
        {
          longitude: -98.7874505
          latitude: 20.078076600000003
        }
        {
          longitude: -98.78737670000001
          latitude: 20.0782561
        }
        {
          longitude: -98.7872936
          latitude: 20.0785839
        }
        {
          longitude: -98.7869999
          latitude: 20.0801524
        }
        {
          longitude: -98.7863669
          latitude: 20.0834248
        }
        {
          longitude: -98.786507
          latitude: 20.0834436
        }
        {
          longitude: -98.7867142
          latitude: 20.0824266
        }
        {
          longitude: -98.7870337
          latitude: 20.0808108
        }
        {
          longitude: -98.7871923
          latitude: 20.0798217
        }
        {
          longitude: -98.787476
          latitude: 20.0783675
        }
        {
          longitude: -98.7876047
          latitude: 20.0780873
        }
        {
          longitude: -98.7877375
          latitude: 20.077865
        }
        {
          longitude: -98.7878508
          latitude: 20.0777252
        }
        {
          longitude: -98.7888191
          latitude: 20.0768138
        }
        {
          longitude: -98.7905182
          latitude: 20.0752248
        }
        {
          longitude: -98.7925467
          latitude: 20.0733216
        }
        {
          longitude: -98.792652
          latitude: 20.0731943
        }
        {
          longitude: -98.7927103
          latitude: 20.073062
        }
        {
          longitude: -98.7927466
          latitude: 20.0728718
        }
        {
          longitude: -98.7927332
          latitude: 20.0727553
        }
        {
          longitude: -98.792489
          latitude: 20.072204500000005
        }
        {
          longitude: -98.7919734
          latitude: 20.0711347
        }
        {
          longitude: -98.7919811
          latitude: 20.0709351
        }
        {
          longitude: -98.7913042
          latitude: 20.0694588
        }
        {
          longitude: -98.7903466
          latitude: 20.0674649
        }
        {
          longitude: -98.7899718
          latitude: 20.0666525
        }
        {
          longitude: -98.7894568
          latitude: 20.0655506
        }
        {
          longitude: -98.7893019
          latitude: 20.065209900000003
        }
        {
          longitude: -98.7891161
          latitude: 20.0648691
        }
        {
          longitude: -98.78893910000001
          latitude: 20.0647847
        }
        {
          longitude: -98.7882772
          latitude: 20.0633971
        }
        {
          longitude: -98.7876476
          latitude: 20.0620007
        }
        {
          longitude: -98.7861637
          latitude: 20.0589516
        }
        {
          longitude: -98.7852699
          latitude: 20.0570852
        }
        {
          longitude: -98.7852397
          latitude: 20.0569441
        }
        {
          longitude: -98.785239
          latitude: 20.0568244
        }
        {
          longitude: -98.7853027
          latitude: 20.0567054
        }
        {
          longitude: -98.7854174
          latitude: 20.0566115
        }
        {
          longitude: -98.7855582
          latitude: 20.0565813
        }
        {
          longitude: -98.7856943
          latitude: 20.056609
        }
        {
          longitude: -98.7858197
          latitude: 20.0566884
        }
        {
          longitude: -98.78588740000001
          latitude: 20.0567993
        }
        {
          longitude: -98.7859337
          latitude: 20.056987
        }
        {
          longitude: -98.7855273
          latitude: 20.0576886
        }
        {
          longitude: -98.7853798
          latitude: 20.0580615
        }
        {
          longitude: -98.7852725
          latitude: 20.0582656
        }
        {
          longitude: -98.7846308
          latitude: 20.0594561
        }
        {
          longitude: -98.78458180000001
          latitude: 20.0594904
        }
        {
          longitude: -98.7845041
          latitude: 20.0595713
        }
        {
          longitude: -98.784207
          latitude: 20.0601439
        }
        {
          longitude: -98.7815348
          latitude: 20.0586964
        }
        {
          longitude: -98.778844
          latitude: 20.0572874
        }
        {
          longitude: -98.7787984
          latitude: 20.0570971
        }
        {
          longitude: -98.7789646
          latitude: 20.0569045
        }
        {
          longitude: -98.7804868
          latitude: 20.0553051
        }
        {
          longitude: -98.7806447
          latitude: 20.0552132
        }
        {
          longitude: -98.7808007
          latitude: 20.0551641
        }
        {
          longitude: -98.7810045
          latitude: 20.0551458
        }
        {
          longitude: -98.7812304
          latitude: 20.0551855
        }
        {
          longitude: -98.78252590000001
          latitude: 20.0558891
        }
        {
          longitude: -98.784036
          latitude: 20.05674
        }
        {
          longitude: -98.7852189
          latitude: 20.0573472
        }
        {
          longitude: -98.7853845
          latitude: 20.057533
        }
        {
          longitude: -98.7855139
          latitude: 20.0578083
        }
        {
          longitude: -98.7858539
          latitude: 20.0585194
        }
        {
          longitude: -98.7861174
          latitude: 20.0590933
        }
        {
          longitude: -98.7878381
          latitude: 20.0628366
        }
        {
          longitude: -98.7882283
          latitude: 20.0636346
        }
        {
          longitude: -98.7882075
          latitude: 20.063715100000003
        }
        {
          longitude: -98.7882377
          latitude: 20.063822
        }
        {
          longitude: -98.7887245
          latitude: 20.0648836
        }
        {
          longitude: -98.7891376
          latitude: 20.065772400000004
        }
        {
          longitude: -98.7895031
          latitude: 20.0665546
        }
        {
          longitude: -98.7903426
          latitude: 20.0683698
        }
        {
          longitude: -98.7910564
          latitude: 20.0698578
        }
        {
          longitude: -98.7915375
          latitude: 20.0709125
        }
        {
          longitude: -98.7918115
          latitude: 20.0714938
        }
        {
          longitude: -98.7918874
          latitude: 20.0715369
        }
        {
          longitude: -98.7919607
          latitude: 20.071707300000003
        }
        {
          longitude: -98.7925252
          latitude: 20.07296
        }
        {
          longitude: -98.7925085
          latitude: 20.0730904
        }
        {
          longitude: -98.7924609
          latitude: 20.0732044
        }
        {
          longitude: -98.7921249
          latitude: 20.0735085
        }
        {
          longitude: -98.7899651
          latitude: 20.075546
        }
        {
          longitude: -98.78941050000002
          latitude: 20.0760751
        }
        {
          longitude: -98.78869240000002
          latitude: 20.0767414
        }
        {
          longitude: -98.7876537
          latitude: 20.0777019
        }
        {
          longitude: -98.7875571
          latitude: 20.0778536
        }
        {
          longitude: -98.7875035
          latitude: 20.077836600000005
        }
        {
          longitude: -98.7868155
          latitude: 20.076788
        }
        {
          longitude: -98.7863763
          latitude: 20.0761097
        }
        {
          longitude: -98.7863059
          latitude: 20.0759982
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7862797
            latitude: 20.0760222
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.7867571
            latitude: 20.0767578
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.7874518
            latitude: 20.0778341
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.7885609
            latitude: 20.0770947
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.79071610000001
            latitude: 20.0750617
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.79133500000002
            latitude: 20.0694814
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7903634
            latitude: 20.0674547
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.7894554
            latitude: 20.0654977
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.78782800000002
            latitude: 20.0623365
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.7842104
            latitude: 20.0600815
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.7821961
            latitude: 20.0590296
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7802307
            latitude: 20.0579809
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7814484
            latitude: 20.055331
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.7877945
            latitude: 20.0627893
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.7887085
            latitude: 20.0648949
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.789446
            latitude: 20.0664721
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.7903238
            latitude: 20.0683767
        }
        {
          id: 18
          options:
            visible: true
          position:
            longitude: -98.7906611
            latitude: 20.0748457
        }
        {
          id: 19
          options:
            visible: true
          position:
            longitude: -98.78847980000002
            latitude: 20.0767811
        }
        {
          id: 20
          options:
            visible: true
          position:
            longitude: -98.7875538
            latitude: 20.0778127
        }
        {
          id: 21
          options:
            visible: true
          position:
            longitude: -98.7868443
            latitude: 20.0768044
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
