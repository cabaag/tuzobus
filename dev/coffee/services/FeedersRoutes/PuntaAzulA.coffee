services.factory 'PuntaAzulAFactory', [
  ->
    _punta_azul_icon = 'img/routes/punta_azul_van.png'
    _punta_azul_color = '#0ba9cc'
    _route =
      id: 23
      name: "Punta Azul B"
      stations: [18, 22]
      visible: true
      icon:
        url: _punta_azul_icon
      stroke:
        color: _punta_azul_color
        weight: 5
      path: [
        {
          longitude: -98.7845503
          latitude: 20.1219681
        }
        {
          longitude: -98.7845503
          latitude: 20.1218554
        }
        {
          longitude: -98.7805284
          latitude: 20.121703
        }
        {
          longitude: -98.7797955
          latitude: 20.121664
        }
        {
          longitude: -98.777458
          latitude: 20.1215513
        }
        {
          longitude: -98.7761235
          latitude: 20.1215267
        }
        {
          longitude: -98.7759854
          latitude: 20.1215135
        }
        {
          longitude: -98.7755354
          latitude: 20.1213549
        }
        {
          longitude: -98.7753249
          latitude: 20.1213253
        }
        {
          longitude: -98.7752947
          latitude: 20.1213007
        }
        {
          longitude: -98.7751968
          latitude: 20.1210829
        }
        {
          longitude: -98.7748381
          latitude: 20.1205534
        }
        {
          longitude: -98.7747871
          latitude: 20.120404800000003
        }
        {
          longitude: -98.7747696
          latitude: 20.120197
        }
        {
          longitude: -98.77457520000002
          latitude: 20.115141
        }
        {
          longitude: -98.774537
          latitude: 20.1148967
        }
        {
          longitude: -98.7744673
          latitude: 20.1147947
        }
        {
          longitude: -98.7731422
          latitude: 20.113689
        }
        {
          longitude: -98.7731074
          latitude: 20.1136185
        }
        {
          longitude: -98.7730363
          latitude: 20.1131223
        }
        {
          longitude: -98.7701368
          latitude: 20.1135064
        }
        {
          longitude: -98.7700557
          latitude: 20.1115557
        }
        {
          longitude: -98.7699524
          latitude: 20.1094469
        }
        {
          longitude: -98.7698599
          latitude: 20.109467
        }
        {
          longitude: -98.7698123
          latitude: 20.1090578
        }
        {
          longitude: -98.7691069
          latitude: 20.1091989
        }
        {
          longitude: -98.7680705
          latitude: 20.1094435
        }
        {
          longitude: -98.7676021
          latitude: 20.1095408
        }
        {
          longitude: -98.7670409
          latitude: 20.1096629
        }
        {
          longitude: -98.7665816
          latitude: 20.1078645
        }
        {
          longitude: -98.7664475
          latitude: 20.1072507
        }
        {
          longitude: -98.7662423
          latitude: 20.1063936
        }
        {
          longitude: -98.7660981
          latitude: 20.1058358
        }
        {
          longitude: -98.7659908
          latitude: 20.1053874
        }
        {
          longitude: -98.76549330000002
          latitude: 20.1031929
        }
        {
          longitude: -98.7654684
          latitude: 20.1031727
        }
        {
          longitude: -98.7653531
          latitude: 20.103270300000002
        }
        {
          longitude: -98.7650963
          latitude: 20.103486300000004
        }
        {
          longitude: -98.7647315
          latitude: 20.1038012
        }
        {
          longitude: -98.7645149
          latitude: 20.1040096
        }
        {
          longitude: -98.7644371
          latitude: 20.1040889
        }
        {
          longitude: -98.7640583
          latitude: 20.1044894
        }
        {
          longitude: -98.7636848
          latitude: 20.1048817
        }
        {
          longitude: -98.7634944
          latitude: 20.1051078
        }
        {
          longitude: -98.7627708
          latitude: 20.1061147
        }
        {
          longitude: -98.7616899
          latitude: 20.1078167
        }
        {
          longitude: -98.7613627
          latitude: 20.1084854
        }
        {
          longitude: -98.7612896
          latitude: 20.1086025
        }
        {
          longitude: -98.7610073
          latitude: 20.1089331
        }
        {
          longitude: -98.7604688
          latitude: 20.1098581
        }
        {
          longitude: -98.75973190000002
          latitude: 20.111062
        }
        {
          longitude: -98.7596125
          latitude: 20.1112931
        }
        {
          longitude: -98.7591364
          latitude: 20.1113945
        }
        {
          longitude: -98.758606
          latitude: 20.1114934
        }
        {
          longitude: -98.7582158
          latitude: 20.111588400000002
        }
        {
          longitude: -98.7563657
          latitude: 20.1119896
        }
        {
          longitude: -98.7561424
          latitude: 20.1120638
        }
        {
          longitude: -98.7559989
          latitude: 20.1120708
        }
        {
          longitude: -98.7557153
          latitude: 20.1121129
        }
        {
          longitude: -98.7557474
          latitude: 20.112465
        }
        {
          longitude: -98.7555939
          latitude: 20.1130448
        }
        {
          longitude: -98.7545907
          latitude: 20.1128276
        }
        {
          longitude: -98.7544815
          latitude: 20.1128251
        }
        {
          longitude: -98.7543433
          latitude: 20.1128824
        }
        {
          longitude: -98.7542589
          latitude: 20.1129404
        }
        {
          longitude: -98.7539337
          latitude: 20.1132029
        }
        {
          longitude: -98.753656
          latitude: 20.1136922
        }
        {
          longitude: -98.7523866
          latitude: 20.1156976
        }
        {
          longitude: -98.7523363
          latitude: 20.1157542
        }
        {
          longitude: -98.7522593
          latitude: 20.1157775
        }
        {
          longitude: -98.7496065
          latitude: 20.1161157
        }
        {
          longitude: -98.7497561
          latitude: 20.1150427
        }
        {
          longitude: -98.7497614
          latitude: 20.1148822
        }
        {
          longitude: -98.749742
          latitude: 20.1147915
        }
        {
          longitude: -98.7492042
          latitude: 20.1139818
        }
        {
          longitude: -98.7500511
          latitude: 20.1133471
        }
        {
          longitude: -98.7506425
          latitude: 20.112924
        }
        {
          longitude: -98.7509047
          latitude: 20.1126563
        }
        {
          longitude: -98.7512112
          latitude: 20.1125436
        }
        {
          longitude: -98.7519139
          latitude: 20.1129378
        }
        {
          longitude: -98.7522137
          latitude: 20.113121
        }
        {
          longitude: -98.7524517
          latitude: 20.1132514
        }
        {
          longitude: -98.7527266
          latitude: 20.1133553
        }
        {
          longitude: -98.7527809
          latitude: 20.1133553
        }
        {
          longitude: -98.7537157
          latitude: 20.1137419
        }
        {
          longitude: -98.7537881
          latitude: 20.1137626
        }
        {
          longitude: -98.75535850000001
          latitude: 20.1140939
        }
        {
          longitude: -98.755614
          latitude: 20.1130581
        }
        {
          longitude: -98.7557675
          latitude: 20.1124675
        }
        {
          longitude: -98.7557622
          latitude: 20.1123428
        }
        {
          longitude: -98.7557454
          latitude: 20.1121368
        }
        {
          longitude: -98.7559895
          latitude: 20.1120972
        }
        {
          longitude: -98.7560834
          latitude: 20.1120997
        }
        {
          longitude: -98.7561478
          latitude: 20.112085300000004
        }
        {
          longitude: -98.756367
          latitude: 20.1120138
        }
        {
          longitude: -98.7582137
          latitude: 20.1116168
        }
        {
          longitude: -98.7586194
          latitude: 20.111521700000004
        }
        {
          longitude: -98.7591237
          latitude: 20.111426
        }
        {
          longitude: -98.7596232
          latitude: 20.1113114
        }
        {
          longitude: -98.759745
          latitude: 20.1110903
        }
        {
          longitude: -98.7604896
          latitude: 20.1098619
        }
        {
          longitude: -98.7610334
          latitude: 20.1089375
        }
        {
          longitude: -98.761311
          latitude: 20.1086076
        }
        {
          longitude: -98.7613693
          latitude: 20.1085232
        }
        {
          longitude: -98.7617053
          latitude: 20.1078437
        }
        {
          longitude: -98.76278560000002
          latitude: 20.1061367
        }
        {
          longitude: -98.76350370000002
          latitude: 20.1051349
        }
        {
          longitude: -98.7636975
          latitude: 20.1049022
        }
        {
          longitude: -98.764362
          latitude: 20.1042111
        }
        {
          longitude: -98.7647456
          latitude: 20.1038182
        }
        {
          longitude: -98.7654631
          latitude: 20.1032099
        }
        {
          longitude: -98.76597
          latitude: 20.1053905
        }
        {
          longitude: -98.7660753
          latitude: 20.1058385
        }
        {
          longitude: -98.7662175
          latitude: 20.1063999
        }
        {
          longitude: -98.7664227
          latitude: 20.107255700000003
        }
        {
          longitude: -98.7665547
          latitude: 20.1078703
        }
        {
          longitude: -98.7670241
          latitude: 20.1096856
        }
        {
          longitude: -98.7676062
          latitude: 20.1095634
        }
        {
          longitude: -98.7676652
          latitude: 20.1095565
        }
        {
          longitude: -98.7680756
          latitude: 20.1094671
        }
        {
          longitude: -98.7691336
          latitude: 20.1092171
        }
        {
          longitude: -98.7697935
          latitude: 20.109083
        }
        {
          longitude: -98.7698404
          latitude: 20.1094822
        }
        {
          longitude: -98.7700195
          latitude: 20.1135247
        }
        {
          longitude: -98.769996
          latitude: 20.1136443
        }
        {
          longitude: -98.7728754
          latitude: 20.1132583
        }
        {
          longitude: -98.7729404
          latitude: 20.1137205
        }
        {
          longitude: -98.7730027
          latitude: 20.1137961
        }
        {
          longitude: -98.77435860000001
          latitude: 20.1149294
        }
        {
          longitude: -98.7743881
          latitude: 20.1150327
        }
        {
          longitude: -98.7744411
          latitude: 20.1167774
        }
        {
          longitude: -98.774466
          latitude: 20.117193
        }
        {
          longitude: -98.7745202
          latitude: 20.1176312
        }
        {
          longitude: -98.7746114
          latitude: 20.1204054
        }
        {
          longitude: -98.7746456
          latitude: 20.1205445
        }
        {
          longitude: -98.7751895
          latitude: 20.1213115
        }
        {
          longitude: -98.7751773
          latitude: 20.1213599
        }
        {
          longitude: -98.7751895
          latitude: 20.1214077
        }
        {
          longitude: -98.775225
          latitude: 20.1214455
        }
        {
          longitude: -98.7752746
          latitude: 20.1214455
        }
        {
          longitude: -98.7753289
          latitude: 20.1214172
        }
        {
          longitude: -98.775976
          latitude: 20.1216678
        }
        {
          longitude: -98.7761262
          latitude: 20.1216829
        }
        {
          longitude: -98.7770764
          latitude: 20.1217027
        }
        {
          longitude: -98.777474
          latitude: 20.1217264
        }
        {
          longitude: -98.7782378
          latitude: 20.1217414
        }
        {
          longitude: -98.7794602
          latitude: 20.1218114
        }
        {
          longitude: -98.7804486
          latitude: 20.1218227
        }
        {
          longitude: -98.7845363
          latitude: 20.1220116
        }
        {
          longitude: -98.7845503
          latitude: 20.1219681
        }
      ]
      substations: []
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
