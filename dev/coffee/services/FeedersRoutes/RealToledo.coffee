services.factory 'RealToledoFactory', [
  ->
    _real_toledo_icon = 'img/routes/real_toledo_van.png'
    _real_toledo_color = '#62af44'
    _route =
      id: 5
      name: "Real de Toledo"
      stations: [4]
      visible: true
      icon:
        url: _real_toledo_icon
      stroke:
        color: _real_toledo_color
        weight: 5
      path: [
        {
          longitude: -98.7797733
          latitude: 20.0264793
        }
        {
          longitude: -98.7808003
          latitude: 20.027012
        }
        {
          longitude: -98.7822343
          latitude: 20.0277223
        }
        {
          longitude: -98.7823121
          latitude: 20.0277834
        }
        {
          longitude: -98.7827767
          latitude: 20.0280272
        }
        {
          longitude: -98.7826815
          latitude: 20.0288563
        }
        {
          longitude: -98.7826096
          latitude: 20.029428
        }
        {
          longitude: -98.7825444
          latitude: 20.0300072
        }
        {
          longitude: -98.7824032
          latitude: 20.0311443
        }
        {
          longitude: -98.78234750000001
          latitude: 20.0315778
        }
        {
          longitude: -98.7822845
          latitude: 20.032192
        }
        {
          longitude: -98.7822101
          latitude: 20.0332309
        }
        {
          longitude: -98.7819721
          latitude: 20.0353558
        }
        {
          longitude: -98.7817461
          latitude: 20.0376004
        }
        {
          longitude: -98.78668
          latitude: 20.0384572
        }
        {
          longitude: -98.7867415
          latitude: 20.0380079
        }
        {
          longitude: -98.7868071
          latitude: 20.0375604
        }
        {
          longitude: -98.7871447
          latitude: 20.0352141
        }
        {
          longitude: -98.7872627
          latitude: 20.0342799
        }
        {
          longitude: -98.787431
          latitude: 20.032468
        }
        {
          longitude: -98.7876342
          latitude: 20.0304924
        }
        {
          longitude: -98.7900204
          latitude: 20.0317121
        }
        {
          longitude: -98.79188350000001
          latitude: 20.03271
        }
        {
          longitude: -98.7971963
          latitude: 20.0354491
        }
        {
          longitude: -98.7979312
          latitude: 20.0358384
        }
        {
          longitude: -98.7979775
          latitude: 20.0357578
        }
        {
          longitude: -98.7962659
          latitude: 20.034904200000003
        }
        {
          longitude: -98.7944505
          latitude: 20.0339564
        }
        {
          longitude: -98.79072820000002
          latitude: 20.031980400000002
        }
        {
          longitude: -98.7874881
          latitude: 20.0303179
        }
        {
          longitude: -98.7871715
          latitude: 20.0301579
        }
        {
          longitude: -98.7875886
          latitude: 20.0293937
        }
        {
          longitude: -98.7877509
          latitude: 20.0290913
        }
        {
          longitude: -98.7877744
          latitude: 20.0290025
        }
        {
          longitude: -98.7877683
          latitude: 20.0284273
        }
        {
          longitude: -98.7875886
          latitude: 20.0273556
        }
        {
          longitude: -98.7874994
          latitude: 20.0273588
        }
        {
          longitude: -98.7876919
          latitude: 20.0284241
        }
        {
          longitude: -98.7876872
          latitude: 20.0289861
        }
        {
          longitude: -98.7876644
          latitude: 20.0290629
        }
        {
          longitude: -98.787081
          latitude: 20.03011
        }
        {
          longitude: -98.7853617
          latitude: 20.0292312
        }
        {
          longitude: -98.7825213
          latitude: 20.0277828
        }
        {
          longitude: -98.7823651
          latitude: 20.0277046
        }
        {
          longitude: -98.7823087
          latitude: 20.027691400000002
        }
        {
          longitude: -98.7822497
          latitude: 20.0277084
        }
        {
          longitude: -98.7813545
          latitude: 20.0272618
        }
        {
          longitude: -98.7805499
          latitude: 20.0268608
        }
        {
          longitude: -98.7797921
          latitude: 20.0264631
        }
        {
          longitude: -98.7797733
          latitude: 20.0264793
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7797713
            latitude: 20.0265001
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.78274250000001
            latitude: 20.0280373
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.7823804
            latitude: 20.0311274
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.7820961
            latitude: 20.0338987
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.781777
            latitude: 20.037049800000002
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.7867323
            latitude: 20.0382455
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7870435
            latitude: 20.0361723
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.7872352
            latitude: 20.0346956
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7874009
            latitude: 20.0331314
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.7876496
            latitude: 20.0305296
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.7890437
            latitude: 20.0312484
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7922449
            latitude: 20.0329273
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7979044
            latitude: 20.0358624
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.7924595
            latitude: 20.03287
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.7900864
            latitude: 20.0316031
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.7872171
            latitude: 20.0301535
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.7876222
            latitude: 20.0274155
        }
        {
          id: 18
          options:
            visible: true
          position:
            longitude: -98.7867484
            latitude: 20.0299122
        }
        {
          id: 19
          options:
            visible: true
          position:
            longitude: -98.7844712
            latitude: 20.0287448
        }
        {
          id: 20
          options:
            visible: true
          position:
            longitude: -98.7828626
            latitude: 20.0279296
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
