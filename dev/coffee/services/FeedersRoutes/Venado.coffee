services.factory 'VenadoFactory', [
  ->
    _venado_icon = 'img/routes/venado_van.png'
    _venado_color = '#774f46'
    _route =
      id: 17
      name: "El Venado"
      stations: [12]
      visible: true
      icon:
        url: _venado_icon
      stroke:
        color: _venado_color
        weight: 5
      path: [
        {
          longitude: -98.7567546
          latitude: 20.0524536
        }
        {
          longitude: -98.7566326
          latitude: 20.0528032
        }
        {
          longitude: -98.7552881
          latitude: 20.0524139
        }
        {
          longitude: -98.7548764
          latitude: 20.0536454
        }
        {
          longitude: -98.7544808
          latitude: 20.0548384
        }
        {
          longitude: -98.7542085
          latitude: 20.0556214
        }
        {
          longitude: -98.7528601
          latitude: 20.0552258
        }
        {
          longitude: -98.7528218
          latitude: 20.0553568
        }
        {
          longitude: -98.7541448
          latitude: 20.055753
        }
        {
          longitude: -98.7551212
          latitude: 20.056045300000005
        }
        {
          longitude: -98.7563838
          latitude: 20.0564119
        }
        {
          longitude: -98.7572951
          latitude: 20.0566934
        }
        {
          longitude: -98.7571844
          latitude: 20.0573139
        }
        {
          longitude: -98.7570839
          latitude: 20.058039500000003
        }
        {
          longitude: -98.7571067
          latitude: 20.0580968
        }
        {
          longitude: -98.7576216
          latitude: 20.0587594
        }
        {
          longitude: -98.7581065
          latitude: 20.0593925
        }
        {
          longitude: -98.7586228
          latitude: 20.0600595
        }
        {
          longitude: -98.7584444
          latitude: 20.0608009
        }
        {
          longitude: -98.759127
          latitude: 20.0609508
        }
        {
          longitude: -98.7589607
          latitude: 20.061614300000002
        }
        {
          longitude: -98.7587609
          latitude: 20.0624958
        }
        {
          longitude: -98.7585437
          latitude: 20.0634872
        }
        {
          longitude: -98.7583525
          latitude: 20.0644276
        }
        {
          longitude: -98.7586489
          latitude: 20.0646203
        }
        {
          longitude: -98.75941200000001
          latitude: 20.0650184
        }
        {
          longitude: -98.7598791
          latitude: 20.0652463
        }
        {
          longitude: -98.7602562
          latitude: 20.065454
        }
        {
          longitude: -98.7611983
          latitude: 20.065923500000004
        }
        {
          longitude: -98.76199030000001
          latitude: 20.0663392
        }
        {
          longitude: -98.7646276
          latitude: 20.0676839
        }
        {
          longitude: -98.7673198
          latitude: 20.069055
        }
        {
          longitude: -98.7675606
          latitude: 20.0691968
        }
        {
          longitude: -98.7681795
          latitude: 20.069497200000004
        }
        {
          longitude: -98.76889700000001
          latitude: 20.0698436
        }
        {
          longitude: -98.7690069
          latitude: 20.0699076
        }
        {
          longitude: -98.7691095
          latitude: 20.0699992
        }
        {
          longitude: -98.769196
          latitude: 20.0700975
        }
        {
          longitude: -98.7692772
          latitude: 20.0702278
        }
        {
          longitude: -98.7693637
          latitude: 20.0704533
        }
        {
          longitude: -98.76965210000002
          latitude: 20.0710649
        }
        {
          longitude: -98.7697191
          latitude: 20.0711716
        }
        {
          longitude: -98.769979
          latitude: 20.0717249
        }
        {
          longitude: -98.7702591
          latitude: 20.072283
        }
        {
          longitude: -98.7704539
          latitude: 20.0727015
        }
        {
          longitude: -98.7705347
          latitude: 20.0728706
        }
        {
          longitude: -98.7707228
          latitude: 20.0732413
        }
        {
          longitude: -98.7708989
          latitude: 20.073569200000005
        }
        {
          longitude: -98.7709559
          latitude: 20.0736626
        }
        {
          longitude: -98.7710202
          latitude: 20.073742200000005
        }
        {
          longitude: -98.7710815
          latitude: 20.0738013
        }
        {
          longitude: -98.7711669
          latitude: 20.0738566
        }
        {
          longitude: -98.7712624
          latitude: 20.0739029
        }
        {
          longitude: -98.7713579
          latitude: 20.0739328
        }
        {
          longitude: -98.7723877
          latitude: 20.0738372
        }
        {
          longitude: -98.772829
          latitude: 20.0737978
        }
        {
          longitude: -98.77301240000001
          latitude: 20.0737996
        }
        {
          longitude: -98.7739895
          latitude: 20.0741173
        }
        {
          longitude: -98.7747536
          latitude: 20.0743678
        }
        {
          longitude: -98.7755592
          latitude: 20.0746135
        }
        {
          longitude: -98.7755978
          latitude: 20.0745103
        }
        {
          longitude: -98.7758672
          latitude: 20.0737816
        }
        {
          longitude: -98.7767721
          latitude: 20.074099
        }
        {
          longitude: -98.7764128
          latitude: 20.0747817
        }
        {
          longitude: -98.7755978
          latitude: 20.0745103
        }
        {
          longitude: -98.7747971
          latitude: 20.0742531
        }
        {
          longitude: -98.77402990000002
          latitude: 20.0739758
        }
        {
          longitude: -98.7733642
          latitude: 20.073764900000004
        }
        {
          longitude: -98.7730959
          latitude: 20.0737648
        }
        {
          longitude: -98.7730074
          latitude: 20.0737791
        }
        {
          longitude: -98.77283640000002
          latitude: 20.073773
        }
        {
          longitude: -98.7723918
          latitude: 20.0738135
        }
        {
          longitude: -98.7713619
          latitude: 20.0739108
        }
        {
          longitude: -98.7712707
          latitude: 20.0738847
        }
        {
          longitude: -98.7711801
          latitude: 20.0738391
        }
        {
          longitude: -98.7711118
          latitude: 20.073796
        }
        {
          longitude: -98.7710454
          latitude: 20.0737296
        }
        {
          longitude: -98.7709845
          latitude: 20.073656400000004
        }
        {
          longitude: -98.770929
          latitude: 20.0735731
        }
        {
          longitude: -98.7707492
          latitude: 20.0732364
        }
        {
          longitude: -98.7705573
          latitude: 20.0728519
        }
        {
          longitude: -98.7704814
          latitude: 20.0726981
        }
        {
          longitude: -98.7702847
          latitude: 20.0722723
        }
        {
          longitude: -98.7700014
          latitude: 20.0717235
        }
        {
          longitude: -98.7697439
          latitude: 20.0711612
        }
        {
          longitude: -98.7696748
          latitude: 20.0710621
        }
        {
          longitude: -98.7693991
          latitude: 20.0704809
        }
        {
          longitude: -98.7693005
          latitude: 20.0702172
        }
        {
          longitude: -98.7692191
          latitude: 20.0700903
        }
        {
          longitude: -98.7691301
          latitude: 20.0699834
        }
        {
          longitude: -98.7690186
          latitude: 20.0698877
        }
        {
          longitude: -98.7689085
          latitude: 20.0698196
        }
        {
          longitude: -98.768669
          latitude: 20.0697092
        }
        {
          longitude: -98.7675816
          latitude: 20.0691807
        }
        {
          longitude: -98.767339
          latitude: 20.0690401
        }
        {
          longitude: -98.7646394
          latitude: 20.0676612
        }
        {
          longitude: -98.76203650000001
          latitude: 20.066334600000005
        }
        {
          longitude: -98.7612012
          latitude: 20.0658959
        }
        {
          longitude: -98.7602735
          latitude: 20.065439
        }
        {
          longitude: -98.7598988
          latitude: 20.0652348
        }
        {
          longitude: -98.7594222
          latitude: 20.0649935
        }
        {
          longitude: -98.7586577
          latitude: 20.064605
        }
        {
          longitude: -98.7583794
          latitude: 20.0644149
        }
        {
          longitude: -98.7585742
          latitude: 20.063479
        }
        {
          longitude: -98.7589834
          latitude: 20.0616247
        }
        {
          longitude: -98.759147
          latitude: 20.0609313
        }
        {
          longitude: -98.7584705
          latitude: 20.0607892
        }
        {
          longitude: -98.7585
          latitude: 20.0606499
        }
        {
          longitude: -98.758637
          latitude: 20.0600625
        }
        {
          longitude: -98.7586747
          latitude: 20.0599311
        }
        {
          longitude: -98.7571708
          latitude: 20.0580373
        }
        {
          longitude: -98.7572906
          latitude: 20.0573191
        }
        {
          longitude: -98.7574048
          latitude: 20.0567238
        }
        {
          longitude: -98.759326
          latitude: 20.057295
        }
        {
          longitude: -98.7593687
          latitude: 20.0571807
        }
        {
          longitude: -98.7581366
          latitude: 20.0567859
        }
        {
          longitude: -98.7570541
          latitude: 20.0564615
        }
        {
          longitude: -98.7575605
          latitude: 20.0549149
        }
        {
          longitude: -98.7581259
          latitude: 20.0532763
        }
        {
          longitude: -98.7582553
          latitude: 20.0529012
        }
        {
          longitude: -98.7567546
          latitude: 20.0524536
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7566587
            latitude: 20.0526079
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.7548731
            latitude: 20.053566
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.754175
            latitude: 20.0555584
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.7543561
            latitude: 20.0558746
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.7572099
            latitude: 20.0566928
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.75858660000002
            latitude: 20.0600645
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7590855
            latitude: 20.0609564
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.758661
            latitude: 20.0628542
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7583398
            latitude: 20.0644005
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.7598546
            latitude: 20.0652552
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.7620211
            latitude: 20.066378300000004
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.767281
            latitude: 20.0690532
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7696688
            latitude: 20.0711304
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.7702287
            latitude: 20.0722439
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.7704848
            latitude: 20.0728322
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.7712386
            latitude: 20.0739186
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.7727869
            latitude: 20.0738172
        }
        {
          id: 18
          options:
            visible: true
          position:
            longitude: -98.77471
            latitude: 20.0743708
        }
        {
          id: 19
          options:
            visible: true
          position:
            longitude: -98.7765694
            latitude: 20.0744407
        }
        {
          id: 20
          options:
            visible: true
          position:
            longitude: -98.7748421
            latitude: 20.0742391
        }
        {
          id: 21
          options:
            visible: true
          position:
            longitude: -98.7729196
            latitude: 20.0737593
        }
        {
          id: 22
          options:
            visible: true
          position:
            longitude: -98.7715758
            latitude: 20.073874500000002
        }
        {
          id: 23
          options:
            visible: true
          position:
            longitude: -98.7705767
            latitude: 20.0728454
        }
        {
          id: 24
          options:
            visible: true
          position:
            longitude: -98.7703796
            latitude: 20.0724347
        }
        {
          id: 25
          options:
            visible: true
          position:
            longitude: -98.7698492
            latitude: 20.0713344
        }
        {
          id: 26
          options:
            visible: true
          position:
            longitude: -98.7673815
            latitude: 20.0690456
        }
        {
          id: 27
          options:
            visible: true
          position:
            longitude: -98.76198830000001
            latitude: 20.0662989
        }
        {
          id: 28
          options:
            visible: true
          position:
            longitude: -98.7599572
            latitude: 20.0652439
        }
        {
          id: 29
          options:
            visible: true
          position:
            longitude: -98.7584183
            latitude: 20.0644169
        }
        {
          id: 30
          options:
            visible: true
          position:
            longitude: -98.7586986
            latitude: 20.0629657
        }
        {
          id: 31
          options:
            visible: true
          position:
            longitude: -98.7591632
            latitude: 20.0609747
        }
        {
          id: 32
          options:
            visible: true
          position:
            longitude: -98.7586449
            latitude: 20.0600853
        }
        {
          id: 33
          options:
            visible: true
          position:
            longitude: -98.757332
            latitude: 20.0581636
        }
        {
          id: 34
          options:
            visible: true
          position:
            longitude: -98.7574104
            latitude: 20.0568433
        }
        {
          id: 35
          options:
            visible: true
          position:
            longitude: -98.7588756
            latitude: 20.0571905
        }
        {
          id: 36
          options:
            visible: true
          position:
            longitude: -98.7580166
            latitude: 20.0567211
        }
        {
          id: 37
          options:
            visible: true
          position:
            longitude: -98.7571026
            latitude: 20.0564484
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
