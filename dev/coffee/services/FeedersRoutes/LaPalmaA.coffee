services.factory 'LaPalmaAFactory', [
  ->
    _palma_icon = 'img/routes/palma_van.png'
    _palma_color = '#009d56'
    _route =
      id: 6
      name: "La Palma A"
      stations: [5]
      visible: true
      icon:
        url: _palma_icon
      stroke:
        color: _palma_color
        weight: 5
      path: [
        {
          longitude: -98.8052852
          latitude: 20.0459352
        }
        {
          longitude: -98.8054904
          latitude: 20.0459875
        }
        {
          longitude: -98.8054957
          latitude: 20.0459623
        }
        {
          longitude: -98.8047232
          latitude: 20.0457998
        }
        {
          longitude: -98.8041533
          latitude: 20.0456858
        }
        {
          longitude: -98.8040346
          latitude: 20.0456694
        }
        {
          longitude: -98.8024246
          latitude: 20.0453255
        }
        {
          longitude: -98.8010674
          latitude: 20.0450256
        }
        {
          longitude: -98.7998363
          latitude: 20.0447743
        }
        {
          longitude: -98.7996941
          latitude: 20.0447327
        }
        {
          longitude: -98.7988915
          latitude: 20.0445758
        }
        {
          longitude: -98.7972841
          latitude: 20.044240100000003
        }
        {
          longitude: -98.7968402
          latitude: 20.0441551
        }
        {
          longitude: -98.7960302
          latitude: 20.0439623
        }
        {
          longitude: -98.7954019
          latitude: 20.0438212
        }
        {
          longitude: -98.7946026
          latitude: 20.04367
        }
        {
          longitude: -98.7934077
          latitude: 20.0434231
        }
        {
          longitude: -98.7937302
          latitude: 20.0429418
        }
        {
          longitude: -98.79436660000002
          latitude: 20.0419818
        }
        {
          longitude: -98.7947199
          latitude: 20.0414595
        }
        {
          longitude: -98.7949111
          latitude: 20.0413745
        }
        {
          longitude: -98.7950948
          latitude: 20.041338000000003
        }
        {
          longitude: -98.795194
          latitude: 20.0412932
        }
        {
          longitude: -98.7956527
          latitude: 20.0412913
        }
        {
          longitude: -98.7958404
          latitude: 20.0412391
        }
        {
          longitude: -98.7960919
          latitude: 20.0410942
        }
        {
          longitude: -98.7963367
          latitude: 20.041091
        }
        {
          longitude: -98.7977194
          latitude: 20.0418772
        }
        {
          longitude: -98.7975819
          latitude: 20.0421896
        }
        {
          longitude: -98.7973291
          latitude: 20.0426388
        }
        {
          longitude: -98.7971762
          latitude: 20.0429273
        }
        {
          longitude: -98.7971051
          latitude: 20.0433746
        }
        {
          longitude: -98.7968805
          latitude: 20.044184
        }
        {
          longitude: -98.7975805
          latitude: 20.044327000000003
        }
        {
          longitude: -98.7988847
          latitude: 20.0445935
        }
        {
          longitude: -98.7989967
          latitude: 20.0446061
        }
        {
          longitude: -98.799963
          latitude: 20.0448222
        }
        {
          longitude: -98.8011063
          latitude: 20.0450596
        }
        {
          longitude: -98.8026325
          latitude: 20.045391
        }
        {
          longitude: -98.80386560000001
          latitude: 20.0456587
        }
        {
          longitude: -98.804048
          latitude: 20.0456952
        }
        {
          longitude: -98.8041526
          latitude: 20.0457053
        }
        {
          longitude: -98.8052242
          latitude: 20.0459239
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.8054213
            latitude: 20.0459875
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.8030529
            latitude: 20.0454288
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.8019096
            latitude: 20.04519
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.7993984
            latitude: 20.0446552
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.7969361
            latitude: 20.0441551
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.7955917
            latitude: 20.0438502
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7936477
            latitude: 20.0431226
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.7976724
            latitude: 20.0418803
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7968798
            latitude: 20.0440902
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.798925
            latitude: 20.0446193
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.801809
            latitude: 20.0452322
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.8032668
            latitude: 20.045540300000003
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
