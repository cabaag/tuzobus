services.factory 'HuixmiFactory', [
  ->
    _huixmi_icon = 'img/routes/huixmi_van.png'
    _huixmi_color = '#f8981b'
    _route =
      id: 15
      name: "El Huixmi"
      stations: [9]
      visible: true
      icon:
        url: _huixmi_icon
      stroke:
        color: _huixmi_color
        weight: 5
      path: [
        {
          longitude: -98.8162601
          latitude: 20.0709817
        }
        {
          longitude: -98.8165532
          latitude: 20.0712003
        }
        {
          longitude: -98.8162903
          latitude: 20.0715139
        }
        {
          longitude: -98.8160724
          latitude: 20.0717885
        }
        {
          longitude: -98.8158354
          latitude: 20.0720549
        }
        {
          longitude: -98.8155172
          latitude: 20.0724473
        }
        {
          longitude: -98.8169193
          latitude: 20.0734544
        }
        {
          longitude: -98.81709030000002
          latitude: 20.073557
        }
        {
          longitude: -98.8171458
          latitude: 20.0735539
        }
        {
          longitude: -98.8172395
          latitude: 20.0735344
        }
        {
          longitude: -98.8174593
          latitude: 20.073519
        }
        {
          longitude: -98.8184535
          latitude: 20.073505500000003
        }
        {
          longitude: -98.8184589
          latitude: 20.0741662
        }
        {
          longitude: -98.8184616
          latitude: 20.0750274
        }
        {
          longitude: -98.8184783
          latitude: 20.0755542
        }
        {
          longitude: -98.8195606
          latitude: 20.0755548
        }
        {
          longitude: -98.8195552
          latitude: 20.0761403
        }
        {
          longitude: -98.8195525
          latitude: 20.0763184
        }
        {
          longitude: -98.8195324
          latitude: 20.0764996
        }
        {
          longitude: -98.8185461
          latitude: 20.0769795
        }
        {
          longitude: -98.8179218
          latitude: 20.0772629
        }
        {
          longitude: -98.8173441
          latitude: 20.077556700000002
        }
        {
          longitude: -98.8171084
          latitude: 20.0777019
        }
        {
          longitude: -98.8169163
          latitude: 20.0778174
        }
        {
          longitude: -98.8164962
          latitude: 20.0781446
        }
        {
          longitude: -98.8160503
          latitude: 20.0785609
        }
        {
          longitude: -98.8157304
          latitude: 20.0788708
        }
        {
          longitude: -98.8152556
          latitude: 20.0793192
        }
        {
          longitude: -98.8144121
          latitude: 20.0801254
        }
        {
          longitude: -98.8137775
          latitude: 20.080718000000005
        }
        {
          longitude: -98.8131656
          latitude: 20.0813068
        }
        {
          longitude: -98.8129852
          latitude: 20.081521600000002
        }
        {
          longitude: -98.8129221
          latitude: 20.0816267
        }
        {
          longitude: -98.8111126
          latitude: 20.0833306
        }
        {
          longitude: -98.8096559
          latitude: 20.0846995
        }
        {
          longitude: -98.8089972
          latitude: 20.0853228
        }
        {
          longitude: -98.8081595
          latitude: 20.0861142
        }
        {
          longitude: -98.8078093
          latitude: 20.0864346
        }
        {
          longitude: -98.8074336
          latitude: 20.086731
        }
        {
          longitude: -98.8069321
          latitude: 20.0870548
        }
        {
          longitude: -98.8062864
          latitude: 20.0874049
        }
        {
          longitude: -98.8049131
          latitude: 20.0881524
        }
        {
          longitude: -98.8035143
          latitude: 20.0889333
        }
        {
          longitude: -98.8022389
          latitude: 20.089623200000002
        }
        {
          longitude: -98.8009367
          latitude: 20.0903509
        }
        {
          longitude: -98.8007314
          latitude: 20.08989
        }
        {
          longitude: -98.8005074
          latitude: 20.0894096
        }
        {
          longitude: -98.8000579
          latitude: 20.0884639
        }
        {
          longitude: -98.799151
          latitude: 20.0865484
        }
        {
          longitude: -98.7980738
          latitude: 20.0842658
        }
        {
          longitude: -98.79718760000002
          latitude: 20.0823592
        }
        {
          longitude: -98.79643730000001
          latitude: 20.0807438
        }
        {
          longitude: -98.7951921
          latitude: 20.0780577
        }
        {
          longitude: -98.79504090000002
          latitude: 20.0777406
        }
        {
          longitude: -98.7947562
          latitude: 20.0771609
        }
        {
          longitude: -98.794423
          latitude: 20.0764864
        }
        {
          longitude: -98.7938584
          latitude: 20.0752526
        }
        {
          longitude: -98.7927332
          latitude: 20.0727553
        }
        {
          longitude: -98.792489
          latitude: 20.072204500000005
        }
        {
          longitude: -98.7919734
          latitude: 20.0711347
        }
        {
          longitude: -98.7919811
          latitude: 20.0709351
        }
        {
          longitude: -98.7913042
          latitude: 20.0694588
        }
        {
          longitude: -98.7903466
          latitude: 20.0674649
        }
        {
          longitude: -98.7899718
          latitude: 20.0666525
        }
        {
          longitude: -98.7894568
          latitude: 20.0655506
        }
        {
          longitude: -98.7892985
          latitude: 20.065201
        }
        {
          longitude: -98.7891161
          latitude: 20.0648691
        }
        {
          longitude: -98.78893910000001
          latitude: 20.0647847
        }
        {
          longitude: -98.7882772
          latitude: 20.0633971
        }
        {
          longitude: -98.7876476
          latitude: 20.0620007
        }
        {
          longitude: -98.7861637
          latitude: 20.0589516
        }
        {
          longitude: -98.7852699
          latitude: 20.0570852
        }
        {
          longitude: -98.7852397
          latitude: 20.0569441
        }
        {
          longitude: -98.7852377
          latitude: 20.0568263
        }
        {
          longitude: -98.7853027
          latitude: 20.0567054
        }
        {
          longitude: -98.7854161
          latitude: 20.0566134
        }
        {
          longitude: -98.7855569
          latitude: 20.0565832
        }
        {
          longitude: -98.7856943
          latitude: 20.056609
        }
        {
          longitude: -98.7858184
          latitude: 20.0566903
        }
        {
          longitude: -98.78588740000001
          latitude: 20.0567993
        }
        {
          longitude: -98.7859324
          latitude: 20.0569889
        }
        {
          longitude: -98.7855206
          latitude: 20.0577013
        }
        {
          longitude: -98.7853798
          latitude: 20.0580615
        }
        {
          longitude: -98.7852725
          latitude: 20.0582656
        }
        {
          longitude: -98.7846308
          latitude: 20.0594561
        }
        {
          longitude: -98.78458180000001
          latitude: 20.0594904
        }
        {
          longitude: -98.7845041
          latitude: 20.0595713
        }
        {
          longitude: -98.784207
          latitude: 20.0601439
        }
        {
          longitude: -98.7815335
          latitude: 20.0586983
        }
        {
          longitude: -98.7788427
          latitude: 20.0572893
        }
        {
          longitude: -98.7787984
          latitude: 20.0570971
        }
        {
          longitude: -98.7789646
          latitude: 20.0569045
        }
        {
          longitude: -98.7804868
          latitude: 20.0553051
        }
        {
          longitude: -98.7806447
          latitude: 20.0552132
        }
        {
          longitude: -98.7808007
          latitude: 20.0551641
        }
        {
          longitude: -98.7810045
          latitude: 20.0551458
        }
        {
          longitude: -98.7812304
          latitude: 20.0551855
        }
        {
          longitude: -98.78262920000002
          latitude: 20.0559457
        }
        {
          longitude: -98.7840347
          latitude: 20.0567419
        }
        {
          longitude: -98.7852176
          latitude: 20.0573491
        }
        {
          longitude: -98.7853832
          latitude: 20.0575349
        }
        {
          longitude: -98.7855139
          latitude: 20.0578083
        }
        {
          longitude: -98.7858539
          latitude: 20.0585194
        }
        {
          longitude: -98.7861174
          latitude: 20.0590933
        }
        {
          longitude: -98.7878381
          latitude: 20.0628366
        }
        {
          longitude: -98.7882283
          latitude: 20.0636346
        }
        {
          longitude: -98.7882075
          latitude: 20.063715100000003
        }
        {
          longitude: -98.7882377
          latitude: 20.063822
        }
        {
          longitude: -98.7887245
          latitude: 20.0648836
        }
        {
          longitude: -98.7891376
          latitude: 20.065772400000004
        }
        {
          longitude: -98.7895031
          latitude: 20.0665546
        }
        {
          longitude: -98.7903426
          latitude: 20.0683698
        }
        {
          longitude: -98.7910564
          latitude: 20.0698578
        }
        {
          longitude: -98.7915375
          latitude: 20.0709125
        }
        {
          longitude: -98.7918115
          latitude: 20.0714938
        }
        {
          longitude: -98.7918874
          latitude: 20.0715369
        }
        {
          longitude: -98.7925252
          latitude: 20.07296
        }
        {
          longitude: -98.7932524
          latitude: 20.0744536
        }
        {
          longitude: -98.7940185
          latitude: 20.0759126
        }
        {
          longitude: -98.7942717
          latitude: 20.0764552
        }
        {
          longitude: -98.7946022
          latitude: 20.077188100000004
        }
        {
          longitude: -98.7948787
          latitude: 20.077787
        }
        {
          longitude: -98.7950244
          latitude: 20.078088
        }
        {
          longitude: -98.7972896
          latitude: 20.082931
        }
        {
          longitude: -98.7979061
          latitude: 20.0842495
        }
        {
          longitude: -98.7984187
          latitude: 20.0853594
        }
        {
          longitude: -98.7991929
          latitude: 20.087016
        }
        {
          longitude: -98.79998250000001
          latitude: 20.0886575
        }
        {
          longitude: -98.800412
          latitude: 20.0895612
        }
        {
          longitude: -98.8006676
          latitude: 20.0901132
        }
        {
          longitude: -98.8008066
          latitude: 20.0904183
        }
        {
          longitude: -98.8009434
          latitude: 20.090366
        }
        {
          longitude: -98.8023676
          latitude: 20.0895807
        }
        {
          longitude: -98.8042573
          latitude: 20.0885385
        }
        {
          longitude: -98.8054918
          latitude: 20.0878618
        }
        {
          longitude: -98.8063035
          latitude: 20.0874208
        }
        {
          longitude: -98.80694220000001
          latitude: 20.0870743
        }
        {
          longitude: -98.8074511
          latitude: 20.0867456
        }
        {
          longitude: -98.807821
          latitude: 20.0864543
        }
        {
          longitude: -98.8081729
          latitude: 20.0861284
        }
        {
          longitude: -98.8090187
          latitude: 20.0853374
        }
        {
          longitude: -98.8094312
          latitude: 20.0849501
        }
        {
          longitude: -98.8108032
          latitude: 20.0836543
        }
        {
          longitude: -98.8114986
          latitude: 20.082993200000004
        }
        {
          longitude: -98.8121926
          latitude: 20.0823661
        }
        {
          longitude: -98.8129275
          latitude: 20.081650100000004
        }
        {
          longitude: -98.8130402
          latitude: 20.0815969
        }
        {
          longitude: -98.8134693
          latitude: 20.0812275
        }
        {
          longitude: -98.8158376
          latitude: 20.0789609
        }
        {
          longitude: -98.8163855
          latitude: 20.0783994
        }
        {
          longitude: -98.8166393
          latitude: 20.0781589
        }
        {
          longitude: -98.8169153
          latitude: 20.0779399
        }
        {
          longitude: -98.8170496
          latitude: 20.0778325
        }
        {
          longitude: -98.8173731
          latitude: 20.0776345
        }
        {
          longitude: -98.8177746
          latitude: 20.077424700000005
        }
        {
          longitude: -98.8187908
          latitude: 20.0769448
        }
        {
          longitude: -98.8195485
          latitude: 20.0766054
        }
        {
          longitude: -98.819577
          latitude: 20.0763056
        }
        {
          longitude: -98.8195841
          latitude: 20.0755536
        }
        {
          longitude: -98.8195525
          latitude: 20.0734909
        }
        {
          longitude: -98.81846420000001
          latitude: 20.0734803
        }
        {
          longitude: -98.8174587
          latitude: 20.0734929
        }
        {
          longitude: -98.817244
          latitude: 20.0735092
        }
        {
          longitude: -98.8171564
          latitude: 20.0735283
        }
        {
          longitude: -98.8170993
          latitude: 20.0735319
        }
        {
          longitude: -98.8169349
          latitude: 20.0734335
        }
        {
          longitude: -98.8155481
          latitude: 20.0724436
        }
        {
          longitude: -98.8158588
          latitude: 20.0720591
        }
        {
          longitude: -98.8160963
          latitude: 20.0718001
        }
        {
          longitude: -98.816315
          latitude: 20.0715204
        }
        {
          longitude: -98.8165841
          latitude: 20.0711971
        }
        {
          longitude: -98.81627250000001
          latitude: 20.0709679
        }
        {
          longitude: -98.8162601
          latitude: 20.0709817
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.8162809
            latitude: 20.0710189
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.81551580000001
            latitude: 20.0724039
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.8170614
            latitude: 20.0735546
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.818436
            latitude: 20.0735218
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.8195398
            latitude: 20.0760593
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.8185561
            latitude: 20.0769228
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.8165203
            latitude: 20.0780955
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.80656730000001
            latitude: 20.087233
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.8043498
            latitude: 20.0884472
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.8008501
            latitude: 20.0900959
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.7976778
            latitude: 20.083339700000003
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.79411780000001
            latitude: 20.0757343
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.79133500000002
            latitude: 20.0694814
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.7903634
            latitude: 20.0674547
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.7894554
            latitude: 20.0654977
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.7878273
            latitude: 20.0623365
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.7842104
            latitude: 20.0600815
        }
        {
          id: 18
          options:
            visible: true
          position:
            longitude: -98.7821961
            latitude: 20.0590296
        }
        {
          id: 19
          options:
            visible: true
          position:
            longitude: -98.7802307
            latitude: 20.0579809
        }
        {
          id: 20
          options:
            visible: true
          position:
            longitude: -98.7814484
            latitude: 20.055331600000002
        }
        {
          id: 21
          options:
            visible: true
          position:
            longitude: -98.7877945
            latitude: 20.0627893
        }
        {
          id: 22
          options:
            visible: true
          position:
            longitude: -98.7887085
            latitude: 20.0648949
        }
        {
          id: 23
          options:
            visible: true
          position:
            longitude: -98.789446
            latitude: 20.0664721
        }
        {
          id: 24
          options:
            visible: true
          position:
            longitude: -98.7903238
            latitude: 20.0683767
        }
        {
          id: 25
          options:
            visible: true
          position:
            longitude: -98.7939877
            latitude: 20.0759183
        }
        {
          id: 26
          options:
            visible: true
          position:
            longitude: -98.7974974
            latitude: 20.0834298
        }
        {
          id: 27
          options:
            visible: true
          position:
            longitude: -98.8006637
            latitude: 20.0901677
        }
        {
          id: 28
          options:
            visible: true
          position:
            longitude: -98.8045066
            latitude: 20.0884195
        }
        {
          id: 29
          options:
            visible: true
          position:
            longitude: -98.806871
            latitude: 20.0871398
        }
        {
          id: 30
          options:
            visible: true
          position:
            longitude: -98.8082846
            latitude: 20.086044
        }
        {
          id: 31
          options:
            visible: true
          position:
            longitude: -98.8166135
            latitude: 20.0782164
        }
        {
          id: 32
          options:
            visible: true
          position:
            longitude: -98.8185668
            latitude: 20.0770954
        }
        {
          id: 33
          options:
            visible: true
          position:
            longitude: -98.8196008
            latitude: 20.076264
        }
        {
          id: 34
          options:
            visible: true
          position:
            longitude: -98.81961150000001
            latitude: 20.0746378
        }
        {
          id: 35
          options:
            visible: true
          position:
            longitude: -98.8195773
            latitude: 20.0735332
        }
        {
          id: 36
          options:
            visible: true
          position:
            longitude: -98.8185038
            latitude: 20.0734652
        }
        {
          id: 37
          options:
            visible: true
          position:
            longitude: -98.8170956
            latitude: 20.0735073
        }
        {
          id: 38
          options:
            visible: true
          position:
            longitude: -98.8155795
            latitude: 20.0724411
        }
        {
          id: 39
          options:
            visible: true
          position:
            longitude: -98.816303
            latitude: 20.0715675
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
