services.factory 'ParqueUrbanoAFactory', [
  ->
    _parque_urbano_icon = 'img/routes/parque_urbano_van.png'
    _parque_urbano_color = '#0ba9cc'
    _route =
      id: 8
      name: "Parque Urbano A"
      stations: [6, 7, 8]
      visible: true
      icon:
        url: _parque_urbano_icon
      stroke:
        color: _parque_urbano_color
        weight: 5
      path: [
        {
          longitude: -98.7789921
          latitude: 20.0397309
        }
        {
          longitude: -98.7798109
          latitude: 20.040199
        }
        {
          longitude: -98.7809777
          latitude: 20.0408132
        }
        {
          longitude: -98.7810521
          latitude: 20.0408976
        }
        {
          longitude: -98.7812942
          latitude: 20.041016
        }
        {
          longitude: -98.7813042
          latitude: 20.0410217
        }
        {
          longitude: -98.78124320000002
          latitude: 20.0417978
        }
        {
          longitude: -98.7811433
          latitude: 20.04271
        }
        {
          longitude: -98.7810259
          latitude: 20.0439541
        }
        {
          longitude: -98.7808335
          latitude: 20.0458445
        }
        {
          longitude: -98.7807456
          latitude: 20.0467667
        }
        {
          longitude: -98.781159
          latitude: 20.0469837
        }
        {
          longitude: -98.7821491
          latitude: 20.0475032
        }
        {
          longitude: -98.7829953
          latitude: 20.047951
        }
        {
          longitude: -98.7841125
          latitude: 20.0485583
        }
        {
          longitude: -98.7850747
          latitude: 20.0490786
        }
        {
          longitude: -98.7846898
          latitude: 20.0498168
        }
        {
          longitude: -98.7844384
          latitude: 20.0502975
        }
        {
          longitude: -98.7851934
          latitude: 20.0506344
        }
        {
          longitude: -98.78591760000002
          latitude: 20.0509544
        }
        {
          longitude: -98.786395
          latitude: 20.0511888
        }
        {
          longitude: -98.7884154
          latitude: 20.0520914
        }
        {
          longitude: -98.7881626
          latitude: 20.0525248
        }
        {
          longitude: -98.7878454
          latitude: 20.0531396
        }
        {
          longitude: -98.7873432
          latitude: 20.0540888
        }
        {
          longitude: -98.7870354
          latitude: 20.05472
        }
        {
          longitude: -98.7868316
          latitude: 20.0551269
        }
        {
          longitude: -98.786729
          latitude: 20.0551231
        }
        {
          longitude: -98.7863515
          latitude: 20.0551811
        }
        {
          longitude: -98.7860551
          latitude: 20.0551785
        }
        {
          longitude: -98.7856031
          latitude: 20.0551704
        }
        {
          longitude: -98.7855508
          latitude: 20.0551521
        }
        {
          longitude: -98.7852765
          latitude: 20.0550009
        }
        {
          longitude: -98.7851532
          latitude: 20.0549216
        }
        {
          longitude: -98.7846074
          latitude: 20.0545512
        }
        {
          longitude: -98.7844411
          latitude: 20.0544378
        }
        {
          longitude: -98.7843297
          latitude: 20.0544044
        }
        {
          longitude: -98.7842594
          latitude: 20.0544252
        }
        {
          longitude: -98.7841078
          latitude: 20.0545461
        }
        {
          longitude: -98.7832059
          latitude: 20.0540428
        }
        {
          longitude: -98.7824086
          latitude: 20.0535906
        }
        {
          longitude: -98.7823717
          latitude: 20.0534791
        }
        {
          longitude: -98.7822939
          latitude: 20.053120700000004
        }
        {
          longitude: -98.7820995
          latitude: 20.0524498
        }
        {
          longitude: -98.7820338
          latitude: 20.0521544
        }
        {
          longitude: -98.7815128
          latitude: 20.0522987
        }
        {
          longitude: -98.78117210000002
          latitude: 20.0523661
        }
        {
          longitude: -98.7803647
          latitude: 20.0525695
        }
        {
          longitude: -98.78035430000001
          latitude: 20.0524908
        }
        {
          longitude: -98.7803124
          latitude: 20.0524177
        }
        {
          longitude: -98.7802629
          latitude: 20.0523837
        }
        {
          longitude: -98.78033660000001
          latitude: 20.0515081
        }
        {
          longitude: -98.7803762
          latitude: 20.0510388
        }
        {
          longitude: -98.78041980000002
          latitude: 20.0507976
        }
        {
          longitude: -98.7804459
          latitude: 20.0504165
        }
        {
          longitude: -98.7804157
          latitude: 20.0503025
        }
        {
          longitude: -98.7804962
          latitude: 20.0494433
        }
        {
          longitude: -98.7805706
          latitude: 20.0487781
        }
        {
          longitude: -98.7806055
          latitude: 20.0483926
        }
        {
          longitude: -98.7806793
          latitude: 20.0475554
        }
        {
          longitude: -98.7807712
          latitude: 20.0467592
        }
        {
          longitude: -98.7808603
          latitude: 20.0458358
        }
        {
          longitude: -98.78098840000001
          latitude: 20.0445954
        }
        {
          longitude: -98.7810994
          latitude: 20.0434155
        }
        {
          longitude: -98.781217
          latitude: 20.0422457
        }
        {
          longitude: -98.7812838
          latitude: 20.0416296
        }
        {
          longitude: -98.78133040000002
          latitude: 20.041006
        }
        {
          longitude: -98.7810581
          latitude: 20.04088
        }
        {
          longitude: -98.780987
          latitude: 20.0407924
        }
        {
          longitude: -98.7802722
          latitude: 20.040426400000005
        }
        {
          longitude: -98.779825
          latitude: 20.0401732
        }
        {
          longitude: -98.7793831
          latitude: 20.0399306
        }
        {
          longitude: -98.7790062
          latitude: 20.0397171
        }
        {
          longitude: -98.7789921
          latitude: 20.0397309
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7789935
            latitude: 20.0397505
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.7808456
            latitude: 20.040773500000004
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.7812391
            latitude: 20.041476
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.7811131
            latitude: 20.0469778
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.7818903
            latitude: 20.0473847
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.7833065
            latitude: 20.0481463
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7850264
            latitude: 20.0490773
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.7868275
            latitude: 20.0514073
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7883007
            latitude: 20.0522439
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.7868336
            latitude: 20.0550683
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.78363300000001
            latitude: 20.0542627
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7819714
            latitude: 20.0521538
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7804318
            latitude: 20.0508738
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.7810158
            latitude: 20.044509
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.7805539
            latitude: 20.0490886
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.7812405
            latitude: 20.0422577
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.7809656
            latitude: 20.0407672
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
