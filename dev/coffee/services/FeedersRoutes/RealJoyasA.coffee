services.factory 'RealJoyasAFactory', [
  ->
    _real_joyas_icon = 'img/routes/real_joyas_van.png'
    _real_joyas_color = '#cedc39'
    _route =
      id: 3
      name: "Real de Joyas A"
      stations: [2, 3]
      visible: true
      icon:
        url: _real_joyas_icon
      stroke:
        color: _real_joyas_color
        weight: 5
      path: [
        {
          longitude: -98.7838181
          latitude: 20.0180383
        }
        {
          longitude: -98.7875162
          latitude: 20.0180609
        }
        {
          longitude: -98.78989270000001
          latitude: 20.0180609
        }
        {
          longitude: -98.7917982
          latitude: 20.0180871
        }
        {
          longitude: -98.791854
          latitude: 20.018115800000004
        }
        {
          longitude: -98.79190360000001
          latitude: 20.0210007
        }
        {
          longitude: -98.7919223
          latitude: 20.0218482
        }
        {
          longitude: -98.7918874
          latitude: 20.0218755
        }
        {
          longitude: -98.7887896
          latitude: 20.0219596
        }
        {
          longitude: -98.78832990000001
          latitude: 20.0219722
        }
        {
          longitude: -98.78835240000001
          latitude: 20.022526
        }
        {
          longitude: -98.7883276
          latitude: 20.0225524
        }
        {
          longitude: -98.7882894
          latitude: 20.022565
        }
        {
          longitude: -98.7873888
          latitude: 20.0225745
        }
        {
          longitude: -98.7874525
          latitude: 20.0247285
        }
        {
          longitude: -98.7874532
          latitude: 20.02531
        }
        {
          longitude: -98.787484
          latitude: 20.026803100000002
        }
        {
          longitude: -98.78743980000002
          latitude: 20.027063300000002
        }
        {
          longitude: -98.7885857
          latitude: 20.0273638
        }
        {
          longitude: -98.78973240000002
          latitude: 20.0276587
        }
        {
          longitude: -98.7911171
          latitude: 20.0280127
        }
        {
          longitude: -98.7924347
          latitude: 20.0283586
        }
        {
          longitude: -98.7928759
          latitude: 20.0284777
        }
        {
          longitude: -98.7937497
          latitude: 20.0286963
        }
        {
          longitude: -98.7945248
          latitude: 20.0288922
        }
        {
          longitude: -98.7952048
          latitude: 20.0290617
        }
        {
          longitude: -98.7952584
          latitude: 20.0285571
        }
        {
          longitude: -98.795298
          latitude: 20.0282156
        }
        {
          longitude: -98.7955233
          latitude: 20.0276499
        }
        {
          longitude: -98.7958995
          latitude: 20.0280127
        }
        {
          longitude: -98.7966324
          latitude: 20.0272819
        }
        {
          longitude: -98.7973807
          latitude: 20.0266488
        }
        {
          longitude: -98.7975182
          latitude: 20.0265807
        }
        {
          longitude: -98.7983007
          latitude: 20.0258814
        }
        {
          longitude: -98.798591
          latitude: 20.0261347
        }
        {
          longitude: -98.7989813
          latitude: 20.0257731
        }
        {
          longitude: -98.80006690000002
          latitude: 20.0268157
        }
        {
          longitude: -98.8004344
          latitude: 20.0271358
        }
        {
          longitude: -98.80093130000002
          latitude: 20.0276083
        }
        {
          longitude: -98.8024621
          latitude: 20.0284481
        }
        {
          longitude: -98.8036966
          latitude: 20.0291133
        }
        {
          longitude: -98.8040527
          latitude: 20.0292916
        }
        {
          longitude: -98.8035726
          latitude: 20.0298423
        }
        {
          longitude: -98.8030154
          latitude: 20.030483
        }
        {
          longitude: -98.8025661
          latitude: 20.0309573
        }
        {
          longitude: -98.8024199
          latitude: 20.0308906
        }
        {
          longitude: -98.80191300000001
          latitude: 20.0307835
        }
        {
          longitude: -98.80080590000001
          latitude: 20.030463400000002
        }
        {
          longitude: -98.7995385
          latitude: 20.0301554
        }
        {
          longitude: -98.7979279
          latitude: 20.0297238
        }
        {
          longitude: -98.7968664
          latitude: 20.029446
        }
        {
          longitude: -98.7967162
          latitude: 20.0294365
        }
        {
          longitude: -98.7951759
          latitude: 20.0290384
        }
        {
          longitude: -98.7930637
          latitude: 20.0285023
        }
        {
          longitude: -98.792198
          latitude: 20.0282698
        }
        {
          longitude: -98.7921276
          latitude: 20.0282484
        }
        {
          longitude: -98.7911258
          latitude: 20.0279844
        }
        {
          longitude: -98.7890129
          latitude: 20.0274476
        }
        {
          longitude: -98.7878314
          latitude: 20.0271459
        }
        {
          longitude: -98.78756520000002
          latitude: 20.0270759
        }
        {
          longitude: -98.7875336
          latitude: 20.0251582
        }
        {
          longitude: -98.7875249
          latitude: 20.023553500000002
        }
        {
          longitude: -98.7874927
          latitude: 20.0226463
        }
        {
          longitude: -98.7883933
          latitude: 20.0226198
        }
        {
          longitude: -98.7884275
          latitude: 20.0225997
        }
        {
          longitude: -98.7884449
          latitude: 20.0225291
        }
        {
          longitude: -98.78841470000002
          latitude: 20.0220219
        }
        {
          longitude: -98.7906323
          latitude: 20.0219734
        }
        {
          longitude: -98.7918996
          latitude: 20.021935
        }
        {
          longitude: -98.7919231
          latitude: 20.021976
        }
        {
          longitude: -98.7919821
          latitude: 20.0219816
        }
        {
          longitude: -98.7920377
          latitude: 20.0219545
        }
        {
          longitude: -98.7920404
          latitude: 20.021884600000003
        }
        {
          longitude: -98.7919854
          latitude: 20.0218424
        }
        {
          longitude: -98.7919647
          latitude: 20.0205408
        }
        {
          longitude: -98.7919338
          latitude: 20.0191408
        }
        {
          longitude: -98.7919063
          latitude: 20.0181069
        }
        {
          longitude: -98.7919009
          latitude: 20.0179771
        }
        {
          longitude: -98.7905786
          latitude: 20.0179689
        }
        {
          longitude: -98.7885656
          latitude: 20.0179715
        }
        {
          longitude: -98.7867156
          latitude: 20.0179639
        }
        {
          longitude: -98.78371820000001
          latitude: 20.0179621
        }
        {
          longitude: -98.7836941
          latitude: 20.0180342
        }
        {
          longitude: -98.7838181
          latitude: 20.0180383
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7850164
            latitude: 20.0180754
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.7872507
            latitude: 20.0180962
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.7896848
            latitude: 20.0180824
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.7917856
            latitude: 20.018129
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.7918681
            latitude: 20.0206126
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.7914979
            latitude: 20.0218266
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7884375
            latitude: 20.0219344
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.7874203
            latitude: 20.0249037
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7876101
            latitude: 20.0271339
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.7902065
            latitude: 20.0277979
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.7951424
            latitude: 20.029073
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7974927
            latitude: 20.0266204
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7987828
            latitude: 20.0260131
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.8008803
            latitude: 20.0276095
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.8029584
            latitude: 20.03051
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.8010131
            latitude: 20.0304691
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.7951431
            latitude: 20.029005
        }
        {
          id: 18
          options:
            visible: true
          position:
            longitude: -98.79846430000002
            latitude: 20.0298404
        }
        {
          id: 19
          options:
            visible: true
          position:
            longitude: -98.7902635
            latitude: 20.0277513
        }
        {
          id: 20
          options:
            visible: true
          position:
            longitude: -98.78761340000001
            latitude: 20.0270703
        }
        {
          id: 21
          options:
            visible: true
          position:
            longitude: -98.7875524
            latitude: 20.0248911
        }
        {
          id: 22
          options:
            visible: true
          position:
            longitude: -98.7884382
            latitude: 20.0220541
        }
        {
          id: 23
          options:
            visible: true
          position:
            longitude: -98.7916884
            latitude: 20.021998
        }
        {
          id: 24
          options:
            visible: true
          position:
            longitude: -98.7919982
            latitude: 20.0206844
        }
        {
          id: 25
          options:
            visible: true
          position:
            longitude: -98.791838
            latitude: 20.0179438
        }
        {
          id: 26
          options:
            visible: true
          position:
            longitude: -98.7897753
            latitude: 20.01795
        }
        {
          id: 27
          options:
            visible: true
          position:
            longitude: -98.7874015
            latitude: 20.0179393
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
