services.factory 'HogaresUnionFactory', [
  ->
    _hogares_union_icon = 'img/routes/hogares_union_van.png'
    _hogares_union_color = '#4187f0'
    _route =
      id: 10
      name: "Hogares Unión"
      stations: [7, 8]
      visible: true
      icon:
        url: _hogares_union_icon
      stroke:
        color: _hogares_union_color
        weight: 5
      path: [
        {
          longitude: -98.7993099
          latitude: 20.0586864
        }
        {
          longitude: -98.7993267
          latitude: 20.0587557
        }
        {
          longitude: -98.7993903
          latitude: 20.0588212
        }
        {
          longitude: -98.79948020000002
          latitude: 20.0588451
        }
        {
          longitude: -98.7995587
          latitude: 20.05883
        }
        {
          longitude: -98.7996117
          latitude: 20.058791
        }
        {
          longitude: -98.7996418
          latitude: 20.0587371
        }
        {
          longitude: -98.7996491
          latitude: 20.0586857
        }
        {
          longitude: -98.7996454
          latitude: 20.0586281
        }
        {
          longitude: -98.7996163
          latitude: 20.0585768
        }
        {
          longitude: -98.7995665
          latitude: 20.0585425
        }
        {
          longitude: -98.7995167
          latitude: 20.058527
        }
        {
          longitude: -98.7994622
          latitude: 20.0585236
        }
        {
          longitude: -98.7994064
          latitude: 20.0585365
        }
        {
          longitude: -98.7993715
          latitude: 20.0585592
        }
        {
          longitude: -98.7993474
          latitude: 20.0585894
        }
        {
          longitude: -98.7993273
          latitude: 20.058624100000003
        }
        {
          longitude: -98.7993179
          latitude: 20.0586587
        }
        {
          longitude: -98.7989793
          latitude: 20.0586297
        }
        {
          longitude: -98.7988874
          latitude: 20.0585988
        }
        {
          longitude: -98.7988016
          latitude: 20.0585554
        }
        {
          longitude: -98.7981351
          latitude: 20.0580137
        }
        {
          longitude: -98.7970736
          latitude: 20.057191
        }
        {
          longitude: -98.7966317
          latitude: 20.0568491
        }
        {
          longitude: -98.7957304
          latitude: 20.0584659
        }
        {
          longitude: -98.79513370000001
          latitude: 20.0579734
        }
        {
          longitude: -98.7949459
          latitude: 20.0578033
        }
        {
          longitude: -98.79413790000001
          latitude: 20.0572005
        }
        {
          longitude: -98.7934674
          latitude: 20.0566789
        }
        {
          longitude: -98.79299
          latitude: 20.0563256
        }
        {
          longitude: -98.7928619
          latitude: 20.0562355
        }
        {
          longitude: -98.7927029
          latitude: 20.0560812
        }
        {
          longitude: -98.7921353
          latitude: 20.055676800000004
        }
        {
          longitude: -98.7916126
          latitude: 20.0552781
        }
        {
          longitude: -98.7902481
          latitude: 20.0541839
        }
        {
          longitude: -98.7892194
          latitude: 20.0533405
        }
        {
          longitude: -98.7886213
          latitude: 20.0528738
        }
        {
          longitude: -98.7888446
          latitude: 20.0523793
        }
        {
          longitude: -98.7890732
          latitude: 20.0519024
        }
        {
          longitude: -98.7892355
          latitude: 20.0515371
        }
        {
          longitude: -98.7892509
          latitude: 20.0514646
        }
        {
          longitude: -98.7892261
          latitude: 20.0513746
        }
        {
          longitude: -98.789373
          latitude: 20.0511182
        }
        {
          longitude: -98.7895758
          latitude: 20.0507097
        }
        {
          longitude: -98.7897867
          latitude: 20.0503005
        }
        {
          longitude: -98.7903013
          latitude: 20.0506788
        }
        {
          longitude: -98.7908159
          latitude: 20.0510798
        }
        {
          longitude: -98.7910915
          latitude: 20.051299
        }
        {
          longitude: -98.7901883
          latitude: 20.0517966
        }
        {
          longitude: -98.7915264
          latitude: 20.0528498
        }
        {
          longitude: -98.7928692
          latitude: 20.0539182
        }
        {
          longitude: -98.7938623
          latitude: 20.0546973
        }
        {
          longitude: -98.794842
          latitude: 20.0554891
        }
        {
          longitude: -98.79525510000002
          latitude: 20.0558198
        }
        {
          longitude: -98.79591690000001
          latitude: 20.0563253
        }
        {
          longitude: -98.7962441
          latitude: 20.0565769
        }
        {
          longitude: -98.7967665
          latitude: 20.0569819
        }
        {
          longitude: -98.7980359
          latitude: 20.0579652
        }
        {
          longitude: -98.7986749
          latitude: 20.0584823
        }
        {
          longitude: -98.7988063
          latitude: 20.0585856
        }
        {
          longitude: -98.7989156
          latitude: 20.0586347
        }
        {
          longitude: -98.79898470000002
          latitude: 20.0586549
        }
        {
          longitude: -98.7993099
          latitude: 20.0586864
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7992784
            latitude: 20.058704
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.7966961
            latitude: 20.0568786
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.7957264
            latitude: 20.0583903
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.793521
            latitude: 20.056695300000005
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.790757
            latitude: 20.0545587
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.7895574
            latitude: 20.053598800000003
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7888164
            latitude: 20.0524813
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.789662
            latitude: 20.0506162
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7908026
            latitude: 20.0514332
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.7915268
            latitude: 20.0528675
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.7936109
            latitude: 20.0545216
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7946328
            latitude: 20.0553379
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7965814
            latitude: 20.056868500000004
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
