services.factory 'SanCarlosFactory', [
  ->
    _san_carlos_icon = 'img/routes/san_carlos_van.png'
    _san_carlos_color = '#62af44'
    _route =
      id: 21
      name: "San Carlos"
      stations: [18]
      visible: true
      icon:
        url: _san_carlos_icon
      stroke:
        color: _san_carlos_color
        weight: 5
      path: [
        {
          longitude: -98.7889901
          latitude: 20.0986483
        }
        {
          longitude: -98.7890102
          latitude: 20.0985236
        }
        {
          longitude: -98.7871327
          latitude: 20.098137
        }
        {
          longitude: -98.7868443
          latitude: 20.0980941
        }
        {
          longitude: -98.7868108
          latitude: 20.0980664
        }
        {
          longitude: -98.7867732
          latitude: 20.0980375
        }
        {
          longitude: -98.786729
          latitude: 20.0980337
        }
        {
          longitude: -98.78666460000001
          latitude: 20.0980614
        }
        {
          longitude: -98.7860477
          latitude: 20.097920300000002
        }
        {
          longitude: -98.7843177
          latitude: 20.0975967
        }
        {
          longitude: -98.7839892
          latitude: 20.0975935
        }
        {
          longitude: -98.7838309
          latitude: 20.0976281
        }
        {
          longitude: -98.7836096
          latitude: 20.0976659
        }
        {
          longitude: -98.7835304
          latitude: 20.0976546
        }
        {
          longitude: -98.7836351
          latitude: 20.0974569
        }
        {
          longitude: -98.7838335
          latitude: 20.0971092
        }
        {
          longitude: -98.785172
          latitude: 20.0939298
        }
        {
          longitude: -98.7850485
          latitude: 20.0938882
        }
        {
          longitude: -98.7837155
          latitude: 20.0970299
        }
        {
          longitude: -98.7834942
          latitude: 20.0974317
        }
        {
          longitude: -98.7828586
          latitude: 20.0982213
        }
        {
          longitude: -98.7825608
          latitude: 20.0985186
        }
        {
          longitude: -98.7825266
          latitude: 20.0985393
        }
        {
          longitude: -98.7824663
          latitude: 20.0985507
        }
        {
          longitude: -98.78237440000001
          latitude: 20.0985696
        }
        {
          longitude: -98.7823288
          latitude: 20.0986017
        }
        {
          longitude: -98.78226710000001
          latitude: 20.0986678
        }
        {
          longitude: -98.782245
          latitude: 20.0987415
        }
        {
          longitude: -98.7822363
          latitude: 20.0988032
        }
        {
          longitude: -98.7822544
          latitude: 20.0988825
        }
        {
          longitude: -98.7822584
          latitude: 20.0989115
        }
        {
          longitude: -98.7822215
          latitude: 20.0989751
        }
        {
          longitude: -98.7817173
          latitude: 20.0999115
        }
        {
          longitude: -98.7802169
          latitude: 20.1030695
        }
        {
          longitude: -98.7800583
          latitude: 20.1034202
        }
        {
          longitude: -98.7799879
          latitude: 20.1036104
        }
        {
          longitude: -98.7798786
          latitude: 20.1039385
        }
        {
          longitude: -98.77983510000001
          latitude: 20.1041028
        }
        {
          longitude: -98.7792073
          latitude: 20.1060819
        }
        {
          longitude: -98.7786984
          latitude: 20.1077518
        }
        {
          longitude: -98.7777969
          latitude: 20.1107545
        }
        {
          longitude: -98.7774733
          latitude: 20.1117326
        }
        {
          longitude: -98.7773607
          latitude: 20.1120091
        }
        {
          longitude: -98.7772762
          latitude: 20.1120815
        }
        {
          longitude: -98.7771562
          latitude: 20.1121778
        }
        {
          longitude: -98.7770368
          latitude: 20.1122464
        }
        {
          longitude: -98.7769121
          latitude: 20.112303100000002
        }
        {
          longitude: -98.77675650000002
          latitude: 20.112347200000002
        }
        {
          longitude: -98.7766036
          latitude: 20.1123667
        }
        {
          longitude: -98.7765118
          latitude: 20.1123661
        }
        {
          longitude: -98.7763428
          latitude: 20.1123478
        }
        {
          longitude: -98.7761839
          latitude: 20.1123113
        }
        {
          longitude: -98.776041
          latitude: 20.1122584
        }
        {
          longitude: -98.7758969
          latitude: 20.1122017
        }
        {
          longitude: -98.7747369
          latitude: 20.1110854
        }
        {
          longitude: -98.7729374
          latitude: 20.1093867
        }
        {
          longitude: -98.77275800000001
          latitude: 20.1091973
        }
        {
          longitude: -98.7720439
          latitude: 20.1085414
        }
        {
          longitude: -98.7692637
          latitude: 20.1059302
        }
        {
          longitude: -98.7676165
          latitude: 20.1043831
        }
        {
          longitude: -98.76599480000002
          latitude: 20.102815800000002
        }
        {
          longitude: -98.7658963
          latitude: 20.1027225
        }
        {
          longitude: -98.7640415
          latitude: 20.1009442
        }
        {
          longitude: -98.7639483
          latitude: 20.1004826
        }
        {
          longitude: -98.7639382
          latitude: 20.1002062
        }
        {
          longitude: -98.7645491
          latitude: 20.0996034
        }
        {
          longitude: -98.7648931
          latitude: 20.099239600000004
        }
        {
          longitude: -98.7649582
          latitude: 20.0992238
        }
        {
          longitude: -98.76505
          latitude: 20.0991778
        }
        {
          longitude: -98.7651519
          latitude: 20.0990715
        }
        {
          longitude: -98.765614
          latitude: 20.0988183
        }
        {
          longitude: -98.7665413
          latitude: 20.0979878
        }
        {
          longitude: -98.7682405
          latitude: 20.0964619
        }
        {
          longitude: -98.7690787
          latitude: 20.095744
        }
        {
          longitude: -98.7695561
          latitude: 20.0962421
        }
        {
          longitude: -98.7696621
          latitude: 20.0962925
        }
        {
          longitude: -98.7697164
          latitude: 20.0962925
        }
        {
          longitude: -98.7697667
          latitude: 20.0962793
        }
        {
          longitude: -98.7698016
          latitude: 20.0962541
        }
        {
          longitude: -98.7698311
          latitude: 20.0962012
        }
        {
          longitude: -98.7698632
          latitude: 20.0961029
        }
        {
          longitude: -98.7692872
          latitude: 20.095557
        }
        {
          longitude: -98.7690371
          latitude: 20.0953142
        }
        {
          longitude: -98.7688393
          latitude: 20.0951571
        }
        {
          longitude: -98.7679475
          latitude: 20.0960633
        }
        {
          longitude: -98.7668927
          latitude: 20.0970343
        }
        {
          longitude: -98.7664052
          latitude: 20.0974593
        }
        {
          longitude: -98.7650674
          latitude: 20.098678500000002
        }
        {
          longitude: -98.7648328
          latitude: 20.0988636
        }
        {
          longitude: -98.764132
          latitude: 20.0995406
        }
        {
          longitude: -98.7633811
          latitude: 20.1001249
        }
        {
          longitude: -98.763088
          latitude: 20.1004443
        }
        {
          longitude: -98.7634172
          latitude: 20.1007156
        }
        {
          longitude: -98.7638242
          latitude: 20.1009178
        }
        {
          longitude: -98.7639617
          latitude: 20.1010103
        }
        {
          longitude: -98.76432550000001
          latitude: 20.1013705
        }
        {
          longitude: -98.76466910000002
          latitude: 20.1017382
        }
        {
          longitude: -98.7651198
          latitude: 20.1021961
        }
        {
          longitude: -98.7655342
          latitude: 20.1026041
        }
        {
          longitude: -98.7658111
          latitude: 20.1028277
        }
        {
          longitude: -98.7690864
          latitude: 20.1059687
        }
        {
          longitude: -98.7718548
          latitude: 20.1085862
        }
        {
          longitude: -98.7726111
          latitude: 20.1092895
        }
        {
          longitude: -98.772819
          latitude: 20.109474
        }
        {
          longitude: -98.7746068
          latitude: 20.1111974
        }
        {
          longitude: -98.77563130000001
          latitude: 20.1121324
        }
        {
          longitude: -98.7763569
          latitude: 20.1128075
        }
        {
          longitude: -98.7766841
          latitude: 20.1127552
        }
        {
          longitude: -98.7769081
          latitude: 20.1126859
        }
        {
          longitude: -98.777075
          latitude: 20.1126028
        }
        {
          longitude: -98.7774257
          latitude: 20.112301800000004
        }
        {
          longitude: -98.777637
          latitude: 20.1117518
        }
        {
          longitude: -98.7779511
          latitude: 20.1108025
        }
        {
          longitude: -98.778856
          latitude: 20.107789
        }
        {
          longitude: -98.7789338
          latitude: 20.1075226
        }
        {
          longitude: -98.7790099
          latitude: 20.1074833
        }
        {
          longitude: -98.7792262
          latitude: 20.1067928
        }
        {
          longitude: -98.7794166
          latitude: 20.10642
        }
        {
          longitude: -98.7794578
          latitude: 20.1062834
        }
        {
          longitude: -98.7794843
          latitude: 20.1061411
        }
        {
          longitude: -98.7794836
          latitude: 20.106002
        }
        {
          longitude: -98.780067
          latitude: 20.1040889
        }
        {
          longitude: -98.7801998
          latitude: 20.1036752
        }
        {
          longitude: -98.7804084
          latitude: 20.1031437
        }
        {
          longitude: -98.781555
          latitude: 20.100754
        }
        {
          longitude: -98.7818782
          latitude: 20.100074
        }
        {
          longitude: -98.7818755
          latitude: 20.0999442
        }
        {
          longitude: -98.7823046
          latitude: 20.0991483
        }
        {
          longitude: -98.7823771
          latitude: 20.0990526
        }
        {
          longitude: -98.7824086
          latitude: 20.099033
        }
        {
          longitude: -98.7825273
          latitude: 20.099044400000004
        }
        {
          longitude: -98.7825655
          latitude: 20.0990412
        }
        {
          longitude: -98.7826265
          latitude: 20.0990097
        }
        {
          longitude: -98.7827063
          latitude: 20.0989568
        }
        {
          longitude: -98.7827653
          latitude: 20.098824
        }
        {
          longitude: -98.7827553
          latitude: 20.098722
        }
        {
          longitude: -98.7827539
          latitude: 20.0986867
        }
        {
          longitude: -98.7827788
          latitude: 20.0986445
        }
        {
          longitude: -98.7829679
          latitude: 20.0983511
        }
        {
          longitude: -98.7832703
          latitude: 20.0979965
        }
        {
          longitude: -98.7834077
          latitude: 20.0979575
        }
        {
          longitude: -98.7835479
          latitude: 20.0978825
        }
        {
          longitude: -98.78373630000002
          latitude: 20.0977711
        }
        {
          longitude: -98.7839898
          latitude: 20.097723200000004
        }
        {
          longitude: -98.7843043
          latitude: 20.0977377
        }
        {
          longitude: -98.7866291
          latitude: 20.0981552
        }
        {
          longitude: -98.7866572
          latitude: 20.0981615
        }
        {
          longitude: -98.7866814
          latitude: 20.0981905
        }
        {
          longitude: -98.7867122
          latitude: 20.098201200000002
        }
        {
          longitude: -98.7867518
          latitude: 20.0982043
        }
        {
          longitude: -98.7867873
          latitude: 20.0981986
        }
        {
          longitude: -98.7868215
          latitude: 20.0981854
        }
        {
          longitude: -98.7871139
          latitude: 20.0982654
        }
        {
          longitude: -98.7889901
          latitude: 20.0986483
        }
      ]
      substations: []
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
