services.factory 'ColoniaFactory', [
  ->
    _colonia_icon = 'img/routes/colonia_van.png'
    _colonia_color = '#f2ff00'
    _route =
      id: 16
      name: "La Colonia"
      stations: [11]
      visible: true
      icon:
        url: _colonia_icon
      stroke:
        color: _colonia_color
        weight: 5
      path: [
        {
          longitude: -98.7707041
          latitude: 20.0549341
        }
        {
          longitude: -98.7705231
          latitude: 20.055222000000004
        }
        {
          longitude: -98.7699337
          latitude: 20.0561637
        }
        {
          longitude: -98.7693328
          latitude: 20.0571221
        }
        {
          longitude: -98.7690874
          latitude: 20.057477000000002
        }
        {
          longitude: -98.7685859
          latitude: 20.0580263
        }
        {
          longitude: -98.768323
          latitude: 20.058779
        }
        {
          longitude: -98.7679133
          latitude: 20.059965
        }
        {
          longitude: -98.7703621
          latitude: 20.0604966
        }
        {
          longitude: -98.7717555
          latitude: 20.0608027
        }
        {
          longitude: -98.7720754
          latitude: 20.0608286
        }
        {
          longitude: -98.7727356
          latitude: 20.0609521
        }
        {
          longitude: -98.7732309
          latitude: 20.0610152
        }
        {
          longitude: -98.7742201
          latitude: 20.0612396
        }
        {
          longitude: -98.7749165
          latitude: 20.0612651
        }
        {
          longitude: -98.7754537
          latitude: 20.0614987
        }
        {
          longitude: -98.7778998
          latitude: 20.0627966
        }
        {
          longitude: -98.7786603
          latitude: 20.0632126
        }
        {
          longitude: -98.77879870000001
          latitude: 20.0632932
        }
        {
          longitude: -98.7788889
          latitude: 20.063399
        }
        {
          longitude: -98.7789184
          latitude: 20.0634834
        }
        {
          longitude: -98.77890560000002
          latitude: 20.0636321
        }
        {
          longitude: -98.7790364
          latitude: 20.0636686
        }
        {
          longitude: -98.7790914
          latitude: 20.0637713
        }
        {
          longitude: -98.7799511
          latitude: 20.0642575
        }
        {
          longitude: -98.7801696
          latitude: 20.0644002
        }
        {
          longitude: -98.781148
          latitude: 20.0649314
        }
        {
          longitude: -98.7815624
          latitude: 20.0651494
        }
        {
          longitude: -98.7808778
          latitude: 20.0664753
        }
        {
          longitude: -98.7806176
          latitude: 20.0669892
        }
        {
          longitude: -98.7803118
          latitude: 20.067549800000002
        }
        {
          longitude: -98.7803078
          latitude: 20.0675863
        }
        {
          longitude: -98.7803212
          latitude: 20.0676288
        }
        {
          longitude: -98.7803527
          latitude: 20.0677249
        }
        {
          longitude: -98.7802796
          latitude: 20.0678439
        }
        {
          longitude: -98.7801736
          latitude: 20.0678855
        }
        {
          longitude: -98.779654
          latitude: 20.0688264
        }
        {
          longitude: -98.7794508
          latitude: 20.0691924
        }
        {
          longitude: -98.7785275
          latitude: 20.0691383
        }
        {
          longitude: -98.7771803
          latitude: 20.0690564
        }
        {
          longitude: -98.7765842
          latitude: 20.0690047
        }
        {
          longitude: -98.77663550000001
          latitude: 20.0682508
        }
        {
          longitude: -98.7766788
          latitude: 20.0674357
        }
        {
          longitude: -98.7767947
          latitude: 20.0659518
        }
        {
          longitude: -98.7768111
          latitude: 20.0657052
        }
        {
          longitude: -98.7768524
          latitude: 20.065056200000004
        }
        {
          longitude: -98.7769248
          latitude: 20.0639527
        }
        {
          longitude: -98.7769564
          latitude: 20.0635219
        }
        {
          longitude: -98.7777919
          latitude: 20.0635597
        }
        {
          longitude: -98.7778019
          latitude: 20.063484
        }
        {
          longitude: -98.7778643
          latitude: 20.0634595
        }
        {
          longitude: -98.7778998
          latitude: 20.0627966
        }
        {
          longitude: -98.7779206
          latitude: 20.062693
        }
        {
          longitude: -98.77571650000002
          latitude: 20.0615227
        }
        {
          longitude: -98.7753966
          latitude: 20.0613211
        }
        {
          longitude: -98.775007
          latitude: 20.0611731
        }
        {
          longitude: -98.7748068
          latitude: 20.0611401
        }
        {
          longitude: -98.7745966
          latitude: 20.061126
        }
        {
          longitude: -98.7745313
          latitude: 20.0611205
        }
        {
          longitude: -98.7744928
          latitude: 20.0610471
        }
        {
          longitude: -98.7729594
          latitude: 20.060204700000003
        }
        {
          longitude: -98.7728212
          latitude: 20.0601601
        }
        {
          longitude: -98.7723527
          latitude: 20.059939
        }
        {
          longitude: -98.7721304
          latitude: 20.0598742
        }
        {
          longitude: -98.771997
          latitude: 20.0598762
        }
        {
          longitude: -98.7716479
          latitude: 20.0606616
        }
        {
          longitude: -98.7681231
          latitude: 20.0598926
        }
        {
          longitude: -98.7684983
          latitude: 20.058829
        }
        {
          longitude: -98.7687636
          latitude: 20.058105
        }
        {
          longitude: -98.7693
          latitude: 20.057476400000002
        }
        {
          longitude: -98.77032520000002
          latitude: 20.0558173
        }
        {
          longitude: -98.7708426
          latitude: 20.0550141
        }
        {
          longitude: -98.7707041
          latitude: 20.0549341
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.770684
            latitude: 20.0553077
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.769583
            latitude: 20.0566783
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.7686154
            latitude: 20.0579551
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.7679414
            latitude: 20.0598126
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.7696379
            latitude: 20.0603795
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.771553
            latitude: 20.0608128
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7727037
            latitude: 20.0609678
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.7777678
            latitude: 20.0627528
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7812835
            latitude: 20.0656388
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.7797425
            latitude: 20.068609200000004
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.7766097
            latitude: 20.0689316
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7767069
            latitude: 20.067311
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7768873
            latitude: 20.064842000000002
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.7779266
            latitude: 20.06287
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.77444580000001
            latitude: 20.060962700000005
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.7727191
            latitude: 20.0600941
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.7716637
            latitude: 20.0605483
        }
        {
          id: 18
          options:
            visible: true
          position:
            longitude: -98.7698632
            latitude: 20.0602421
        }
        {
          id: 19
          options:
            visible: true
          position:
            longitude: -98.7682238
            latitude: 20.0598762
        }
        {
          id: 20
          options:
            visible: true
          position:
            longitude: -98.7688105
            latitude: 20.0580892
        }
        {
          id: 21
          options:
            visible: true
          position:
            longitude: -98.7697479
            latitude: 20.0567804
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
