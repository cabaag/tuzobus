services.factory 'ParquesPoblamientoFactory', [
  ->
    _parque_poblamiento_icon = 'img/routes/parque_poblamiento_van.png'
    _parque_poblamiento_color = '#3f5ba9'
    _route =
      id: 25
      name: "Parques de Poblamiento 1 y 2"
      stations: [22]
      visible: true
      icon:
        url: _parque_poblamiento_icon
      stroke:
        color: _parque_poblamiento_color
        weight: 5
      path: [
        {
          longitude: -98.7690391
          latitude: 20.1216886
        }
        {
          longitude: -98.7692108
          latitude: 20.1216684
        }
        {
          longitude: -98.7691753
          latitude: 20.1213505
        }
        {
          longitude: -98.7691974
          latitude: 20.1211981
        }
        {
          longitude: -98.7692765
          latitude: 20.1209802
        }
        {
          longitude: -98.7696842
          latitude: 20.1203512
        }
        {
          longitude: -98.7702072
          latitude: 20.1196416
        }
        {
          longitude: -98.7703072
          latitude: 20.1194616
        }
        {
          longitude: -98.7703467
          latitude: 20.1192538
        }
        {
          longitude: -98.7703433
          latitude: 20.118514
        }
        {
          longitude: -98.7702998
          latitude: 20.117499
        }
        {
          longitude: -98.7703004
          latitude: 20.1173309
        }
        {
          longitude: -98.7701932
          latitude: 20.1144232
        }
        {
          longitude: -98.77015360000001
          latitude: 20.1136216
        }
        {
          longitude: -98.7701368
          latitude: 20.1135064
        }
        {
          longitude: -98.7695655
          latitude: 20.1135864
        }
        {
          longitude: -98.7645404
          latitude: 20.1142255
        }
        {
          longitude: -98.7623457
          latitude: 20.1144666
        }
        {
          longitude: -98.7621445
          latitude: 20.1144666
        }
        {
          longitude: -98.7581883
          latitude: 20.1136059
        }
        {
          longitude: -98.7571905
          latitude: 20.1153066
        }
        {
          longitude: -98.7577598
          latitude: 20.1152241
        }
        {
          longitude: -98.7577967
          latitude: 20.115464
        }
        {
          longitude: -98.7577772
          latitude: 20.1156057
        }
        {
          longitude: -98.7576424
          latitude: 20.115957
        }
        {
          longitude: -98.757623
          latitude: 20.1160949
        }
        {
          longitude: -98.7576813
          latitude: 20.1164897
        }
        {
          longitude: -98.7570772
          latitude: 20.1162246
        }
        {
          longitude: -98.7567486
          latitude: 20.1161069
        }
        {
          longitude: -98.7556858
          latitude: 20.1158304
        }
        {
          longitude: -98.7549689
          latitude: 20.1155905
        }
        {
          longitude: -98.7544432
          latitude: 20.1155125
        }
        {
          longitude: -98.7527863
          latitude: 20.1150925
        }
        {
          longitude: -98.7523866
          latitude: 20.1156976
        }
        {
          longitude: -98.7523363
          latitude: 20.1157542
        }
        {
          longitude: -98.7522593
          latitude: 20.1157775
        }
        {
          longitude: -98.7496065
          latitude: 20.1161157
        }
        {
          longitude: -98.7484646
          latitude: 20.116292
        }
        {
          longitude: -98.7476123
          latitude: 20.1163745
        }
        {
          longitude: -98.747094
          latitude: 20.1164267
        }
        {
          longitude: -98.7470061
          latitude: 20.1163826
        }
        {
          longitude: -98.7467245
          latitude: 20.1161698
        }
        {
          longitude: -98.7467305
          latitude: 20.1160899
        }
        {
          longitude: -98.7467822
          latitude: 20.1159828
        }
        {
          longitude: -98.7473662
          latitude: 20.115529500000005
        }
        {
          longitude: -98.7474145
          latitude: 20.1154936
        }
        {
          longitude: -98.7474722
          latitude: 20.1153003
        }
        {
          longitude: -98.748653
          latitude: 20.1144081
        }
        {
          longitude: -98.7491613
          latitude: 20.1140328
        }
        {
          longitude: -98.7496595
          latitude: 20.1148312
        }
        {
          longitude: -98.74968630000001
          latitude: 20.1148973
        }
        {
          longitude: -98.749691
          latitude: 20.1149533
        }
        {
          longitude: -98.7495053
          latitude: 20.1161314
        }
        {
          longitude: -98.7494845
          latitude: 20.1163058
        }
        {
          longitude: -98.7495898
          latitude: 20.1162957
        }
        {
          longitude: -98.7545961
          latitude: 20.1156447
        }
        {
          longitude: -98.7549475
          latitude: 20.1158248
        }
        {
          longitude: -98.7563409
          latitude: 20.1162189
        }
        {
          longitude: -98.757739
          latitude: 20.116836
        }
        {
          longitude: -98.7577812
          latitude: 20.1167705
        }
        {
          longitude: -98.7577893
          latitude: 20.1166433
        }
        {
          longitude: -98.7577598
          latitude: 20.11654
        }
        {
          longitude: -98.7577812
          latitude: 20.1165123
        }
        {
          longitude: -98.75770610000001
          latitude: 20.1160666
        }
        {
          longitude: -98.7577095
          latitude: 20.1159652
        }
        {
          longitude: -98.75787650000001
          latitude: 20.1155717
        }
        {
          longitude: -98.7578825
          latitude: 20.1154589
        }
        {
          longitude: -98.757849
          latitude: 20.1152083
        }
        {
          longitude: -98.7594757
          latitude: 20.1149911
        }
        {
          longitude: -98.7643406
          latitude: 20.1143753
        }
        {
          longitude: -98.769996
          latitude: 20.1136443
        }
        {
          longitude: -98.7700027
          latitude: 20.1144572
        }
        {
          longitude: -98.7701214
          latitude: 20.117351
        }
        {
          longitude: -98.7701865
          latitude: 20.1190762
        }
        {
          longitude: -98.77017370000002
          latitude: 20.1192588
        }
        {
          longitude: -98.77011340000001
          latitude: 20.119403
        }
        {
          longitude: -98.7700155
          latitude: 20.1195875
        }
        {
          longitude: -98.76947970000002
          latitude: 20.1203349
        }
        {
          longitude: -98.7691451
          latitude: 20.1208524
        }
        {
          longitude: -98.7690452
          latitude: 20.1210495
        }
        {
          longitude: -98.769013
          latitude: 20.1212012
        }
        {
          longitude: -98.7690023
          latitude: 20.1213656
        }
        {
          longitude: -98.7690391
          latitude: 20.1216886
        }
      ]
      substations: []
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
