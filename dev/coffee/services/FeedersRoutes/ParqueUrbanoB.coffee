services.factory 'ParqueUrbanoBFactory', [
  ->
    _parque_urbano_icon = 'img/routes/parque_urbano_van.png'
    _parque_urbano_color = '#0ba9cc'
    _route =
      id: 9
      name: "Parque Urbano B"
      stations: [6, 7, 8]
      visible: true
      icon:
        url: _parque_urbano_icon
      stroke:
        color: _parque_urbano_color
        weight: 5
      path: [
        {
          longitude: -98.7789921
          latitude: 20.0397309
        }
        {
          longitude: -98.7798156
          latitude: 20.0402015
        }
        {
          longitude: -98.7802669
          latitude: 20.0404507
        }
        {
          longitude: -98.7809777
          latitude: 20.0408132
        }
        {
          longitude: -98.7810568
          latitude: 20.0409008
        }
        {
          longitude: -98.7813042
          latitude: 20.0410217
        }
        {
          longitude: -98.78126
          latitude: 20.0415654
        }
        {
          longitude: -98.7814564
          latitude: 20.0416706
        }
        {
          longitude: -98.7823047
          latitude: 20.0421562
        }
        {
          longitude: -98.7829364
          latitude: 20.0424839
        }
        {
          longitude: -98.783397
          latitude: 20.0427513
        }
        {
          longitude: -98.7837598
          latitude: 20.0429431
        }
        {
          longitude: -98.784553
          latitude: 20.043396
        }
        {
          longitude: -98.7848012
          latitude: 20.0435465
        }
        {
          longitude: -98.7852424
          latitude: 20.0437991
        }
        {
          longitude: -98.7862482
          latitude: 20.0443598
        }
        {
          longitude: -98.7874525
          latitude: 20.045054
        }
        {
          longitude: -98.7879782
          latitude: 20.0453324
        }
        {
          longitude: -98.788797
          latitude: 20.0457884
        }
        {
          longitude: -98.7902614
          latitude: 20.046578400000005
        }
        {
          longitude: -98.7911372
          latitude: 20.0470616
        }
        {
          longitude: -98.7908026
          latitude: 20.0477054
        }
        {
          longitude: -98.790525
          latitude: 20.0482351
        }
        {
          longitude: -98.7902923
          latitude: 20.0486723
        }
        {
          longitude: -98.7899416
          latitude: 20.0493236
        }
        {
          longitude: -98.7897894
          latitude: 20.0496209
        }
        {
          longitude: -98.7895788
          latitude: 20.0500329
        }
        {
          longitude: -98.7893622
          latitude: 20.0504373
        }
        {
          longitude: -98.7890893
          latitude: 20.050967
        }
        {
          longitude: -98.7888828
          latitude: 20.0513544
        }
        {
          longitude: -98.7887909
          latitude: 20.0514168
        }
        {
          longitude: -98.7886561
          latitude: 20.0516555
        }
        {
          longitude: -98.7884288
          latitude: 20.0520725
        }
        {
          longitude: -98.7876657
          latitude: 20.0517273
        }
        {
          longitude: -98.7866613
          latitude: 20.0512845
        }
        {
          longitude: -98.78629450000001
          latitude: 20.0511217
        }
        {
          longitude: -98.78593000000001
          latitude: 20.0509357
        }
        {
          longitude: -98.7844605
          latitude: 20.050283000000004
        }
        {
          longitude: -98.7838553
          latitude: 20.0500099
        }
        {
          longitude: -98.7834768
          latitude: 20.0498263
        }
        {
          longitude: -98.7825923
          latitude: 20.0494061
        }
        {
          longitude: -98.7819225
          latitude: 20.0490886
        }
        {
          longitude: -98.7805961
          latitude: 20.0484814
        }
        {
          longitude: -98.7806793
          latitude: 20.0475554
        }
        {
          longitude: -98.7807249
          latitude: 20.0470269
        }
        {
          longitude: -98.7807712
          latitude: 20.0467592
        }
        {
          longitude: -98.78083450000001
          latitude: 20.0460694
        }
        {
          longitude: -98.7809099
          latitude: 20.0453771
        }
        {
          longitude: -98.78098840000001
          latitude: 20.0445954
        }
        {
          longitude: -98.7811359
          latitude: 20.0430294
        }
        {
          longitude: -98.7812868
          latitude: 20.041624
        }
        {
          longitude: -98.78133040000002
          latitude: 20.041006
        }
        {
          longitude: -98.7810581
          latitude: 20.04088
        }
        {
          longitude: -98.780987
          latitude: 20.0407924
        }
        {
          longitude: -98.7802722
          latitude: 20.040426400000005
        }
        {
          longitude: -98.7790062
          latitude: 20.0397171
        }
        {
          longitude: -98.7789921
          latitude: 20.0397309
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7789935
            latitude: 20.0397505
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.7808456
            latitude: 20.040773500000004
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.7812391
            latitude: 20.041476
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.7840877
            latitude: 20.0431459
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.7848776
            latitude: 20.0436171
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.7868845
            latitude: 20.044738400000004
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7894735
            latitude: 20.0461683
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.7908147
            latitude: 20.0476039
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7877536
            latitude: 20.0517462
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.7868349
            latitude: 20.0513192
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.7850291
            latitude: 20.0505122
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7832528
            latitude: 20.0496889
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7814423
            latitude: 20.0488411
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.7810158
            latitude: 20.044509
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.7812405
            latitude: 20.0422577
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.7809656
            latitude: 20.0407672
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
