services.factory 'RealValleFactory', [
  ->
    _real_valle_icon = 'img/routes/real_valle_van.png'
    _real_valle_color = '#009c56'
    _route =
      id: 24
      name: "Real del Valle"
      stations: [21, 22]
      visible: true
      icon:
        url: _real_valle_icon
      stroke:
        color: _real_valle_color
        weight: 5
      path: [
        {
          longitude: -98.753017
          latitude: 20.0889673
        }
        {
          longitude: -98.7529157
          latitude: 20.0889491
        }
        {
          longitude: -98.7528688
          latitude: 20.0891141
        }
        {
          longitude: -98.7528111
          latitude: 20.0892652
        }
        {
          longitude: -98.7526059
          latitude: 20.0895524
        }
        {
          longitude: -98.7525362
          latitude: 20.0896998
        }
        {
          longitude: -98.75222240000001
          latitude: 20.0907205
        }
        {
          longitude: -98.7521245
          latitude: 20.0909719
        }
        {
          longitude: -98.7519762
          latitude: 20.091861
        }
        {
          longitude: -98.7519957
          latitude: 20.0919933
        }
        {
          longitude: -98.7520708
          latitude: 20.0922465
        }
        {
          longitude: -98.752103
          latitude: 20.092402
        }
        {
          longitude: -98.7520903
          latitude: 20.0926916
        }
        {
          longitude: -98.7517034
          latitude: 20.096739
        }
        {
          longitude: -98.7515371
          latitude: 20.0986961
        }
        {
          longitude: -98.7512742
          latitude: 20.1012641
        }
        {
          longitude: -98.7511998
          latitude: 20.1023006
        }
        {
          longitude: -98.7511029
          latitude: 20.1032011
        }
        {
          longitude: -98.7510616
          latitude: 20.103907
        }
        {
          longitude: -98.7510395
          latitude: 20.1040606
        }
        {
          longitude: -98.7509858
          latitude: 20.104286
        }
        {
          longitude: -98.7507619
          latitude: 20.1048439
        }
        {
          longitude: -98.7507042
          latitude: 20.1050316
        }
        {
          longitude: -98.7506616
          latitude: 20.1055061
        }
        {
          longitude: -98.7506533
          latitude: 20.1061821
        }
        {
          longitude: -98.7506091
          latitude: 20.1068382
        }
        {
          longitude: -98.7504856
          latitude: 20.1119096
        }
        {
          longitude: -98.7504957
          latitude: 20.1120834
        }
        {
          longitude: -98.75044140000001
          latitude: 20.1128554
        }
        {
          longitude: -98.7498845
          latitude: 20.11332
        }
        {
          longitude: -98.7489602
          latitude: 20.1140089
        }
        {
          longitude: -98.7480509
          latitude: 20.1137369
        }
        {
          longitude: -98.747904
          latitude: 20.1137117
        }
        {
          longitude: -98.7471624
          latitude: 20.1135052
        }
        {
          longitude: -98.7471047
          latitude: 20.1134775
        }
        {
          longitude: -98.7472509
          latitude: 20.1130568
        }
        {
          longitude: -98.7473601
          latitude: 20.1125871
        }
        {
          longitude: -98.7474044
          latitude: 20.1120941
        }
        {
          longitude: -98.7473877
          latitude: 20.1113864
        }
        {
          longitude: -98.7474688
          latitude: 20.111175400000004
        }
        {
          longitude: -98.7475734
          latitude: 20.1110426
        }
        {
          longitude: -98.7482208
          latitude: 20.1105451
        }
        {
          longitude: -98.74833850000002
          latitude: 20.110439300000003
        }
        {
          longitude: -98.7484331
          latitude: 20.1103259
        }
        {
          longitude: -98.7484464
          latitude: 20.1101667
        }
        {
          longitude: -98.7485008
          latitude: 20.110029400000002
        }
        {
          longitude: -98.748527
          latitude: 20.1090666
        }
        {
          longitude: -98.7485718
          latitude: 20.1082534
        }
        {
          longitude: -98.7485993
          latitude: 20.1059529
        }
        {
          longitude: -98.7486188
          latitude: 20.1058471
        }
        {
          longitude: -98.7508202
          latitude: 20.1062085
        }
        {
          longitude: -98.7508471
          latitude: 20.1052608
        }
        {
          longitude: -98.7508457
          latitude: 20.1050675
        }
        {
          longitude: -98.7508946
          latitude: 20.1048748
        }
        {
          longitude: -98.7509959
          latitude: 20.104656900000002
        }
        {
          longitude: -98.7511481
          latitude: 20.104179
        }
        {
          longitude: -98.7512058
          latitude: 20.1038591
        }
        {
          longitude: -98.7513382
          latitude: 20.1023075
        }
        {
          longitude: -98.7513392
          latitude: 20.1022225
        }
        {
          longitude: -98.7513912
          latitude: 20.1018
        }
        {
          longitude: -98.7514251
          latitude: 20.1012011
        }
        {
          longitude: -98.7514847
          latitude: 20.1011829
        }
        {
          longitude: -98.751523
          latitude: 20.1011438
        }
        {
          longitude: -98.75154370000001
          latitude: 20.1009889
        }
        {
          longitude: -98.75150350000001
          latitude: 20.100931
        }
        {
          longitude: -98.7514593
          latitude: 20.1008976
        }
        {
          longitude: -98.7516846
          latitude: 20.0982743
        }
        {
          longitude: -98.7518449
          latitude: 20.096792500000003
        }
        {
          longitude: -98.7521976
          latitude: 20.0927024
        }
        {
          longitude: -98.7522063
          latitude: 20.092414
        }
        {
          longitude: -98.7521084
          latitude: 20.0919461
        }
        {
          longitude: -98.7521117
          latitude: 20.0918062
        }
        {
          longitude: -98.7521751
          latitude: 20.0913292
        }
        {
          longitude: -98.75224250000001
          latitude: 20.0910121
        }
        {
          longitude: -98.7523336
          latitude: 20.090770900000003
        }
        {
          longitude: -98.7526321
          latitude: 20.0897438
        }
        {
          longitude: -98.7527052
          latitude: 20.0895927
        }
        {
          longitude: -98.7529137
          latitude: 20.089308
        }
        {
          longitude: -98.7529801
          latitude: 20.0891367
        }
        {
          longitude: -98.753017
          latitude: 20.0889673
        }
      ]
      substations: []
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
