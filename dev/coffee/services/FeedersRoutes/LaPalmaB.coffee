services.factory 'LaPalmaBFactory', [
  ->
    _palma_icon = 'img/routes/palma_van.png'
    _palma_color = '#009d56'
    _route =
      id: 7
      name: "La Palma B"
      stations: [5]
      visible: true
      icon:
        url: _palma_icon
      stroke:
        color: _palma_color
        weight: 5
      path: [
        {
          longitude: -98.8080257
          latitude: 20.035085
        }
        {
          longitude: -98.8083107
          latitude: 20.0345703
        }
        {
          longitude: -98.8078178
          latitude: 20.034118
        }
        {
          longitude: -98.8075215
          latitude: 20.033881
        }
        {
          longitude: -98.80704
          latitude: 20.0342515
        }
        {
          longitude: -98.8066377
          latitude: 20.0349747
        }
        {
          longitude: -98.8064627
          latitude: 20.0352966
        }
        {
          longitude: -98.8061797
          latitude: 20.0358069
        }
        {
          longitude: -98.80591350000002
          latitude: 20.0363184
        }
        {
          longitude: -98.8054964
          latitude: 20.0370215
        }
        {
          longitude: -98.8050857
          latitude: 20.037741300000004
        }
        {
          longitude: -98.8046844
          latitude: 20.0384736
        }
        {
          longitude: -98.8043464
          latitude: 20.0390871
        }
        {
          longitude: -98.8031817
          latitude: 20.038483
        }
        {
          longitude: -98.8029483
          latitude: 20.0383438
        }
        {
          longitude: -98.8027559
          latitude: 20.0382688
        }
        {
          longitude: -98.8023274
          latitude: 20.0380405
        }
        {
          longitude: -98.8014899
          latitude: 20.037611200000004
        }
        {
          longitude: -98.8004377
          latitude: 20.0370505
        }
        {
          longitude: -98.800024
          latitude: 20.0378442
        }
        {
          longitude: -98.79961160000002
          latitude: 20.0385186
        }
        {
          longitude: -98.7993615
          latitude: 20.0389649
        }
        {
          longitude: -98.7991268
          latitude: 20.0393719
        }
        {
          longitude: -98.798931
          latitude: 20.0396799
        }
        {
          longitude: -98.7984469
          latitude: 20.0405801
        }
        {
          longitude: -98.79811830000001
          latitude: 20.041185500000005
        }
        {
          longitude: -98.7977274
          latitude: 20.0418532
        }
        {
          longitude: -98.7976496
          latitude: 20.0420605
        }
        {
          longitude: -98.797486
          latitude: 20.0423625
        }
        {
          longitude: -98.7972003
          latitude: 20.0428687
        }
        {
          longitude: -98.7971715
          latitude: 20.0429409
        }
        {
          longitude: -98.79710040000002
          latitude: 20.0433702
        }
        {
          longitude: -98.79688580000001
          latitude: 20.0441677
        }
        {
          longitude: -98.7954019
          latitude: 20.0438212
        }
        {
          longitude: -98.7934137
          latitude: 20.0434269
        }
        {
          longitude: -98.7944926
          latitude: 20.041792800000003
        }
        {
          longitude: -98.7947199
          latitude: 20.0414595
        }
        {
          longitude: -98.7949111
          latitude: 20.0413745
        }
        {
          longitude: -98.7950948
          latitude: 20.041338000000003
        }
        {
          longitude: -98.7952262
          latitude: 20.0413543
        }
        {
          longitude: -98.7956527
          latitude: 20.0412913
        }
        {
          longitude: -98.7958404
          latitude: 20.0412391
        }
        {
          longitude: -98.7960919
          latitude: 20.0410942
        }
        {
          longitude: -98.7963233
          latitude: 20.0410834
        }
        {
          longitude: -98.7977194
          latitude: 20.0418772
        }
        {
          longitude: -98.7977381
          latitude: 20.0418854
        }
        {
          longitude: -98.7978729
          latitude: 20.041640000000005
        }
        {
          longitude: -98.7980533
          latitude: 20.0413448
        }
        {
          longitude: -98.79842810000001
          latitude: 20.040662
        }
        {
          longitude: -98.798805
          latitude: 20.039971
        }
        {
          longitude: -98.7989592
          latitude: 20.0396774
        }
        {
          longitude: -98.799214
          latitude: 20.0392705
        }
        {
          longitude: -98.7996277
          latitude: 20.0385278
        }
        {
          longitude: -98.8000535
          latitude: 20.0378417
        }
        {
          longitude: -98.8004505
          latitude: 20.037082
        }
        {
          longitude: -98.801062
          latitude: 20.0374077
        }
        {
          longitude: -98.8013105
          latitude: 20.0375441
        }
        {
          longitude: -98.80175470000002
          latitude: 20.0377756
        }
        {
          longitude: -98.80243
          latitude: 20.0381214
        }
        {
          longitude: -98.80275050000002
          latitude: 20.0382921
        }
        {
          longitude: -98.8029483
          latitude: 20.0383684
        }
        {
          longitude: -98.8031609
          latitude: 20.0385007
        }
        {
          longitude: -98.8036658
          latitude: 20.038760200000002
        }
        {
          longitude: -98.8043598
          latitude: 20.0391123
        }
        {
          longitude: -98.8048191
          latitude: 20.0382783
        }
        {
          longitude: -98.8052087
          latitude: 20.0375765
        }
        {
          longitude: -98.8056319
          latitude: 20.0368363
        }
        {
          longitude: -98.8059316
          latitude: 20.0363304
        }
        {
          longitude: -98.8061998
          latitude: 20.0358189
        }
        {
          longitude: -98.8064808
          latitude: 20.0353118
        }
        {
          longitude: -98.8067765
          latitude: 20.0347662
        }
        {
          longitude: -98.8070574
          latitude: 20.0342679
        }
        {
          longitude: -98.8072163
          latitude: 20.0344008
        }
        {
          longitude: -98.8080257
          latitude: 20.035085
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.8071895
            latitude: 20.0340978
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.806875
            latitude: 20.034492800000002
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.8059007
            latitude: 20.0362838
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.8050713
            latitude: 20.0377346
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.8029208
            latitude: 20.0383167
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.8004706
            latitude: 20.037047300000005
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7991429
            latitude: 20.0393114
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.7977676
            latitude: 20.0417285
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7968798
            latitude: 20.0440902
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.7955917
            latitude: 20.0438502
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.7936477
            latitude: 20.0431226
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7976724
            latitude: 20.0418803
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.79916030000001
            latitude: 20.0393971
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.800366
            latitude: 20.037276
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.8027847
            latitude: 20.0383261
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.8050813
            latitude: 20.0378247
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.8059141
            latitude: 20.0363997
        }
        {
          id: 18
          options:
            visible: true
          position:
            longitude: -98.8070373
            latitude: 20.0343378
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
