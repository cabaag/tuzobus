services.factory 'TuzosFactory', [
  ->
    _tuzos_icon = 'img/routes/tuzos_van.png'
    _tuzos_color = '#7c3592'
    _route =
      id: 12
      name: "Los Tuzos"
      stations: [9]
      visible: true
      icon:
        url: _tuzos_icon
      stroke:
        color: _tuzos_color
        weight: 5
      path: [
        {
          longitude: -98.7567499
          latitude: 20.0524693
        }
        {
          longitude: -98.7566326
          latitude: 20.0528032
        }
        {
          longitude: -98.7581259
          latitude: 20.0532763
        }
        {
          longitude: -98.7586489
          latitude: 20.0517613
        }
        {
          longitude: -98.7599072
          latitude: 20.0521311
        }
        {
          longitude: -98.7602856
          latitude: 20.052241
        }
        {
          longitude: -98.7605862
          latitude: 20.0523106
        }
        {
          longitude: -98.7607075
          latitude: 20.0517658
        }
        {
          longitude: -98.7608389
          latitude: 20.051168
        }
        {
          longitude: -98.7627433
          latitude: 20.0519604
        }
        {
          longitude: -98.7645834
          latitude: 20.0527226
        }
        {
          longitude: -98.763485
          latitude: 20.056153
        }
        {
          longitude: -98.7631457
          latitude: 20.057626
        }
        {
          longitude: -98.7630009
          latitude: 20.0582566
        }
        {
          longitude: -98.7628332
          latitude: 20.058852
        }
        {
          longitude: -98.7717555
          latitude: 20.0608027
        }
        {
          longitude: -98.7720754
          latitude: 20.0608286
        }
        {
          longitude: -98.7727356
          latitude: 20.0609521
        }
        {
          longitude: -98.7732309
          latitude: 20.0610152
        }
        {
          longitude: -98.7742201
          latitude: 20.0612396
        }
        {
          longitude: -98.7749165
          latitude: 20.0612651
        }
        {
          longitude: -98.7756172
          latitude: 20.060529400000004
        }
        {
          longitude: -98.7764132
          latitude: 20.0596803
        }
        {
          longitude: -98.7776712
          latitude: 20.0582858
        }
        {
          longitude: -98.7787984
          latitude: 20.0570971
        }
        {
          longitude: -98.7789646
          latitude: 20.0569045
        }
        {
          longitude: -98.7804868
          latitude: 20.0553051
        }
        {
          longitude: -98.7806447
          latitude: 20.0552132
        }
        {
          longitude: -98.7808007
          latitude: 20.0551641
        }
        {
          longitude: -98.7810045
          latitude: 20.0551458
        }
        {
          longitude: -98.7812304
          latitude: 20.0551855
        }
        {
          longitude: -98.78252590000001
          latitude: 20.0558891
        }
        {
          longitude: -98.784036
          latitude: 20.05674
        }
        {
          longitude: -98.7840762
          latitude: 20.0571966
        }
        {
          longitude: -98.7841068
          latitude: 20.0573595
        }
        {
          longitude: -98.78416350000002
          latitude: 20.0574719
        }
        {
          longitude: -98.7852089
          latitude: 20.0580527
        }
        {
          longitude: -98.7852494
          latitude: 20.0580968
        }
        {
          longitude: -98.7852672
          latitude: 20.0581762
        }
        {
          longitude: -98.7852725
          latitude: 20.0582656
        }
        {
          longitude: -98.7852001
          latitude: 20.0584061
        }
        {
          longitude: -98.7846308
          latitude: 20.0594561
        }
        {
          longitude: -98.78458180000001
          latitude: 20.0594904
        }
        {
          longitude: -98.7845041
          latitude: 20.0595713
        }
        {
          longitude: -98.784207
          latitude: 20.0601439
        }
        {
          longitude: -98.7815348
          latitude: 20.0586964
        }
        {
          longitude: -98.778844
          latitude: 20.0572874
        }
        {
          longitude: -98.7787984
          latitude: 20.0570971
        }
        {
          longitude: -98.7789646
          latitude: 20.0569045
        }
        {
          longitude: -98.7804868
          latitude: 20.0553051
        }
        {
          longitude: -98.7806447
          latitude: 20.0552132
        }
        {
          longitude: -98.7808007
          latitude: 20.0551641
        }
        {
          longitude: -98.7810045
          latitude: 20.0551458
        }
        {
          longitude: -98.7812304
          latitude: 20.0551855
        }
        {
          longitude: -98.78252590000001
          latitude: 20.0558891
        }
        {
          longitude: -98.7826041
          latitude: 20.0557772
        }
        {
          longitude: -98.7815314
          latitude: 20.0551825
        }
        {
          longitude: -98.7812299
          latitude: 20.0550594
        }
        {
          longitude: -98.7810972
          latitude: 20.0550136
        }
        {
          longitude: -98.780862
          latitude: 20.0550088
        }
        {
          longitude: -98.7806697
          latitude: 20.0550597
        }
        {
          longitude: -98.7804446
          latitude: 20.0551678
        }
        {
          longitude: -98.7801797
          latitude: 20.0553839
        }
        {
          longitude: -98.7774657
          latitude: 20.0582131
        }
        {
          longitude: -98.77734880000001
          latitude: 20.0582246
        }
        {
          longitude: -98.7759452
          latitude: 20.059640000000005
        }
        {
          longitude: -98.775327
          latitude: 20.0602725
        }
        {
          longitude: -98.7747034
          latitude: 20.0609554
        }
        {
          longitude: -98.7745966
          latitude: 20.061126
        }
        {
          longitude: -98.7745313
          latitude: 20.0611205
        }
        {
          longitude: -98.7744928
          latitude: 20.0610471
        }
        {
          longitude: -98.7729594
          latitude: 20.060204700000003
        }
        {
          longitude: -98.7728212
          latitude: 20.0601601
        }
        {
          longitude: -98.7723527
          latitude: 20.059939
        }
        {
          longitude: -98.7721304
          latitude: 20.0598742
        }
        {
          longitude: -98.771997
          latitude: 20.0598762
        }
        {
          longitude: -98.7716479
          latitude: 20.0606616
        }
        {
          longitude: -98.76745030000001
          latitude: 20.0597477
        }
        {
          longitude: -98.7630499
          latitude: 20.0587906
        }
        {
          longitude: -98.7633385
          latitude: 20.0575486
        }
        {
          longitude: -98.7637188
          latitude: 20.0560357
        }
        {
          longitude: -98.7643026
          latitude: 20.0541839
        }
        {
          longitude: -98.7647973
          latitude: 20.0527796
        }
        {
          longitude: -98.76273090000001
          latitude: 20.0519227
        }
        {
          longitude: -98.760822
          latitude: 20.051134
        }
        {
          longitude: -98.7605672
          latitude: 20.0522908
        }
        {
          longitude: -98.7602647
          latitude: 20.052212500000003
        }
        {
          longitude: -98.7599192
          latitude: 20.0521121
        }
        {
          longitude: -98.7586321
          latitude: 20.0517331
        }
        {
          longitude: -98.7585332
          latitude: 20.0520105
        }
        {
          longitude: -98.757034
          latitude: 20.0515886
        }
        {
          longitude: -98.7567499
          latitude: 20.0524693
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7566587
            latitude: 20.0526079
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.7570651
            latitude: 20.0516417
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.7582298
            latitude: 20.0519452
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.7590646
            latitude: 20.0519201
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.7603334
            latitude: 20.052264
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.7608463
            latitude: 20.0511957
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7624201
            latitude: 20.0518376
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.7645277
            latitude: 20.0527163
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7640683
            latitude: 20.054247600000004
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.76358890000002
            latitude: 20.0557215
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.76311550000001
            latitude: 20.0575891
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7629358
            latitude: 20.0588986
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7642306
            latitude: 20.0591878
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.7657655
            latitude: 20.0595077
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.7679777
            latitude: 20.0600053
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.7696379
            latitude: 20.0603795
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.771553
            latitude: 20.0608128
        }
        {
          id: 18
          options:
            visible: true
          position:
            longitude: -98.7727037
            latitude: 20.0609678
        }
        {
          id: 19
          options:
            visible: true
          position:
            longitude: -98.7761617
            latitude: 20.0599864
        }
        {
          id: 20
          options:
            visible: true
          position:
            longitude: -98.7773218
            latitude: 20.058716
        }
        {
          id: 21
          options:
            visible: true
          position:
            longitude: -98.7814484
            latitude: 20.055331600000002
        }
        {
          id: 22
          options:
            visible: true
          position:
            longitude: -98.7842104
            latitude: 20.0600815
        }
        {
          id: 23
          options:
            visible: true
          position:
            longitude: -98.7821961
            latitude: 20.0590296
        }
        {
          id: 24
          options:
            visible: true
          position:
            longitude: -98.7802307
            latitude: 20.0579809
        }
        {
          id: 25
          options:
            visible: true
          position:
            longitude: -98.7815081
            latitude: 20.055154
        }
        {
          id: 26
          options:
            visible: true
          position:
            longitude: -98.7794897
            latitude: 20.0559949
        }
        {
          id: 27
          options:
            visible: true
          position:
            longitude: -98.7764353
            latitude: 20.0590889
        }
        {
          id: 28
          options:
            visible: true
          position:
            longitude: -98.77444580000001
            latitude: 20.060962700000005
        }
        {
          id: 29
          options:
            visible: true
          position:
            longitude: -98.7727191
            latitude: 20.0600941
        }
        {
          id: 30
          options:
            visible: true
          position:
            longitude: -98.7716637
            latitude: 20.0605483
        }
        {
          id: 31
          options:
            visible: true
          position:
            longitude: -98.7698632
            latitude: 20.0602421
        }
        {
          id: 32
          options:
            visible: true
          position:
            longitude: -98.7682238
            latitude: 20.0598762
        }
        {
          id: 33
          options:
            visible: true
          position:
            longitude: -98.7661685
            latitude: 20.0594309
        }
        {
          id: 34
          options:
            visible: true
          position:
            longitude: -98.7644727
            latitude: 20.0590857
        }
        {
          id: 35
          options:
            visible: true
          position:
            longitude: -98.7631168
            latitude: 20.0587796
        }
        {
          id: 36
          options:
            visible: true
          position:
            longitude: -98.7633334
            latitude: 20.057712
        }
        {
          id: 36
          options:
            visible: true
          position:
            longitude: -98.76378940000001
            latitude: 20.055845
        }
        {
          id: 37
          options:
            visible: true
          position:
            longitude: -98.7641991
            latitude: 20.054599
        }
        {
          id: 38
          options:
            visible: true
          position:
            longitude: -98.7647979
            latitude: 20.0528265
        }
        {
          id: 39
          options:
            visible: true
          position:
            longitude: -98.7628848
            latitude: 20.0519743
        }
        {
          id: 40
          options:
            visible: true
          position:
            longitude: -98.7608919
            latitude: 20.0511484
        }
        {
          id: 41
          options:
            visible: true
          position:
            longitude: -98.7605601
            latitude: 20.0522571
        }
        {
          id: 42
          options:
            visible: true
          position:
            longitude: -98.7594288
            latitude: 20.0519484
        }
        {
          id: 43
          options:
            visible: true
          position:
            longitude: -98.7586986
            latitude: 20.0517418
        }
        {
          id: 44
          options:
            visible: true
          position:
            longitude: -98.7580944
            latitude: 20.0532259
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
