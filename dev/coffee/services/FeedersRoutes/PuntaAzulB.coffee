services.factory 'PuntaAzulBFactory', [
  ->
    _punta_azul_icon = 'img/routes/punta_azul_van.png'
    _punta_azul_color = '#0ba9cc'
    _route =
      id: 22
      name: "Punta Azul A"
      stations: [18, 22]
      visible: true
      icon:
        url: _punta_azul_icon
      stroke:
        color: _punta_azul_color
        weight: 5
      path: [
        {
          longitude: -98.7845503
          latitude: 20.1219681
        }
        {
          longitude: -98.7845503
          latitude: 20.1218554
        }
        {
          longitude: -98.777458
          latitude: 20.1215513
        }
        {
          longitude: -98.7761235
          latitude: 20.1215267
        }
        {
          longitude: -98.7759854
          latitude: 20.1215135
        }
        {
          longitude: -98.7755354
          latitude: 20.1213549
        }
        {
          longitude: -98.7753249
          latitude: 20.1213253
        }
        {
          longitude: -98.7752947
          latitude: 20.1213007
        }
        {
          longitude: -98.7751968
          latitude: 20.1210829
        }
        {
          longitude: -98.7748381
          latitude: 20.1205534
        }
        {
          longitude: -98.7747871
          latitude: 20.120404800000003
        }
        {
          longitude: -98.7747696
          latitude: 20.120197
        }
        {
          longitude: -98.77457520000002
          latitude: 20.115141
        }
        {
          longitude: -98.774537
          latitude: 20.1148967
        }
        {
          longitude: -98.7744673
          latitude: 20.1147947
        }
        {
          longitude: -98.7731422
          latitude: 20.113689
        }
        {
          longitude: -98.7731074
          latitude: 20.1136185
        }
        {
          longitude: -98.7730363
          latitude: 20.1131223
        }
        {
          longitude: -98.7701368
          latitude: 20.1135064
        }
        {
          longitude: -98.7700544
          latitude: 20.1115519
        }
        {
          longitude: -98.7699524
          latitude: 20.1094469
        }
        {
          longitude: -98.7698599
          latitude: 20.109467
        }
        {
          longitude: -98.7698123
          latitude: 20.1090578
        }
        {
          longitude: -98.7691069
          latitude: 20.1091989
        }
        {
          longitude: -98.7680705
          latitude: 20.1094435
        }
        {
          longitude: -98.7676732
          latitude: 20.1095275
        }
        {
          longitude: -98.7676021
          latitude: 20.1095408
        }
        {
          longitude: -98.766023
          latitude: 20.1098694
        }
        {
          longitude: -98.7654054
          latitude: 20.1100212
        }
        {
          longitude: -98.7645853
          latitude: 20.1102044
        }
        {
          longitude: -98.7633434
          latitude: 20.1104594
        }
        {
          longitude: -98.7606345
          latitude: 20.1110671
        }
        {
          longitude: -98.7596125
          latitude: 20.1112931
        }
        {
          longitude: -98.7576095
          latitude: 20.114551
        }
        {
          longitude: -98.7538123
          latitude: 20.1137469
        }
        {
          longitude: -98.7537224
          latitude: 20.1137218
        }
        {
          longitude: -98.753656
          latitude: 20.1136922
        }
        {
          longitude: -98.7523866
          latitude: 20.1156976
        }
        {
          longitude: -98.7523363
          latitude: 20.1157542
        }
        {
          longitude: -98.7522593
          latitude: 20.1157775
        }
        {
          longitude: -98.7496065
          latitude: 20.1161157
        }
        {
          longitude: -98.7497561
          latitude: 20.1150427
        }
        {
          longitude: -98.7497614
          latitude: 20.1148822
        }
        {
          longitude: -98.749742
          latitude: 20.1147915
        }
        {
          longitude: -98.7492042
          latitude: 20.1139818
        }
        {
          longitude: -98.7500511
          latitude: 20.1133471
        }
        {
          longitude: -98.7506425
          latitude: 20.112924
        }
        {
          longitude: -98.7507559
          latitude: 20.1127993
        }
        {
          longitude: -98.7509047
          latitude: 20.1126563
        }
        {
          longitude: -98.7512112
          latitude: 20.1125436
        }
        {
          longitude: -98.7519139
          latitude: 20.1129378
        }
        {
          longitude: -98.7522137
          latitude: 20.113121
        }
        {
          longitude: -98.7524517
          latitude: 20.1132514
        }
        {
          longitude: -98.7527266
          latitude: 20.1133553
        }
        {
          longitude: -98.7527809
          latitude: 20.1133553
        }
        {
          longitude: -98.7537157
          latitude: 20.1137419
        }
        {
          longitude: -98.7537881
          latitude: 20.1137626
        }
        {
          longitude: -98.7576068
          latitude: 20.1145655
        }
        {
          longitude: -98.7593087
          latitude: 20.1149187
        }
        {
          longitude: -98.7594181
          latitude: 20.1149565
        }
        {
          longitude: -98.7594757
          latitude: 20.1149911
        }
        {
          longitude: -98.7595984
          latitude: 20.1149723
        }
        {
          longitude: -98.759876
          latitude: 20.113871
        }
        {
          longitude: -98.7601462
          latitude: 20.112952300000003
        }
        {
          longitude: -98.7603817
          latitude: 20.1119146
        }
        {
          longitude: -98.7606398
          latitude: 20.1110828
        }
        {
          longitude: -98.7608799
          latitude: 20.1110343
        }
        {
          longitude: -98.7633535
          latitude: 20.1104777
        }
        {
          longitude: -98.7645904
          latitude: 20.1102271
        }
        {
          longitude: -98.7654141
          latitude: 20.1100439
        }
        {
          longitude: -98.766027
          latitude: 20.1098959
        }
        {
          longitude: -98.7676062
          latitude: 20.1095634
        }
        {
          longitude: -98.7676652
          latitude: 20.1095565
        }
        {
          longitude: -98.7680756
          latitude: 20.1094671
        }
        {
          longitude: -98.7691336
          latitude: 20.1092171
        }
        {
          longitude: -98.7697935
          latitude: 20.109083
        }
        {
          longitude: -98.7698404
          latitude: 20.1094822
        }
        {
          longitude: -98.7700195
          latitude: 20.1135247
        }
        {
          longitude: -98.769996
          latitude: 20.1136443
        }
        {
          longitude: -98.7728754
          latitude: 20.1132583
        }
        {
          longitude: -98.7729404
          latitude: 20.1137205
        }
        {
          longitude: -98.7730027
          latitude: 20.1137961
        }
        {
          longitude: -98.7740427
          latitude: 20.1146562
        }
        {
          longitude: -98.77435860000001
          latitude: 20.1149294
        }
        {
          longitude: -98.7743881
          latitude: 20.1150327
        }
        {
          longitude: -98.7744411
          latitude: 20.1167774
        }
        {
          longitude: -98.774466
          latitude: 20.117193
        }
        {
          longitude: -98.7745202
          latitude: 20.1176312
        }
        {
          longitude: -98.7746114
          latitude: 20.1204054
        }
        {
          longitude: -98.7746456
          latitude: 20.1205445
        }
        {
          longitude: -98.7751895
          latitude: 20.1213115
        }
        {
          longitude: -98.7751773
          latitude: 20.1213599
        }
        {
          longitude: -98.7751895
          latitude: 20.1214077
        }
        {
          longitude: -98.775225
          latitude: 20.1214455
        }
        {
          longitude: -98.7752746
          latitude: 20.1214455
        }
        {
          longitude: -98.7753289
          latitude: 20.1214172
        }
        {
          longitude: -98.775976
          latitude: 20.1216678
        }
        {
          longitude: -98.7761262
          latitude: 20.1216829
        }
        {
          longitude: -98.7770764
          latitude: 20.1217027
        }
        {
          longitude: -98.777474
          latitude: 20.1217264
        }
        {
          longitude: -98.7782378
          latitude: 20.1217414
        }
        {
          longitude: -98.7794602
          latitude: 20.1218114
        }
        {
          longitude: -98.7798404
          latitude: 20.121812600000002
        }
        {
          longitude: -98.7804539
          latitude: 20.1218239
        }
        {
          longitude: -98.7845363
          latitude: 20.1220116
        }
        {
          longitude: -98.7845503
          latitude: 20.1219681
        }
      ]
      substations: []
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
