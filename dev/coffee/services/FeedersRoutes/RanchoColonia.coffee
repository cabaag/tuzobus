services.factory 'RanchoColoniaFactory', [
  ->
    _rancho_colonia_icon = 'img/routes/rancho_colonia_van.png'
    _rancho_colonia_color = '#3f59a9'
    _route =
      id: 11
      name: "Rancho la Colonia"
      stations: [8]
      visible: true
      icon:
        url: _rancho_colonia_icon
      stroke:
        color: _rancho_colonia_color
        weight: 5
      path: [
        {
          longitude: -98.7711641
          latitude: 20.0462729
        }
        {
          longitude: -98.7715242
          latitude: 20.046448600000005
        }
        {
          longitude: -98.7724891
          latitude: 20.0469853
        }
        {
          longitude: -98.7728371
          latitude: 20.0471541
        }
        {
          longitude: -98.7752055
          latitude: 20.0484619
        }
        {
          longitude: -98.7767264
          latitude: 20.0493066
        }
        {
          longitude: -98.7770469
          latitude: 20.0494792
        }
        {
          longitude: -98.7774331
          latitude: 20.0495611
        }
        {
          longitude: -98.777521
          latitude: 20.0495711
        }
        {
          longitude: -98.7775853
          latitude: 20.049590700000003
        }
        {
          longitude: -98.777816
          latitude: 20.0496965
        }
        {
          longitude: -98.7777838
          latitude: 20.0497589
        }
        {
          longitude: -98.7777711
          latitude: 20.0498823
        }
        {
          longitude: -98.7774398
          latitude: 20.0504763
        }
        {
          longitude: -98.7772058
          latitude: 20.050955100000003
        }
        {
          longitude: -98.7769584
          latitude: 20.051459
        }
        {
          longitude: -98.7767337
          latitude: 20.0518728
        }
        {
          longitude: -98.7764923
          latitude: 20.0522999
        }
        {
          longitude: -98.7762107
          latitude: 20.052795
        }
        {
          longitude: -98.7760115
          latitude: 20.0531919
        }
        {
          longitude: -98.7759431
          latitude: 20.0532353
        }
        {
          longitude: -98.7758935
          latitude: 20.0532838
        }
        {
          longitude: -98.7759009
          latitude: 20.0533304
        }
        {
          longitude: -98.7759847
          latitude: 20.0533959
        }
        {
          longitude: -98.7762798
          latitude: 20.053554700000003
        }
        {
          longitude: -98.7763877
          latitude: 20.0535912
        }
        {
          longitude: -98.776438
          latitude: 20.0535799
        }
        {
          longitude: -98.7764394
          latitude: 20.0535087
        }
        {
          longitude: -98.77638840000002
          latitude: 20.05335
        }
        {
          longitude: -98.7769664
          latitude: 20.0532038
        }
        {
          longitude: -98.7779159
          latitude: 20.0529771
        }
        {
          longitude: -98.7785268
          latitude: 20.0528265
        }
        {
          longitude: -98.7785791
          latitude: 20.0528851
        }
        {
          longitude: -98.7786998
          latitude: 20.0529481
        }
        {
          longitude: -98.7788272
          latitude: 20.052967
        }
        {
          longitude: -98.7788869
          latitude: 20.0529468
        }
        {
          longitude: -98.7789579
          latitude: 20.0528574
        }
        {
          longitude: -98.7790089
          latitude: 20.0527572
        }
        {
          longitude: -98.7799879
          latitude: 20.052515300000003
        }
        {
          longitude: -98.77998460000002
          latitude: 20.0525544
        }
        {
          longitude: -98.7799966
          latitude: 20.0526136
        }
        {
          longitude: -98.7800664
          latitude: 20.0526848
        }
        {
          longitude: -98.78013610000002
          latitude: 20.0527087
        }
        {
          longitude: -98.78023130000001
          latitude: 20.0527075
        }
        {
          longitude: -98.7803205
          latitude: 20.0526766
        }
        {
          longitude: -98.7807973
          latitude: 20.0525947
        }
        {
          longitude: -98.781557
          latitude: 20.0524089
        }
        {
          longitude: -98.7820311
          latitude: 20.052622400000004
        }
        {
          longitude: -98.7821746
          latitude: 20.0531364
        }
        {
          longitude: -98.7819794
          latitude: 20.053486
        }
        {
          longitude: -98.7823657
          latitude: 20.053697
        }
        {
          longitude: -98.7835613
          latitude: 20.054366
        }
        {
          longitude: -98.7840659
          latitude: 20.0546526
        }
        {
          longitude: -98.7848313
          latitude: 20.055115
        }
        {
          longitude: -98.78490570000001
          latitude: 20.0551401
        }
        {
          longitude: -98.7849433
          latitude: 20.0551275
        }
        {
          longitude: -98.7850033
          latitude: 20.055073
        }
        {
          longitude: -98.78510220000001
          latitude: 20.0550211
        }
        {
          longitude: -98.7852772
          latitude: 20.0551017
        }
        {
          longitude: -98.7854449
          latitude: 20.0552264
        }
        {
          longitude: -98.78557560000002
          latitude: 20.0552636
        }
        {
          longitude: -98.7861409
          latitude: 20.0552894
        }
        {
          longitude: -98.7866231
          latitude: 20.0553354
        }
        {
          longitude: -98.786729
          latitude: 20.0551231
        }
        {
          longitude: -98.7863515
          latitude: 20.0551811
        }
        {
          longitude: -98.7856031
          latitude: 20.0551704
        }
        {
          longitude: -98.7855508
          latitude: 20.0551521
        }
        {
          longitude: -98.7852765
          latitude: 20.0550009
        }
        {
          longitude: -98.7851532
          latitude: 20.0549216
        }
        {
          longitude: -98.7844411
          latitude: 20.0544378
        }
        {
          longitude: -98.7843297
          latitude: 20.0544044
        }
        {
          longitude: -98.7842594
          latitude: 20.0544252
        }
        {
          longitude: -98.7841078
          latitude: 20.0545461
        }
        {
          longitude: -98.7824086
          latitude: 20.0535906
        }
        {
          longitude: -98.7823717
          latitude: 20.0534791
        }
        {
          longitude: -98.782296
          latitude: 20.0531282
        }
        {
          longitude: -98.7820995
          latitude: 20.0524498
        }
        {
          longitude: -98.7820338
          latitude: 20.0521544
        }
        {
          longitude: -98.7815128
          latitude: 20.0522987
        }
        {
          longitude: -98.78117210000002
          latitude: 20.0523661
        }
        {
          longitude: -98.7803647
          latitude: 20.0525695
        }
        {
          longitude: -98.78035430000001
          latitude: 20.0524908
        }
        {
          longitude: -98.7803124
          latitude: 20.0524177
        }
        {
          longitude: -98.7802629
          latitude: 20.0523837
        }
        {
          longitude: -98.7802045
          latitude: 20.0523661
        }
        {
          longitude: -98.7801422
          latitude: 20.052363600000003
        }
        {
          longitude: -98.7800865
          latitude: 20.0523868
        }
        {
          longitude: -98.7800436
          latitude: 20.0524146
        }
        {
          longitude: -98.7795933
          latitude: 20.0525204
        }
        {
          longitude: -98.779196
          latitude: 20.0526004
        }
        {
          longitude: -98.7789935
          latitude: 20.0526527
        }
        {
          longitude: -98.7789063
          latitude: 20.0525802
        }
        {
          longitude: -98.7788399
          latitude: 20.0525475
        }
        {
          longitude: -98.7787782
          latitude: 20.0525223
        }
        {
          longitude: -98.7787306
          latitude: 20.0525217
        }
        {
          longitude: -98.7786052
          latitude: 20.0520561
        }
        {
          longitude: -98.7785187
          latitude: 20.0517374
        }
        {
          longitude: -98.7783578
          latitude: 20.051150300000003
        }
        {
          longitude: -98.7781245
          latitude: 20.0502937
        }
        {
          longitude: -98.7780031
          latitude: 20.049871
        }
        {
          longitude: -98.7780815
          latitude: 20.0497778
        }
        {
          longitude: -98.7781365
          latitude: 20.0496631
        }
        {
          longitude: -98.7781519
          latitude: 20.0496014
        }
        {
          longitude: -98.778101
          latitude: 20.0495308
        }
        {
          longitude: -98.7780084
          latitude: 20.0494974
        }
        {
          longitude: -98.7779481
          latitude: 20.0495208
        }
        {
          longitude: -98.7778737
          latitude: 20.0496102
        }
        {
          longitude: -98.7777282
          latitude: 20.0495441
        }
        {
          longitude: -98.7775813
          latitude: 20.0494899
        }
        {
          longitude: -98.7773956
          latitude: 20.0494458
        }
        {
          longitude: -98.7771468
          latitude: 20.0494011
        }
        {
          longitude: -98.7760478
          latitude: 20.0487995
        }
        {
          longitude: -98.77502450000001
          latitude: 20.0482452
        }
        {
          longitude: -98.7743445
          latitude: 20.0478691
        }
        {
          longitude: -98.7735862
          latitude: 20.0474445
        }
        {
          longitude: -98.7727178
          latitude: 20.0469765
        }
        {
          longitude: -98.7721696
          latitude: 20.046683
        }
        {
          longitude: -98.7716335
          latitude: 20.0463806
        }
        {
          longitude: -98.7712038
          latitude: 20.0461639
        }
        {
          longitude: -98.7711641
          latitude: 20.0462729
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7712158
            latitude: 20.046322
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.7750238
            latitude: 20.0483831
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.7777764
            latitude: 20.0497034
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.777075
            latitude: 20.051171100000005
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.7761148
            latitude: 20.053491100000002
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.7779803
            latitude: 20.0529871
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7798753
            latitude: 20.0525688
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.7816965
            latitude: 20.0524977
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7840911
            latitude: 20.0546853
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.78648620000001
            latitude: 20.0553423
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.78363300000001
            latitude: 20.0542627
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7819714
            latitude: 20.0521538
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7799356
            latitude: 20.0524164
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.7787628
            latitude: 20.0524694
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.7785234
            latitude: 20.0516631
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.77806010000002
            latitude: 20.0498433
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.774877
            latitude: 20.04814
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
