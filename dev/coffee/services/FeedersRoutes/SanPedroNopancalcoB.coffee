services.factory 'SanPedroNopancalcoBFactory', [
  ->
    _san_pedro_nopancalco_icon = 'img/routes/san_pedro_nopancalco_van.png'
    _san_pedro_nopancalco_color = '#F4EB37'
    _route =
      id: 19
      name: "San Pedro Nopalcalco B"
      stations: [17]
      visible: true
      icon:
        url: _san_pedro_nopancalco_icon
      stroke:
        color: _san_pedro_nopancalco_color
        weight: 5
      path: [
        {
          longitude: -98.79631920000001
          latitude: 20.0890039
        }
        {
          longitude: -98.7962052
          latitude: 20.0889154
        }
        {
          longitude: -98.7958975
          latitude: 20.0887179
        }
        {
          longitude: -98.795359
          latitude: 20.0884214
        }
        {
          longitude: -98.794437
          latitude: 20.0880082
        }
        {
          longitude: -98.7937517
          latitude: 20.087682
        }
        {
          longitude: -98.79400250000002
          latitude: 20.0872028
        }
        {
          longitude: -98.7942398
          latitude: 20.0867148
        }
        {
          longitude: -98.7947569
          latitude: 20.0857852
        }
        {
          longitude: -98.7950794
          latitude: 20.0852001
        }
        {
          longitude: -98.79478290000002
          latitude: 20.0849079
        }
        {
          longitude: -98.7947193
          latitude: 20.0848159
        }
        {
          longitude: -98.7947072
          latitude: 20.0847284
        }
        {
          longitude: -98.7946489
          latitude: 20.0847076
        }
        {
          longitude: -98.79463380000001
          latitude: 20.0848175
        }
        {
          longitude: -98.794433
          latitude: 20.085180600000005
        }
        {
          longitude: -98.7942318
          latitude: 20.0854577
        }
        {
          longitude: -98.7940789
          latitude: 20.0853796
        }
        {
          longitude: -98.7939985
          latitude: 20.085374
        }
        {
          longitude: -98.79347
          latitude: 20.0863413
        }
        {
          longitude: -98.793222
          latitude: 20.086816
        }
        {
          longitude: -98.79315420000002
          latitude: 20.0869081
        }
        {
          longitude: -98.7929571
          latitude: 20.0872935
        }
        {
          longitude: -98.7925277
          latitude: 20.0881091
        }
        {
          longitude: -98.7922656
          latitude: 20.0885524
        }
        {
          longitude: -98.7920371
          latitude: 20.0889793
        }
        {
          longitude: -98.7911017
          latitude: 20.088496900000003
        }
        {
          longitude: -98.7908562
          latitude: 20.0890083
        }
        {
          longitude: -98.7905894
          latitude: 20.0895738
        }
        {
          longitude: -98.7902414
          latitude: 20.0903044
        }
        {
          longitude: -98.7894662
          latitude: 20.0899844
        }
        {
          longitude: -98.7886769
          latitude: 20.0896513
        }
        {
          longitude: -98.78745990000002
          latitude: 20.0891588
        }
        {
          longitude: -98.787014
          latitude: 20.088997
        }
        {
          longitude: -98.7865909
          latitude: 20.0888364
        }
        {
          longitude: -98.7864212
          latitude: 20.0887501
        }
        {
          longitude: -98.7862066
          latitude: 20.0885939
        }
        {
          longitude: -98.7861047
          latitude: 20.0884377
        }
        {
          longitude: -98.785925
          latitude: 20.0881367
        }
        {
          longitude: -98.7859196
          latitude: 20.0879723
        }
        {
          longitude: -98.7859357
          latitude: 20.0871675
        }
        {
          longitude: -98.7859458
          latitude: 20.0866139
        }
        {
          longitude: -98.78599070000001
          latitude: 20.0860875
        }
        {
          longitude: -98.7860745
          latitude: 20.0855786
        }
        {
          longitude: -98.7862194
          latitude: 20.0848581
        }
        {
          longitude: -98.7863615
          latitude: 20.084178
        }
        {
          longitude: -98.78646810000001
          latitude: 20.0835872
        }
        {
          longitude: -98.7861778
          latitude: 20.0835489
        }
        {
          longitude: -98.7860833
          latitude: 20.0839783
        }
        {
          longitude: -98.78600280000002
          latitude: 20.0846755
        }
        {
          longitude: -98.78594650000001
          latitude: 20.0848506
        }
        {
          longitude: -98.7858171
          latitude: 20.0853469
        }
        {
          longitude: -98.785634
          latitude: 20.0864363
        }
        {
          longitude: -98.7856719
          latitude: 20.087216600000005
        }
        {
          longitude: -98.7857044
          latitude: 20.0879239
        }
        {
          longitude: -98.7857895
          latitude: 20.0880397
        }
        {
          longitude: -98.7857829
          latitude: 20.0882186
        }
        {
          longitude: -98.7857573
          latitude: 20.0901362
        }
        {
          longitude: -98.7857473
          latitude: 20.0920323
        }
        {
          longitude: -98.7857326
          latitude: 20.0920827
        }
        {
          longitude: -98.7845216
          latitude: 20.0917357
        }
        {
          longitude: -98.7838604
          latitude: 20.0915323
        }
        {
          longitude: -98.78256290000002
          latitude: 20.0911224
        }
        {
          longitude: -98.7812392
          latitude: 20.0907149
        }
        {
          longitude: -98.7803112
          latitude: 20.090422700000005
        }
        {
          longitude: -98.7789305
          latitude: 20.0900008
        }
        {
          longitude: -98.778917
          latitude: 20.0897828
        }
        {
          longitude: -98.7789097
          latitude: 20.0888458
        }
        {
          longitude: -98.7788889
          latitude: 20.088386700000004
        }
        {
          longitude: -98.7788514
          latitude: 20.0872921
        }
        {
          longitude: -98.7788158
          latitude: 20.0863532
        }
        {
          longitude: -98.7786656
          latitude: 20.0862443
        }
        {
          longitude: -98.7781701
          latitude: 20.0867065
        }
        {
          longitude: -98.7778643
          latitude: 20.0870056
        }
        {
          longitude: -98.7773606
          latitude: 20.0875038
        }
        {
          longitude: -98.7762093
          latitude: 20.0885637
        }
        {
          longitude: -98.77552480000001
          latitude: 20.0892243
        }
        {
          longitude: -98.774999
          latitude: 20.0897193
        }
        {
          longitude: -98.774698
          latitude: 20.0900184
        }
        {
          longitude: -98.77462220000001
          latitude: 20.0902023
        }
        {
          longitude: -98.7745421
          latitude: 20.090344
        }
        {
          longitude: -98.7744512
          latitude: 20.0904668
        }
        {
          longitude: -98.7742875
          latitude: 20.0906387
        }
        {
          longitude: -98.77513580000002
          latitude: 20.0913314
        }
        {
          longitude: -98.775215
          latitude: 20.0914234
        }
        {
          longitude: -98.7752414
          latitude: 20.0914861
        }
        {
          longitude: -98.7752451
          latitude: 20.0915903
        }
        {
          longitude: -98.77522100000002
          latitude: 20.0918037
        }
        {
          longitude: -98.7751767
          latitude: 20.0920022
        }
        {
          longitude: -98.775168
          latitude: 20.0921155
        }
        {
          longitude: -98.7751807
          latitude: 20.0922313
        }
        {
          longitude: -98.7754077
          latitude: 20.0927389
        }
        {
          longitude: -98.7755126
          latitude: 20.0929952
        }
        {
          longitude: -98.7754999
          latitude: 20.0931696
        }
        {
          longitude: -98.7753061
          latitude: 20.0936262
        }
        {
          longitude: -98.7750587
          latitude: 20.0941729
        }
        {
          longitude: -98.7749353
          latitude: 20.0944543
        }
        {
          longitude: -98.7746771
          latitude: 20.0948933
        }
        {
          longitude: -98.7738631
          latitude: 20.0962478
        }
        {
          longitude: -98.7738282
          latitude: 20.0963322
        }
        {
          longitude: -98.7738098
          latitude: 20.0964266
        }
        {
          longitude: -98.7738142
          latitude: 20.0965551
        }
        {
          longitude: -98.7737152
          latitude: 20.0967144
        }
        {
          longitude: -98.7736069
          latitude: 20.0968328
        }
        {
          longitude: -98.7733856
          latitude: 20.0969361
        }
        {
          longitude: -98.7729069
          latitude: 20.0970626
        }
        {
          longitude: -98.77254140000001
          latitude: 20.0971237
        }
        {
          longitude: -98.7722229
          latitude: 20.0971375
        }
        {
          longitude: -98.7718735
          latitude: 20.0971212
        }
        {
          longitude: -98.7715128
          latitude: 20.0970626
        }
        {
          longitude: -98.7711568
          latitude: 20.0969631
        }
        {
          longitude: -98.7706947
          latitude: 20.0967528
        }
        {
          longitude: -98.7702602
          latitude: 20.0964606
        }
        {
          longitude: -98.769524
          latitude: 20.0957616
        }
        {
          longitude: -98.7694542
          latitude: 20.095601600000002
        }
        {
          longitude: -98.7694475
          latitude: 20.0953977
        }
        {
          longitude: -98.7694824
          latitude: 20.0953221
        }
        {
          longitude: -98.7696218
          latitude: 20.0952037
        }
        {
          longitude: -98.7700617
          latitude: 20.0947377
        }
        {
          longitude: -98.7705445
          latitude: 20.094186
        }
        {
          longitude: -98.7710917
          latitude: 20.093629400000005
        }
        {
          longitude: -98.7721699
          latitude: 20.092647
        }
        {
          longitude: -98.7727332
          latitude: 20.0920827
        }
        {
          longitude: -98.7740046
          latitude: 20.090929
        }
        {
          longitude: -98.7743157
          latitude: 20.0906368
        }
        {
          longitude: -98.7744901
          latitude: 20.090463
        }
        {
          longitude: -98.7746087
          latitude: 20.0902823
        }
        {
          longitude: -98.7747073
          latitude: 20.0900568
        }
        {
          longitude: -98.7749346
          latitude: 20.08982
        }
        {
          longitude: -98.7754745
          latitude: 20.0893093
        }
        {
          longitude: -98.7760678
          latitude: 20.0887394
        }
        {
          longitude: -98.7765493
          latitude: 20.0882803
        }
        {
          longitude: -98.7768826
          latitude: 20.0879811
        }
        {
          longitude: -98.7773896
          latitude: 20.0875158
        }
        {
          longitude: -98.7778724
          latitude: 20.0870365
        }
        {
          longitude: -98.7781942
          latitude: 20.0867254
        }
        {
          longitude: -98.7786589
          latitude: 20.0862733
        }
        {
          longitude: -98.778785
          latitude: 20.086357
        }
        {
          longitude: -98.7788232
          latitude: 20.0873168
        }
        {
          longitude: -98.77886
          latitude: 20.0883892
        }
        {
          longitude: -98.7788828
          latitude: 20.0888874
        }
        {
          longitude: -98.7788849
          latitude: 20.0896085
        }
        {
          longitude: -98.7788997
          latitude: 20.0900153
        }
        {
          longitude: -98.7803044
          latitude: 20.0904472
        }
        {
          longitude: -98.7815939
          latitude: 20.0908515
        }
        {
          longitude: -98.7825501
          latitude: 20.0911557
        }
        {
          longitude: -98.7838537
          latitude: 20.0915638
        }
        {
          longitude: -98.7845134
          latitude: 20.0917603
        }
        {
          longitude: -98.7857292
          latitude: 20.0921079
        }
        {
          longitude: -98.7856862
          latitude: 20.0923063
        }
        {
          longitude: -98.7855422
          latitude: 20.0927086
        }
        {
          longitude: -98.7850485
          latitude: 20.0938882
        }
        {
          longitude: -98.785172
          latitude: 20.0939298
        }
        {
          longitude: -98.7854201
          latitude: 20.0933851
        }
        {
          longitude: -98.7855461
          latitude: 20.0931123
        }
        {
          longitude: -98.7857111
          latitude: 20.0927112
        }
        {
          longitude: -98.7858358
          latitude: 20.092317
        }
        {
          longitude: -98.7858941
          latitude: 20.0920575
        }
        {
          longitude: -98.785915
          latitude: 20.0909781
        }
        {
          longitude: -98.7859223
          latitude: 20.0902142
        }
        {
          longitude: -98.7859964
          latitude: 20.0900855
        }
        {
          longitude: -98.7860826
          latitude: 20.0897401
        }
        {
          longitude: -98.7861201
          latitude: 20.0895984
        }
        {
          longitude: -98.7862059
          latitude: 20.0894504
        }
        {
          longitude: -98.7863615
          latitude: 20.0892275
        }
        {
          longitude: -98.786495
          latitude: 20.0890845
        }
        {
          longitude: -98.7865583
          latitude: 20.0889582
        }
        {
          longitude: -98.7865888
          latitude: 20.0888552
        }
        {
          longitude: -98.7870049
          latitude: 20.0890268
        }
        {
          longitude: -98.7874512
          latitude: 20.0891796
        }
        {
          longitude: -98.7886605
          latitude: 20.0896771
        }
        {
          longitude: -98.78945550000002
          latitude: 20.0900089
        }
        {
          longitude: -98.7902501
          latitude: 20.0903301
        }
        {
          longitude: -98.7906135
          latitude: 20.0895852
        }
        {
          longitude: -98.7908603
          latitude: 20.0890687
        }
        {
          longitude: -98.7913897
          latitude: 20.089302
        }
        {
          longitude: -98.7918131
          latitude: 20.0894711
        }
        {
          longitude: -98.7920518
          latitude: 20.088993800000004
        }
        {
          longitude: -98.7922848
          latitude: 20.088563
        }
        {
          longitude: -98.7925487
          latitude: 20.0881209
        }
        {
          longitude: -98.7929678
          latitude: 20.0873205
        }
        {
          longitude: -98.7937309
          latitude: 20.0876939
        }
        {
          longitude: -98.7944209
          latitude: 20.0880271
        }
        {
          longitude: -98.7953395
          latitude: 20.088444
        }
        {
          longitude: -98.7958767
          latitude: 20.0887337
        }
        {
          longitude: -98.7962299
          latitude: 20.0889638
        }
        {
          longitude: -98.7962944
          latitude: 20.0890165
        }
        {
          longitude: -98.79631920000001
          latitude: 20.0890039
        }
      ]
      substations: [
        {
          id: 1
          options:
            visible: true
          position:
            longitude: -98.7962636
            latitude: 20.0890039
        }
        {
          id: 2
          options:
            visible: true
          position:
            longitude: -98.7953898
            latitude: 20.0884289
        }
        {
          id: 3
          options:
            visible: true
          position:
            longitude: -98.7937865
            latitude: 20.0876782
        }
        {
          id: 4
          options:
            visible: true
          position:
            longitude: -98.7942351
            latitude: 20.086767
        }
        {
          id: 5
          options:
            visible: true
          position:
            longitude: -98.7950452
            latitude: 20.0853135
        }
        {
          id: 6
          options:
            visible: true
          position:
            longitude: -98.7933949
            latitude: 20.0864395
        }
        {
          id: 7
          options:
            visible: true
          position:
            longitude: -98.7925272
            latitude: 20.0880674
        }
        {
          id: 8
          options:
            visible: true
          position:
            longitude: -98.7908388
            latitude: 20.0890102
        }
        {
          id: 9
          options:
            visible: true
          position:
            longitude: -98.7902407
            latitude: 20.0902665
        }
        {
          id: 10
          options:
            visible: true
          position:
            longitude: -98.7887239
            latitude: 20.0896531
        }
        {
          id: 11
          options:
            visible: true
          position:
            longitude: -98.78662440000001
            latitude: 20.0888225
        }
        {
          id: 12
          options:
            visible: true
          position:
            longitude: -98.7859531
            latitude: 20.0870119
        }
        {
          id: 13
          options:
            visible: true
          position:
            longitude: -98.7864406
            latitude: 20.0839324
        }
        {
          id: 14
          options:
            visible: true
          position:
            longitude: -98.7858861
            latitude: 20.0849154
        }
        {
          id: 15
          options:
            visible: true
          position:
            longitude: -98.7857084
            latitude: 20.0920449
        }
        {
          id: 16
          options:
            visible: true
          position:
            longitude: -98.7813291
            latitude: 20.0907238
        }
        {
          id: 17
          options:
            visible: true
          position:
            longitude: -98.7789613
            latitude: 20.0899907
        }
        {
          id: 18
          options:
            visible: true
          position:
            longitude: -98.7789197
            latitude: 20.0887835
        }
        {
          id: 19
          options:
            visible: true
          position:
            longitude: -98.778858
            latitude: 20.0864653
        }
        {
          id: 20
          options:
            visible: true
          position:
            longitude: -98.7768457
            latitude: 20.0879616
        }
        {
          id: 21
          options:
            visible: true
          position:
            longitude: -98.7749936
            latitude: 20.089706
        }
        {
          id: 22
          options:
            visible: true
          position:
            longitude: -98.7753645
            latitude: 20.0926892
        }
        {
          id: 23
          options:
            visible: true
          position:
            longitude: -98.7728224
            latitude: 20.0970564
        }
        {
          id: 24
          options:
            visible: true
          position:
            longitude: -98.7706377
            latitude: 20.0966968
        }
        {
          id: 25
          options:
            visible: true
          position:
            longitude: -98.769538
            latitude: 20.0953032
        }
        {
          id: 26
          options:
            visible: true
          position:
            longitude: -98.774936
            latitude: 20.0898364
        }
        {
          id: 27
          options:
            visible: true
          position:
            longitude: -98.776896
            latitude: 20.0879912
        }
        {
          id: 28
          options:
            visible: true
          position:
            longitude: -98.7786475
            latitude: 20.0863431
        }
        {
          id: 29
          options:
            visible: true
          position:
            longitude: -98.778858
            latitude: 20.0886015
        }
        {
          id: 30
          options:
            visible: true
          position:
            longitude: -98.77895260000001
            latitude: 20.090053
        }
        {
          id: 31
          options:
            visible: true
          position:
            longitude: -98.7811587
            latitude: 20.090726900000003
        }
        {
          id: 32
          options:
            visible: true
          position:
            longitude: -98.7856916
            latitude: 20.0921123
        }
        {
          id: 33
          options:
            visible: true
          position:
            longitude: -98.7858988
            latitude: 20.0921381
        }
        {
          id: 34
          options:
            visible: true
          position:
            longitude: -98.7866224
            latitude: 20.0888899
        }
        {
          id: 35
          options:
            visible: true
          position:
            longitude: -98.78862530000002
            latitude: 20.0896746
        }
        {
          id: 36
          options:
            visible: true
          position:
            longitude: -98.7902058
            latitude: 20.0903232
        }
        {
          id: 37
          options:
            visible: true
          position:
            longitude: -98.7908529
            latitude: 20.0891059
        }
        {
          id: 38
          options:
            visible: true
          position:
            longitude: -98.79253530000001
            latitude: 20.0881688
        }
        {
          id: 39
          options:
            visible: true
          position:
            longitude: -98.7936974
            latitude: 20.0876978
        }
        {
          id: 40
          options:
            visible: true
          position:
            longitude: -98.7953027
            latitude: 20.0884446
        }
      ]
    _getRoute = ->
      _route
    {
      getRoute: _getRoute
    }
]
