services.provider 'languages', languagesProvider = ->
  algo = true
  _messages = [
    # Spanish messages
    {
      # Menu
      CARD: 'Tarjeta'
      MAP: 'Mapa'
      MAKE_ROUTE: 'Crear ruta'
      STATIONS: 'Estaciones'
      EXPRESS_ROUTES: 'Rutas exprés'
      FEEDER_ROUTES: 'Rutas alimentadoras'
      EVENTS: 'Eventos'
      NEWS: 'Noticias'
      PLACES: 'Lugares'
      INFORMATION: 'Información'
      CONFIGURATION: 'Configuración'
      HELP: 'Ayuda'
      ABOUT: 'Acerca de'
      # Card view
      CARD_BALANCE: 'Saldo'
      CARD_CREDIT: 'Credito'
      CARD_HISTORY: 'Historial'
      # Map view
      MAP_OPTIONS: 'Ver'
      # Make route view
      MAKE_ROUTE_ORIGIN: 'Origen'
      MAKE_ROUTE_DESTINE: 'Destino'
      MAKE_ROUTE_CREATE_BUTTON: 'Crear ruta'
      MAKE_ROUTE_MY_UBICATION: 'Mi ubicación'
      # Stations view
      STATION_ADDRESS: 'Dirección'
      STATION_NEAREST_PLACES: 'Lugares cercanos'
      # Places view
      PLACES_ORDER_BY: 'Ordernar por'
      PLACES_ORDER_BY_DEFAULT: 'Default'
      PLACES_ORDER_BY_NAME_ASCENDET: 'Nombre ascendente'
      PLACES_ORDER_BY_NAME_DESCENDENT: 'Nombre descendete'
      PLACES_ORDER_BY_CATEGORY: 'Categoría'
      PLACES_DISTANCE: 'Distancia'
      PLACES_APPROXIMATE_TIME: 'Tiempo aproximado'
      PLACES_CATEGORY_gas: 'Gasolinera'
      PLACES_CATEGORY_mall: 'Centro comercial'
      PLACES_CATEGORY_restaurant: 'Restaurant'
      PLACES_CATEGORY_shop: 'Tienda'
      # Information view
      INFORMATION_COST: 'Costo'
      INFORMATION_HORARY: 'Horarios'
      INFORMATION_HORARY_MONDAY_FRIDAY: 'Lunes a Viernes'
      INFORMATION_HORARY_SATURDAY: 'Sábado'
      INFORMATION_HORARY_SUNDAY: 'Domingo'
      INFORMATION_PHONES: 'Teléfonos'
      INFORMATION_WEB_PAGE: 'Página web'
      INFORMATION_EMAIL: 'Correo electrónico'
      INFORMATION_SUGGESTS: 'Quejas y sugerencias'
      INFORMATION_SUGGESTS_CONTENT: '¿Tienes alguna queja o sugerencia?'
      INFORMATION_SUGGESTS_SEND_BUTTON: 'Enviar'
      # Configuration view
      CONFIGURATION_TITLE: 'Configuración'
      CONFIGURATION_LABEL_LANGUAGE: 'Lenguaje'
      CONFIGURATION_THEME: 'Tema'
      CONFIGURATION_THEME_LIGHT: 'Claro'
      CONFIGURATION_THEME_DARK: 'Oscuro'
      # Miscelaneous
      SEARCH: 'Buscar'
      CANCEL: 'Cancelar'
      MAIN_ROUTE: 'Ruta troncal'
      ROUTE_1: 'Ruta exprés 1'
      ROUTE_2: 'Ruta exprés 2'
      ROUTE_3: 'Ruta exprés 3'
      ROUTE_4: 'Ruta exprés 4'
    }
    # English messages
    {
      # Menu
      CARD: 'Card'
      MAP: 'Map'
      MAKE_ROUTE: 'Make route'
      STATIONS: 'Stations'
      EXPRESS_ROUTES: 'Express routes'
      FEEDER_ROUTES: 'Feeder routes'
      EVENTS: 'Events'
      NEWS: 'News'
      PLACES: 'Places'
      INFORMATION: 'Information'
      CONFIGURATION: 'Configuration'
      HELP: 'Help'
      ABOUT: 'About'
      # Card view
      CARD_BALANCE: 'Balance'
      CARD_CREDIT: 'Credit'
      CARD_HISTORY: 'History'
      # Map view
      MAP_OPTIONS: 'View'
      # Make route view
      MAKE_ROUTE_ORIGIN: 'Origin'
      MAKE_ROUTE_DESTINE: 'Destine'
      MAKE_ROUTE_CREATE_BUTTON: 'Create route'
      MAKE_ROUTE_MY_UBICATION: 'My ubication'
      # Stations view
      STATION_ADDRESS: 'Address'
      STATION_NEAREST_PLACES: 'Nearest places'
      # Places view
      PLACES_ORDER_BY: 'Order by'
      PLACES_ORDER_BY_DEFAULT: 'Default'
      PLACES_ORDER_BY_NAME_ASCENDET: 'Name ascendent'
      PLACES_ORDER_BY_NAME_DESCENDENT: 'Name descendent'
      PLACES_ORDER_BY_CATEGORY: 'Category'
      PLACES_DISTANCE: 'Distance'
      PLACES_APPROXIMATE_TIME: 'Approximate time'
      PLACES_CATEGORY_gas: 'Gas'
      PLACES_CATEGORY_mall: 'Mall'
      PLACES_CATEGORY_restaurant: 'Restaurant'
      PLACES_CATEGORY_shop: 'Shop'
      # Information view
      INFORMATION_COST: 'Cost'
      INFORMATION_HORARY: 'Horary'
      INFORMATION_HORARY_MONDAY_FRIDAY: 'Monday to Friday'
      INFORMATION_HORARY_SATURDAY: 'Saturday'
      INFORMATION_HORARY_SUNDAY: 'Sunday'
      INFORMATION_PHONES: 'Phones'
      INFORMATION_WEB_PAGE: 'Web page'
      INFORMATION_EMAIL: 'Email'
      INFORMATION_SUGGESTS: 'Complaint and Suggestion'
      INFORMATION_SUGGESTS_CONTENT: 'Do you have any complaints or suggestions?'
      INFORMATION_SUGGESTS_SEND_BUTTON: 'Send'
      # Configuration view
      CONFIGURATION_TITLE: 'Configuration'
      CONFIGURATION_LABEL_LANGUAGE: 'Language'
      CONFIGURATION_THEME: 'Theme'
      CONFIGURATION_THEME_LIGHT: 'Light'
      CONFIGURATION_THEME_DARK: 'Dark'
      # Miscelaneous
      SEARCH: 'Search'
      CANCEL: 'Cancel'
      MAIN_ROUTE: 'Main route'
      ROUTE_1: 'Route express 1'
      ROUTE_2: 'Route express 2'
      ROUTE_3: 'Route express 3'
      ROUTE_4: 'Route express 4'
    }
    # Italian messages
    {
      # Menu
      CARD: 'Carta'
      MAP: 'Carta'
      MAKE_ROUTE: 'Creare percorso'
      STATIONS: 'Stagioni'
      EXPRESS_ROUTES: 'Itinerari espressi'
      FEEDER_ROUTES: 'Percorsi di alimentazione'
      EVENTS: 'Eventi'
      NEWS: 'Notizie'
      PLACES: 'Posti'
      INFORMATION: 'Informazioni'
      CONFIGURATION: 'Configurazione'
      HELP: 'Aiuto'
      ABOUT: 'Circa di'
      # Card view
      CARD_BALANCE: 'Saldo'
      CARD_CREDIT: 'Credito'
      CARD_HISTORY: 'Record'
      # Map view
      MAP_OPTIONS: 'Vedere'
      # Make route view
      MAKE_ROUTE_ORIGIN: 'Fonte'
      MAKE_ROUTE_DESTINE: 'Destinazione'
      MAKE_ROUTE_CREATE_BUTTON: 'Creare percorso'
      MAKE_ROUTE_MY_UBICATION: 'Mia posizione'
      # Stations view
      STATION_ADDRESS: 'Indirizzo'
      STATION_NEAREST_PLACES: 'Luoghi vicini'
      # Places view
      PLACES_ORDER_BY: 'Ordina per'
      PLACES_ORDER_BY_DEFAULT: 'Predefinito'
      PLACES_ORDER_BY_NAME_ASCENDET: 'Nome ascendente'
      PLACES_ORDER_BY_NAME_DESCENDENT: 'Nome discendente'
      PLACES_ORDER_BY_CATEGORY: 'Categoria'
      PLACES_DISTANCE: 'Distanza'
      PLACES_APPROXIMATE_TIME: 'Tempo approssimativo'
      PLACES_CATEGORY_gas: 'Distributore di benzina'
      PLACES_CATEGORY_mall: 'Centro commerciale'
      PLACES_CATEGORY_restaurant: 'Ristorante'
      PLACES_CATEGORY_shop: 'Negozio'
      # Information view
      INFORMATION_COST: 'Costo'
      INFORMATION_HORARY: 'Orario'
      INFORMATION_HORARY_MONDAY_FRIDAY: 'Lunedi a Venerdì'
      INFORMATION_HORARY_SATURDAY: 'Sabato'
      INFORMATION_HORARY_SUNDAY: 'Domenica'
      INFORMATION_PHONES: 'Telefoni'
      INFORMATION_WEB_PAGE: 'Pagina web'
      INFORMATION_EMAIL: 'E-mail'
      INFORMATION_SUGGESTS: 'Reclami e Suggerimenti'
      INFORMATION_SUGGESTS_CONTENT: 'Avete eventuali reclami o suggerimenti?'
      INFORMATION_SUGGESTS_SEND_BUTTON: 'Inviare'
      # Configuration view
      CONFIGURATION_TITLE: 'Configurazione'
      CONFIGURATION_LABEL_LANGUAGE: 'Lingua'
      CONFIGURATION_THEME: 'Tema'
      CONFIGURATION_THEME_LIGHT: 'Chiaro'
      CONFIGURATION_THEME_DARK: 'Buio'
      # Miscelaneous
      SEARCH: 'Ricerca'
      CANCEL: 'Cancellare'
      MAIN_ROUTE: 'Percorso principale'
      ROUTE_1: 'Percorso espressi 1'
      ROUTE_2: 'Percorso espressi 2'
      ROUTE_3: 'Percorso espressi 3'
      ROUTE_4: 'Percorso espressi 4'
    }
    # French messages
    {
      # Menu
      CARD: 'Carte'
      MAP: 'Carte'
      MAKE_ROUTE: 'Créer itinéraire'
      STATIONS: 'Saisons'
      EXPRESS_ROUTES: 'Routes express'
      FEEDER_ROUTES: 'Routes collectrices'
      EVENTS: 'Événements'
      NEWS: 'Nouvelles'
      PLACES: 'Lieux'
      INFORMATION: 'Information'
      CONFIGURATION: 'Configuration'
      HELP: 'Aide'
      ABOUT: 'À propos de'
      # Card view
      CARD_BALANCE: 'Balance'
      CARD_CREDIT: 'Crédit'
      CARD_HISTORY: 'Record'
      # Map view
      MAP_OPTIONS: 'Regarder'
      # Make route view
      MAKE_ROUTE_ORIGIN: 'Source'
      MAKE_ROUTE_DESTINE: 'Destination'
      MAKE_ROUTE_CREATE_BUTTON: 'Créer itinéraire'
      MAKE_ROUTE_MY_UBICATION: 'Ma position'
      # Stations view
      STATION_ADDRESS: 'Adresse'
      STATION_NEAREST_PLACES: 'À proximité des lieux'
      # Places view
      PLACES_ORDER_BY: 'Trier par'
      PLACES_ORDER_BY_DEFAULT: 'Défaut'
      PLACES_ORDER_BY_NAME_ASCENDET: 'Nom croissant'
      PLACES_ORDER_BY_NAME_DESCENDENT: 'Nom décroissant'
      PLACES_ORDER_BY_CATEGORY: 'Catégorie'
      PLACES_DISTANCE: 'Distance'
      PLACES_APPROXIMATE_TIME: 'Durée approximative'
      PLACES_CATEGORY_gas: 'Station-service'
      PLACES_CATEGORY_mall: 'Centre commercial'
      PLACES_CATEGORY_restaurant: 'Restaurant'
      PLACES_CATEGORY_shop: 'Magasin'
      # Information view
      INFORMATION_COST: 'Coût'
      INFORMATION_HORARY: 'Calendrier'
      INFORMATION_HORARY_MONDAY_FRIDAY: 'Lundi à vendredi'
      INFORMATION_HORARY_SATURDAY: 'Samedi'
      INFORMATION_HORARY_SUNDAY: 'Dimanche'
      INFORMATION_PHONES: 'Téléphones'
      INFORMATION_WEB_PAGE: 'Page web'
      INFORMATION_EMAIL: 'Email'
      INFORMATION_SUGGESTS: 'Plaintes et Suggestions'
      INFORMATION_SUGGESTS_CONTENT: 'Avez-vous des plaintes ou des suggestions?'
      INFORMATION_SUGGESTS_SEND_BUTTON: 'Envoyer'
      # Configuration view
      CONFIGURATION_TITLE: 'Configuration'
      CONFIGURATION_LABEL_LANGUAGE: 'Langue'
      CONFIGURATION_THEME: 'Thème'
      CONFIGURATION_THEME_LIGHT: 'Clair'
      CONFIGURATION_THEME_DARK: 'Sombre'
      # Miscelaneous
      SEARCH: 'Recherche'
      CANCEL: 'Annuler'
      MAIN_ROUTE: 'Route principale'
      ROUTE_1: 'Route express 1'
      ROUTE_2: 'Route express 2'
      ROUTE_3: 'Route express 3'
      ROUTE_4: 'Route express 4'
    }
  ]
  {
    messages: ->
      _messages
    $get: ->
      _messages
  }
