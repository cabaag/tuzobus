services.factory 'FeedersRoutesFactory', [
  'MatildeAFactory'
  'MatildeBFactory'
  'RealJoyasAFactory'
  'RealJoyasBFactory'
  'RealToledoFactory'
  'LaPalmaAFactory'
  'LaPalmaBFactory'
  'ParqueUrbanoAFactory'
  'ParqueUrbanoBFactory'
  'HogaresUnionFactory'
  'RanchoColoniaFactory'
  'TuzosFactory'
  'PitahayasFactory'
  'PaseoCamelinasFactory'
  'HuixmiFactory'
  'ColoniaFactory'
  'VenadoFactory'
  'SanPedroNopancalcoAFactory'
  'SanPedroNopancalcoBFactory'
  'LomaAbetosAFactory'
  'LomaAbetosBFactory'
  'SanCarlosFactory'
  'PuntaAzulAFactory'
  'PuntaAzulBFactory'
  'RealValleFactory'
  'ParquesPoblamientoFactory'
  (MatildeA, MatildeB, RealJoyasA, RealJoyasB, RealToledo, LaPalmaA, LaPalmaB,
  ParqueUrbanoA, ParqueUrbanoB, HogaresUnion, RanchoColonia, Tuzos, Pitahayas,
  PaseoCamelinas, Huixmi, Colonia, Venado, SanPedroNopancalcoA,
  SanPedroNopancalcoB, LomaAbetosA, LomaAbetosB, SanCarlos, PuntaAzulA,
  PuntaAzulB, RealValle, ParquesPoblamiento)->
    _routes = [
      MatildeA.getRoute()
      MatildeB.getRoute()
      RealJoyasA.getRoute()
      RealJoyasB.getRoute()
      RealToledo.getRoute()
      LaPalmaA.getRoute()
      LaPalmaB.getRoute()
      ParqueUrbanoA.getRoute()
      ParqueUrbanoB.getRoute()
      HogaresUnion.getRoute()
      RanchoColonia.getRoute()
      Tuzos.getRoute()
      Pitahayas.getRoute()
      PaseoCamelinas.getRoute()
      Huixmi.getRoute()
      Colonia.getRoute()
      Venado.getRoute()
      SanPedroNopancalcoA.getRoute()
      SanPedroNopancalcoB.getRoute()
      LomaAbetosA.getRoute()
      LomaAbetosB.getRoute()
      SanCarlos.getRoute()
      PuntaAzulA.getRoute()
      PuntaAzulB.getRoute()
      RealValle.getRoute()
      ParquesPoblamiento.getRoute()
    ]
    _getRoutes = ->
      _routes
    _getRoute = (index)->
      _routes[index]
    _getStations = ->
    {
      getRoutes: _getRoutes
      getRoute: _getRoute
    }
]
