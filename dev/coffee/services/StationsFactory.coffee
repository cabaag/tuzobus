services.factory 'StationsFactory', [
  '$q'
  ($q)->
    baseImgs = './img/stations/'
    baseImgsx32 = './img/stationsx32/'
    urlIconBus = 'img/busstop.png'
    urlNearestStation = 'img/busstop_nearest.png'
    zoom = 14
    _nearestStation = {}
    _stations = [
      {
        id: 1
        name: "Terminal Tellez"
        position:
          latitude: 20.014944
          longitude: -98.806535
        address: "Carr. Federal México-Pachuca, Km. 78+000, casi esquina con c"+
        "amino a comunidad de Téllez, Col. Lindavista, C.P.43845, Zempoala Hgo."
        icon:
          url: "#{baseImgs}tellez.png"
        iconx32:
          url: "#{baseImgsx32}tellez.png"
        options:
          visible: true
      }
      {
        id: 2
        name: "Gabriel Mancera"
        position:
          latitude: 20.025588
          longitude: -98.807621
        address: "Carr. Federal México-Pachuca, Km. 79+000, casi esquina con L"+
        "ey Agraria, Col. Santa María Matilde, C.P. 42119, Pachuca de Soto, Hgo"
        icon:
          url: "#{baseImgs}gabriel-mancera.png"
        iconx32:
          url: "#{baseImgsx32}gabriel-mancera.png"
        options:
          visible: true
      }
      {
        id: 3
        name: "Matilde"
        position:
          latitude: 20.030495
          longitude: -98.803138
        address: "Carr. Federal México-Pachuca, Km. 79+600, casi esquina con "+
        "Av. La Principal, Col. Santa María Matilde, C.P. 42119, Pachuca de " +
        "Soto, Hgo."
        icon:
          url: "#{baseImgs}matilde.png"
        iconx32:
          url: "#{baseImgsx32}matilde.png"
        expressRoute: [1, 2 ,4]
        options:
          visible: true
      }
      {
        id: 4
        name: "Efrén Rebolledo"
        position:
          latitude: 20.033617
          longitude: -98.800262
        address: "Carr. Federal México-Pachuca, Km. 79+600, casi esquina con Av. La Principal, Col. Santa María Matilde, C.P. 42119, Pachuca de Soto, Hgo"
        icon:
          url: "#{baseImgs}efren-rebolledo.png"
        iconx32:
          url: "#{baseImgsx32}efren-rebolledo.png"
        expressRoute: [4]
        options:
          visible: true
      }
      {
        id: 5
        name: "Tercera Edad"
        position:
          latitude: 20.042840
          longitude: -98.793469
        address: "Carr. Federal México-Pachuca, Km. 80+600, Cruce con camino a Palma Gorda, Barrio La Palma, C.P. 42110, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}tercera-edad.png"
        iconx32:
          url: "#{baseImgsx32}tercera-edad.png"
        expressRoute: [4]
        options:
          visible: true
      }
      {
        id: 6
        name: "San Antonio"
        position:
          latitude: 20.045735
          longitude: -98.791844
        address: "Carr. Federal México-Pachuca, Km. 81+200, esquina con De Los Gurriones, Col. San Antonio el Desmonte, C.P. 42083, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}san-antonio.png"
        iconx32:
          url: "#{baseImgsx32}san-antonio.png"
        expressRoute: [4]
        options:
          visible: true
      }
      {
        id: 7
        name: "Ejército Mexicano"
        position:
          latitude: 20.052312
          longitude: -98.788425
        address: "Carr. Federal México-Pachuca, Km. 81+620, entre Hidalgo y Manuel Vargas Cataño,Col. San Antonio el Desmonte, C.P. 42083, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}ejercito-mexicano.png"
        iconx32:
          url: "#{baseImgsx32}ejercito-mexicano.png"
        expressRoute: [4]
        options:
          visible: true
      }
      {
        id: 8
        name: "Felipe Angeles"
        position:
          latitude: 20.055137
          longitude: -98.786952
        address: "Carr. Federal México-Pachuca, Km. 82+330, entre Av. De Las Aves y Blvd. Nuevo Hidalgo, Fracc. Villas de Pachuca, C.P. 42083, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}felipe-angeles.png"
        iconx32:
          url: "#{baseImgsx32}felipe-angeles.png"
        expressRoute: [1, 2, 3, 4]
        options:
          visible: true
      }
      {
        id: 9
        name: "Centro de Justicia"
        position:
          latitude: 20.061099
          longitude: -98.783846
        address: "Blvd. Felipe Ángeles, Km.82+830, casi esquina con Av. Los Prismas, Fracc. Los Prismas, C.P. 42083, Pachuca de Soto, Hgo"
        icon:
          url: "#{baseImgs}centro-justicia.png"
        iconx32:
          url: "#{baseImgsx32}centro-justicia.png"
        expressRoute: [2]
        options:
          visible: true
      }
      {
        id: 10
        name: "Vicente Segura"
        position:
          latitude: 20.065296
          longitude: -98.781703
        address: "Blvd. Felipe Ángeles, Km.83+330, frentea estacionamiento de la Feria, Fracc. SPAUAH, C.P. 42082, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}vicente-segura.png"
        iconx32:
          url: "#{baseImgsx32}vicente-segura.png"
        expressRoute: [1, 2, 3, 4]
        options:
          visible: true
      }
      {
        id: 11
        name: "Juan C. Doria"
        position:
          latitude: 20.068718
          longitude: -98.779822
        address: "Blvd. Felipe Ángeles, Km.83+850, entre Justino Fernandez y Gral. Rafael Cravioto, Fracc. Pachoacan, C.P. 42083, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}juanc-doria.png"
        iconx32:
          url: "#{baseImgsx32}juanc-doria.png"
        expressRoute: [2]
        options:
          visible: true
      }
      {
        id: 12
        name: "Hospitales"
        position:
          latitude: 20.075490
          longitude: -98.776232
        address: "Blvd. Felipe Ángeles, Km.84+720, entre Río Papaloapan y Río Yaqui, Unidad habitacional ISSSTE, C.P. 42083, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}hospitales.png"
        iconx32:
          url: "#{baseImgsx32}hospitales.png"
        expressRoute: [2]
        options:
          visible: true
      }
      {
        id: 13
        name: "SEPH"
        position:
          latitude: 20.079527
          longitude: -98.774285
        address: "Blvd. Felipe Ángeles, Km.85+190, frente a SEPH y Club de Golf Pachuca, Ejido Venta Prieta, C.P. 42083, Pachuca de Soto,Hgo"
        icon:
          url: "#{baseImgs}seph.png"
        iconx32:
          url: "#{baseImgsx32}seph.png"
        expressRoute: [2]
        options:
          visible: true
      }
      {
        id: 14
        name: "Tecnológico"
        position:
          latitude: 20.082528
          longitude: -98.772876
        address: "Blvd. Felipe Ángeles, Km.85+710, Col. Venta Prieta, C.P. 42083, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}tecnologico.png"
        iconx32:
          url: "#{baseImgsx32}tecnologico.png"
        expressRoute: [2]
        options:
          visible: true
      }
      {
        id: 15
        name: "Bicentenario"
        position:
          latitude: 20.089126
          longitude: -98.770860
        address: "Blvd. Felipe Ángeles, Km.86+310, Col. Venta Prieta, C.P. 42083, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}bicentenario.png"
        iconx32:
          url: "#{baseImgsx32}bicentenario.png"
        expressRoute: [1, 2, 3, 4]
        options:
          visible: true
      }
      {
        id: 16
        name: "Centro Minero"
        position:
          latitude: 20.090976
          longitude: -98.770783
        address: "Blvd. Felipe Ángeles, Km.86+580, frentea zona comercial Plaza Perisur, Col. Venta Prieta, C.P. 42083, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}centro-minero.png"
        iconx32:
          url: "#{baseImgsx32}centro-minero.png"
        expressRoute: [2]
        options:
          visible: true
      }
      {
        id: 17
        name: "Zona Plateada"
        position:
          latitude: 20.094911
          longitude: -98.769153
        address: "Blvd. Felipe Ángeles, Km.87+000, casi esquina con Camino Real de la Plata, Ejido Venta Prieta, C.P. 42083, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}zona-plateada.png"
        iconx32:
          url: "#{baseImgsx32}zona-plateada.png"
        expressRoute: [2]
        options:
          visible: true
      }
      {
        id: 18
        name: "Tecnológico de Monterrey"
        position:
          latitude: 20.096310
          longitude: -98.767781
        address: "Blvd. Felipe Ángeles, Km.87+600, frente a Tec. de Monterrey, Ejido Venta Prieta, C.P. 42083, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}tecnologico-monterrey.png"
        iconx32:
          url: "#{baseImgsx32}tecnologico-monterrey.png"
        expressRoute: [1, 2, 3, 4]
        options:
          visible: true
      }
      {
        id: 19
        name: "Central de Autobuses"
        position:
          latitude: 20.102593
          longitude: -98.761092
        address: "Blvd. Felipe Ángeles, Km.89+240, frente a Poliforum José M. Morelos, Fracc. Ex-Hacienda de Coscotitlán, C.P. 42064, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}central-autobuses.png"
        iconx32:
          url: "#{baseImgsx32}central-autobuses.png"
        expressRoute: [1, 2, 3, 4]
        options:
          visible: true
      }
      {
        id: 20
        name: "Cuna del Fútbol"
        position:
          latitude: 20.107684
          longitude: -98.755913
        address: "Blvd. Felipe Ángeles, Km.88+375, frente a CECATI, Fracc. Jaime Torres Bodet, C.P. 42064, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}cuna-futbol.png"
        iconx32:
          url: "#{baseImgsx32}cuna-futbol.png"
        expressRoute: [3]
        options:
          visible: true
      }
      {
        id: 21
        name: "Santa Julia"
        position:
          latitude: 20.112664
          longitude: -98.750749
        address: "Av. Benito Juárez, Km.89+840, con esquina a Cuesco, Col. Cuesco, C.P. 42064, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}santa-julia.png"
        iconx32:
          url: "#{baseImgsx32}santa-julia.png"
        expressRoute: [2, 3, 4]
        options:
          visible: true
      }
      {
        id: 22
        name: "Prepa 1"
        position:
          latitude: 20.114804
          longitude: -98.747974
        address: "Av. Benito Juárez, km.90+210, frente a Preparatoria No. 1, Col. Cuesco, C.P. 42064, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}prepa1.png"
        iconx32:
          url: "#{baseImgsx32}prepa1.png"
        expressRoute: [3]
        options:
          visible: true
      }
      {
        id: 23
        name: "Revolución"
        position:
          latitude: 20.115679
          longitude: -98.744867
        address: "Av. Revolución, entre calle Jaime Nunó y Quinta Mary, Col. Revolución, C.P. 42060, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}revolucion.png"
        iconx32:
          url: "#{baseImgsx32}revolucion.png"
        expressRoute: [2, 3, 4]
        options:
          visible: true
      }
      {
        id: 24
        name: "Manuel Dublán"
        position:
          latitude: 20.118398
          longitude: -98.739397
        address: "Av. Revolución, entre calle Manuel Dublán y Jesús Silva, Col. Revolución, C.P.42060, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}manuel-dublan.png"
        iconx32:
          url: "#{baseImgsx32}manuel-dublan.png"
        expressRoute: [3]
        options:
          visible: true
      }
      {
        id: 25
        name: "Presidente Alemán"
        position:
          latitude: 20.121800
          longitude: -98.734690
        address: "Av. Revolución, esquina con calle Belisario Domínguez, Col. Centro, C.P. 42000,Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}presidente-aleman.png"
        iconx32:
          url: "#{baseImgsx32}presidente-aleman.png"
        expressRoute: [2, 3, 4]
        options:
          visible: true
      }
      {
        id: 26
        name: "Niños Heroes"
        position:
          latitude: 20.124951
          longitude: -98.732797
        address: "Calle Mariano Matamoros, entre calle Guadalupe Victoria y Mariano Matamoros, Col. Centro, C.P. 42000, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}ninios-heroes.png"
        iconx32:
          url: "#{baseImgsx32}ninios-heroes.png"
        expressRoute: [4]
        options:
          visible: true
      }
      {
        id: 27
        name: "Centro Histórico"
        position:
          latitude: 20.128021
          longitude: -98.731551
        address: "Calle Ignacio Allende 103, Pachuca, HGO, México"
        icon:
          url: "#{baseImgs}centro-historico.png"
        iconx32:
          url: "#{baseImgsx32}centro-historico.png"
        expressRoute: [4]
        options:
          visible: true
      }
      {
        id: 28
        name: "Plaza Juárez"
        position:
          latitude: 20.122750
          longitude: -98.737020
        address: "Av B. Juárez 104, Pachuca, Hgo., México"
        icon:
          url: "#{baseImgs}plaza-juarez.png"
        iconx32:
          url: "#{baseImgsx32}plaza-juarez.png"
        expressRoute: [2, 3, 4]
        options:
          visible: true
      }
      {
        id: 29
        name: "Parque del Maestro"
        position:
          latitude: 20.120352
          longitude: -98.740522
        address: "Av. Benito Juárez, entre calle Gral. Ignacio Mejía y Manuel Dublán, Col. Centro, C.P. 42000, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}parque-maestro.png"
        iconx32:
          url: "#{baseImgsx32}parque-maestro.png"
        expressRoute: [3]
        options:
          visible: true
      }
      {
        id: 30
        name: "Bioparque"
        position:
          latitude: 20.116476
          longitude: -98.745683
        address: "Av. Benito Juárez, esquina con calle 16 de enero, Col. Maestranza, C.P. 42060, Pachuca de Soto, Hgo."
        icon:
          url: "#{baseImgs}bioparque.png"
        iconx32:
          url: "#{baseImgsx32}bioparque.png"
        expressRoute: [2, 3, 4]
        options:
          visible: true
      }
    ]

    _getStations = ->
      _stations

    _getStation = (id)->
      _stations[id]

    _getStationById = (id)->
      station = null
      id = parseInt id
      angular.forEach _stations, (_station, k)->
        if _station.id is id
          station = _station
      station
    _setVisibility = ->
      _visible = true
    _getNearestStation = ->
      _nearestStation

    _searchNearestStation = (location)->
      _distances = []
      getDistances = (response, status)->
        if status is google.maps.DistanceMatrixStatus.OK
          origins = response.originAddresses
          destinations = response.destinationAddresses
          for i in [0..origins.length-1]
            results = response.rows[i].elements
            for j in [0..results.length-1]
              element = results[j]

              distance = element.distance.text
              duration = element.duration.text
              from = origins[i]
              to = destinations[j]
              distance =
                distance: distance
                duration: duration
                from: from
                to: to
              _distances.push distance
        return

      deferred = $q.defer()
      origin = new google.maps.LatLng(location.latitude, location.longitude)
      destinations = []
      for i in [0..24]
        destinations.push new google.maps.LatLng(_stations[i].position.latitude, _stations[i].position.longitude)
      service = new google.maps.DistanceMatrixService()
      service.getDistanceMatrix
        origins: [origin]
        destinations: destinations
        travelMode: google.maps.TravelMode.DRIVING
      , (response, status)->
        getDistances(response, status)
        destinations = []
        for i in [24..29]
          destinations.push new google.maps.LatLng(_stations[i].position.latitude, _stations[i].position.longitude)
        service = new google.maps.DistanceMatrixService()
        service.getDistanceMatrix
          origins: [origin]
          destinations: destinations
          travelMode: google.maps.TravelMode.DRIVING
        , (response, status)->
          getDistances(response, status)
          _nearestStation.distance = _distances[0].distance
          _nearestStation.duration = _distances[0].duration
          _nearestStation.from = _distances[0].from
          _nearestStation.to = _distances[0].to
          angular.forEach _distances, (value, key)->
            actualDistance = parseFloat _nearestStation.distance.split(" ")[0]
            distance =  parseFloat value.distance.split(" ")[0]
            if distance < actualDistance
              _nearestStation = _stations[key]
              _nearestStation.distance = value.distance
              _nearestStation.duration = value.duration
              _nearestStation.from = value.from
              _nearestStation.to = value.to
          _nearestStation.icon.url = 'img/busstop_nearest.png'
          deferred.resolve(_nearestStation)
      deferred.promise


    {
      getStations: _getStations
      getStation: _getStation
      getStationById: _getStationById
      setVisibility: _setVisibility
      getNearestStation: _getNearestStation
      searchNearestStation: _searchNearestStation
    }
]
