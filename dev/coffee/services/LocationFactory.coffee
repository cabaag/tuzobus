services.factory 'LocationFactory', [
  '$q'
  '$cordovaGeolocation'
  ($q, $cordovaGeolocation)->
    _currentLocation = {
      latitude: 0
      longitude: 0
    }
    _posOptions = {timeout: 10000, enableHighAccuracy: true}
    _error = false
    _changed = false

    _setCurrentLocation = (location)->
      _currentLocation = location

    _getCurrentLocation = ->
      if _currentLocation.latitude isnt 0
        _currentLocation
      else
        _findLocation()

    # Busca la locación actúal del  dispositivo, si la locación no ha cambiado
    # se deja como está, pero si cambio, entonces se cambia la variable booleana
    # '_changed' y se ocupará de referencia para saber si cambio la ubicación
    _findLocation = ->
      deferred = $q.defer()
      $cordovaGeolocation.getCurrentPosition(_posOptions)
        .then (position)->
          if _currentLocation.latitude isnt position.coords.latitude and _currentLocation.longitude isnt position.coords.longitude
            _changed = true
            _currentLocation =
              latitude: position.coords.latitude
              longitude: position.coords.longitude
          else
            _changed = false
          deferred.resolve(_currentLocation)
        , (error)->
          deferred.reject(error)
      deferred.promise

    _hasChanged = ->
      _changed
    {
      setCurrentLocation: _setCurrentLocation
      getCurrentLocation: _getCurrentLocation
      findLocation: _findLocation
      hasChanged: _hasChanged
    }
]
