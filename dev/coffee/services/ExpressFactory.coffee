services.factory 'ExpressFactory', [
  ->
    _express = [
      {
        id: 0
        stations: [1..30]
      }
      {
        id: 1
        stations: [3, 8, 10, 15, 18, 19]
      }
      {
        id: 2
        stations: [3, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 23, 28, 30]
      }
      {
        id: 3
        stations: [8, 10, 15, 18, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30]
      }
      {
        id: 4
        stations: [3, 4, 5, 6, 7, 8, 10, 15, 18, 19, 21, 23, 25, 26, 27, 28, 30]
      }
    ]
    _getExpress = (id)->
      _express[id]
    _getExpresses = ->
      _express
    {
      getExpresses: _getExpresses
      getExpress: _getExpress
    }
]
