angular.module 'tuzobus', [
  'ionic'
  # 'ionic-material'
  'angularLoad'
  'ngCordova'
  'ngStorage'
  'tuzobus.controllers'
  'tuzobus.services'
  'tuzobus.directives'
  'tuzobus.filters'
  'uiGmapgoogle-maps'
  'pascalprecht.translate'
]
.run [
  '$rootScope'
  '$ionicPlatform'
  '$localStorage'
  '$translate'
  'API'
  'LocationFactory'
  'ExpressFactory'
  'StationsFactory'
  'FeedersRoutesFactory'
  'ConfigurationFactory'
  'PlacesFactory'
  ($rootScope, $ionicPlatform, $localStorage, $translate, $api, $location,
  $express, $stations, $feedersRoutes, $config, $places) ->
    if $localStorage.languageCode
      $config.setLanguageCode($localStorage.languageCode)
      $translate.use($localStorage.languageCode)
    $ionicPlatform.ready ->
      $location.findLocation()
      if window.cordova and window.cordova.plugins.Keyboard
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar true
      if window.StatusBar
        StatusBar.styleDefault()

      $api.getPlaces()
      .then (success)->
        $localStorage.places = success
        $places.setPlaces(success)
      , (error)->
        $places.setPlaces($localStorage.places)

      # Busca la información de las estaciones para insertarlas en las
      # rutas express
      expresses = $express.getExpresses()
      angular.forEach expresses, (express, keyExpress)->
        stations = []
        angular.forEach $stations.getStations(), (station, key)->
          search = true
          angular.forEach express.stations, (stationExpress, kse)->
            if search && station.id is stationExpress
              stations.push
                name: station.name
                number: station.id
              search = false
          if search
            stations.push
              name: ""
              number: "."
        express.stations = stations

    angular.forEach $feedersRoutes.getRoutes(), (value, key)->
      stations = []
      angular.forEach value.stations, (value, key)->
        stations.push $stations.getStation(value-1)
      value.stations = stations

    angular.forEach $places.getPlaces(), (value, key)->
      switch value.category
        when 1 then value.icon = "ion-pricetag"
        when 2 then value.icon = "ion-university"
        when 3 then value.icon = "ion-fork"
        when 4 then value.icon = "ion-bag"
        else value.icon = ""

    $rootScope.facebookAppId = "976432712417531"
]
.config [
  '$ionicConfigProvider'
  'uiGmapGoogleMapApiProvider'
  '$translateProvider'
  'languagesProvider'
  ($ionicConfigProvider,
  uiGmapGoogleMapApiProvider,
  $translateProvider,
  languagesProvider)->
    # $ionicConfigProvider.views.maxCache(0)
    # $ionicConfigProvider.scrolling.jsScrolling(true)
    $ionicConfigProvider.backButton.previousTitleText(false)
    $ionicConfigProvider.backButton.text('')
    uiGmapGoogleMapApiProvider.configure
      cliet: 'Tuzobus'
      china: false
      v: '3.17'
      language: 'es'
      sensor: 'true'
      libraries: 'places'
    messages = languagesProvider.messages()
    $translateProvider.translations 'es', messages[0]
    $translateProvider.translations 'en', messages[1]
    $translateProvider.translations 'it', messages[2]
    $translateProvider.translations 'fr', messages[3]
    $translateProvider.preferredLanguage('es')
    # $translateProvider.useSanitizeValueStrategy('sanitize')
]
# ./adb logcat SystemWebChromeClient:D chromium:I "*:S"
