<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, X-Session-Token, Content-Type, Accept');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');

require './vendor/autoload.php';
require './app/libs/Twitter.php';
require './app/libs/helper.php';

$config = require './app/config/database.php';
require './app/libs/connect.php';
$db->debug = true;


$app = new \Slim\Slim(array('debug' => true));
require './app/routes/api.php';
$app->run();
