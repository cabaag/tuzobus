<?php

$app->get('/', function() use ($app){
  echo "hola";
});

$app->get('/news/:limit', function($limit) use ($app){
  $tweets = Twitter::getNews($limit);
  if(!empty($tweets)) {
    Helper::objetize($tweets);
    Helper::sendResponse(true, $tweets, "", $app);
  }
});

$app->get('/places', function() use ($app, $db){
  $places = $db->Places();
  if(count($places)) {
    $result = [];
    foreach ($places as $place) {
      $place = Helper::objetize($place);
      $result[] = array(
        "id"=> $place->id,
        "name"=> $place->name,
        "coords" => array(
          "latitude"=>$place->coord_latitude,
          "longitude"=>$place->coord_longitude
        ),
        "stations"=> $place->stations,
        "address"=> $place->address,
        "horary"=> $place->horary,
        "phone"=> $place->phone,
        "email"=> $place->email,
        "pageWeb"=> $place->pageWeb,
        "image"=> $place->image,
        "category"=> $place->category
      );
    }
    $result = Helper::objetize($result);
    Helper::sendResponse(true, $result, "", $app);
  }else{
    // Helper::sendResponse(false, "", "User nor password not valid", $app);
  }
});
