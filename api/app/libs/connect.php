<?php

try{
  $pdo = new PDO($config[db][connectionString], $config[db][username], $config[db][password]);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $db  = new NotORM($pdo);
}catch(PDOException $e){
  echo "Error in database ". $e->getMessage(). " ";
}
