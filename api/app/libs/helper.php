<?php

/**
 *
 */
class Helper
{

  public static function verifyMethod($method = '', $app = null) {
    return $app->request->getMethod() == $method;
  }

  public static function objetize($array) {
    return json_decode(json_encode( $array ));
  }

  public static function sendResponse($success = true, $data = "Hola", $errors = null, $app = null) {
    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response->status(200);
    $response->body(json_encode(
      array(
        "success" => $success ? true : false,
        "data" => $data,
        "errors" => $errors
      )
    ));
  }

}
