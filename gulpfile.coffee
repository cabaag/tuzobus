gulp        = require 'gulp'
gutil       = require 'gulp-util'
bower       = require 'bower'
jade        = require 'gulp-jade'
coffee      = require 'gulp-coffee'
concat      = require 'gulp-concat'
uglify      = require 'gulp-uglify'
sass        = require 'gulp-sass'
minifyCss   = require 'gulp-minify-css'
rename      = require 'gulp-rename'
sh          = require 'shelljs'

paths =
  sass:   [ './dev/sass/**/*.scss' ]
  jade:   [ './dev/jade/**/*.jade']
  coffee: [ './dev/coffee/**/*.coffee']
  libs:   [ './dev/libs/**/*']
  images: [ './dev/img/**/*']
  index:  [ './dev/index.jade']
  maps:   [ './dev/maps/**/*']


gulp.task 'default', [
  'sass'
  'jade'
  'coffee'
  'index'
  'images'
  'libs'
  'watch'
]

gulp.task 'sass', ->
  gulp.src('./dev/sass/main.scss')
    .pipe sass
      errLogToConsole: true
    .on 'error', gutil.log
    .pipe gulp.dest('./www/css/')
    .pipe minifyCss(keepSpecialComments: 0)
    .pipe rename(extname: '.min.css')
    .pipe gulp.dest('./www/css/')

gulp.task 'jade', ->
  gulp.src(paths.jade)
    .pipe jade(errLogToConsole: true)
      .on 'error', gutil.log
    .pipe gulp.dest('./www/templates/')

gulp.task 'coffee', ->
  gulp.src(paths.coffee)
    .pipe coffee
      bare: true
    .on 'error', gutil.log
    .pipe concat('main.js')
    .pipe gulp.dest './www/js/'
    .pipe uglify()
    .pipe rename extname: '.min.js'
    .pipe gulp.dest './www/js/'

gulp.task 'index', ->
  gulp.src('./dev/index.jade')
    .pipe jade()
    .pipe gulp.dest('./www/')

gulp.task 'libs', ->
  gulp.src(paths.libs)
    .pipe gulp.dest './www/libs/'

gulp.task 'images', ->
  gulp.src(paths.images)
    .pipe gulp.dest './www/img/'

gulp.task 'maps', ->
  gulp.src(paths.maps)
    .pipe gulp.dest './www/maps/'

gulp.task 'watch', ->
  gulp.watch paths.sass, [ 'sass' ]
  gulp.watch paths.jade, [ 'jade' ]
  gulp.watch paths.coffee, [ 'coffee' ]
  gulp.watch paths.libs, [ 'libs' ]
  gulp.watch paths.images, [ 'images' ]
  gulp.watch paths.index, [ 'index' ]

gulp.task 'install', [ 'git-check' ], ->
  bower.commands.install().on 'log', (data) ->
    gutil.log 'bower', gutil.colors.cyan(data.id), data.message
    return

gulp.task 'git-check', (done) ->
  if !sh.which('git')
    console.log '  ' + gutil.colors.red('Git is not installed.'),
    '\n  Git, the version control system, is required to download Ionic.',
    '\n  Download git here:',
    gutil.colors.cyan('http://git-scm.com/downloads') + '.',
    '\n  Once git is installed, run \'' +
    gutil.colors.cyan('gulp install') + '\' again.'
    process.exit 1
  done()
  return
